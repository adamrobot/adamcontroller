﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace AdamController
{
    public class AdamUdpClient
    {
        private UdpClient client;
        public List<byte> currentBlock { get; private set; }
        public string address;
        public int port;

        public delegate void Connected();
        public event Connected connected = delegate { };

        public AdamUdpClient(int port)
        {
            this.port = port;
            client = new UdpClient(port);
            client.Client.SendBufferSize = 4915200;
        }

        public void ConnectToServer()
        {
            if (!client.Client.Connected)
            {
                client.Connect(address, port);
                connected();
            }
        }

        public void SendData(byte[] data)
        {
            if (client.Client.Connected)
            {
                //int length = 60000;
                //if (data.Length > length)
                //{
                //    client.Send(data.Take(length).ToArray(), length);
                //    client.Send(data.Skip(length).ToArray(), data.Length - length);
                //}
                //else
                //{
                    client.Send(data, data.Length);
                //}
            }
        }
    }
}
