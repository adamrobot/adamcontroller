﻿using ReactiveUI;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Reactive;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Microsoft.Speech.Recognition;
using System.Speech.Synthesis;
using System.Speech.AudioFormat;
using NAudio.Wave;
using System.Reactive.Disposables;

namespace AdamController
{
    public partial class AudioView : UserControl, IViewFor<AudioVM>
    {
        public static readonly DependencyProperty ViewModelProperty = DependencyProperty
           .Register(nameof(ViewModel), typeof(AudioVM), typeof(AudioView));

        public AudioVM ViewModel
        {
            get => (AudioVM)GetValue(ViewModelProperty);
            set => SetValue(ViewModelProperty, value);
        }

        object IViewFor.ViewModel
        {
            get => ViewModel;
            set => ViewModel = (AudioVM)value;
        }

        public ReactiveCommand<Unit, Unit> sendCommand { get; set; }

        public AudioView()
        {
            InitializeComponent();
            DataContext = this;

            voiceSelectorCB.SelectionChanged += VoiceSelectorCBSelectionChanged;
            //sayTB.

            InitSendCommand();
        }

        private void VoiceSelectorCBSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ViewModel.voice = voiceSelectorCB.SelectedItem.ToString();
        }

        public void Init(AdamTcpClient client)
        {
            ViewModel = new AudioVM(client);

            this.WhenActivated(disposable =>
            {
                this.WhenAnyValue(vm => vm.ViewModel)
                    .Subscribe(frame => { UpdateVoiceSelectorCB(); })
                    .DisposeWith(disposable);

                this.WhenAnyValue(vm => vm.ViewModel.voice)
                    .Subscribe(voice => { voiceSelectorCB.SelectedItem = voice; })
                    .DisposeWith(disposable);
            });
        }

        private void InitSendCommand()
        {
            sendCommand = ReactiveCommand.Create(
                () =>
                {
                    ViewModel.Speak();
                }
                );
        }

        private void UpdateVoiceSelectorCB()
        {
            voiceSelectorCB.Items.Clear();

            foreach (var voice in ViewModel.synthesizer.GetInstalledVoices())
                voiceSelectorCB.Items.Add(voice.VoiceInfo.Name);
            
            if (voiceSelectorCB.Items.Count > 0)
                voiceSelectorCB.SelectedIndex = 0;
        }
    }
}
