﻿using Newtonsoft.Json;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using ReactiveUI.Legacy;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Reactive.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Speech.Recognition;
using System.Speech.Synthesis;
using System.Speech.AudioFormat;
using NAudio.Wave;
using System.Linq;

namespace AdamController
{
    public class AudioVM : ITime
    {
        enum AudioRate
        {
            R8000  = 8000,
            R16000 = 16000,
            R22000 = 22000,
            R24000 = 24000,
            R32000 = 32000,
            R42100 = 42100,
            R44100 = 44100,
            R48000 = 48000
        }

        public AdamTcpClient client { get; set; }
        public SpeechSynthesizer synthesizer { get; set; }
        private MemoryStream synthOut { get; set; }
        [Reactive] public string voice { get; set; }
        [Reactive] public string text { get; set; }

        SpeechAudioFormatInfo audioFormat;

        private const string propType = "Type";
        private const string propVoice = "Voice";
        private const string propText = "Text";

        public AudioVM()
        {
            Init();
        }

        public AudioVM(AdamTcpClient client)
        {
            this.client = client;

            Init();
        }

        public AudioVM(AudioVM audioVM)
        {
            Update(audioVM);
            Init();
        }

        private void Update(AudioVM audioVM)
        {
            client = audioVM.client;
            voice = audioVM.voice;
            text = audioVM.text;
        }

        private void Init()
        {
            synthesizer = new SpeechSynthesizer { Volume = 100, Rate = 0 };
            synthOut = new MemoryStream();
            audioFormat = new SpeechAudioFormatInfo(44100, AudioBitsPerSample.Sixteen, AudioChannel.Stereo);
            synthesizer.SetOutputToAudioStream(synthOut, audioFormat);
            //synthesizer.SetOutputToWaveFile("D:/IlyaZuev/AdmProject/VR_16.07.2020/Wpf/default.audio");
            //synthesizer.SpeakProgress += new EventHandler<SpeakProgressEventArgs>(SpeakProgress);
            synthesizer.SpeakCompleted += new EventHandler<SpeakCompletedEventArgs>(SpeakComplited);
        }

        private void SpeakComplited(object sender, SpeakCompletedEventArgs e)
        {
            byte[] buff = synthOut.GetBuffer().ToArray();

            client.SendData(buff);
            synthOut = new MemoryStream();
            synthesizer.SetOutputToAudioStream(synthOut, audioFormat);
        }

        public void Speak()
        {
            if (string.IsNullOrEmpty(text)) return;
            synthesizer.SelectVoice(voice);
            string result = text.Replace(System.Environment.NewLine, " ");
            synthesizer.SpeakAsync(result);
        }

        public ITime Clone() => new AudioVM(this);
        public ITime Get() => this;

        public void Update(object left, object right, int maxTime, int curTime)
        {
            if (curTime == maxTime) Update((AudioVM)right);
            else Update((AudioVM)left);
        }

        public void ReadJson(JsonTextReader reader)
        {
            while (reader.TokenType != JsonToken.EndObject)
            {
                if (reader.TokenType == JsonToken.PropertyName)
                {
                    var token = reader.Value;
                    reader.Read();
                    switch (token)
                    {
                        case propVoice:
                            voice = (string)reader.Value;
                            break;
                        case propText:
                            text = (string)reader.Value;
                            break;
                    }
                }
                reader.Read();
            }
        }

        public void WriteJson(JsonWriter writer)
        {
            writer.WriteStartObject();
            writer.WritePropertyName(propType);
            writer.WriteValue(GetType().FullName);
            writer.WritePropertyName(propVoice);
            writer.WriteValue(voice);
            writer.WritePropertyName(propText);
            writer.WriteValue(text);
            writer.WriteEndObject();
        }
    }
}
