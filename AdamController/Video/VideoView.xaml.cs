﻿using Gst;
using GLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Forms;

namespace AdamController
{
    public partial class VideoView : System.Windows.Controls.UserControl
    {
        private IntPtr handle1Video;
        private IntPtr handle2Video;

        private const int port1 = 5000;
        private const int port2 = 5250;

        private Pipeline pipeline1;
        private Pipeline pipeline2;

        private delegate void BusSyncDelegate(object o, SyncMessageArgs args);

        public VideoView()
        {
            InitializeComponent();
            DataContext = this;

            handle1Video = leftCameraPanel.Handle;
            handle2Video = rightCameraPanel.Handle;
            
            //StartVideoStream(port1, Bus1SyncMessage);
            //StartVideoStream(port1, Bus2SyncMessage);
        }

        private void StartVideoStream(int port, SyncMessageHandler syncMessageHandler)
        {
            Gst.Application.Init();

            var pipeline = Parse.Launch("udpsrc port=5000 caps=\"application/x-rtp, media=(string)video, clock-rate=(int)90000, encoding-name=(string)H264, payload=(int)96\" ! " +
                                        "rtph264depay ! decodebin ! videoconvert ! autovideosink sync=false");

            Bus bus = pipeline.Bus;

            bus.EnableSyncMessageEmission();
            bus.SyncMessage += syncMessageHandler;

            var setStateRet = pipeline.SetState(State.Null);
            Console.WriteLine("SetStateNULL returned: " + setStateRet.ToString());
            setStateRet = pipeline.SetState(State.Ready);
            Console.WriteLine("SetStateReady returned: " + setStateRet.ToString());
            setStateRet = pipeline.SetState(State.Playing);
            Console.WriteLine("SetStateReady returned: " + setStateRet.ToString());
        }

        private void Bus1SyncMessage(object o, SyncMessageArgs args)
        {
            WindowHandle(handle1Video, args);
        }

        private void Bus2SyncMessage(object o, SyncMessageArgs args)
        {
            WindowHandle(handle2Video, args);
        }

        private void WindowHandle(IntPtr videoHandle, SyncMessageArgs args)
        {
            if (Gst.Video.Global.IsVideoOverlayPrepareWindowHandleMessage(args.Message))
            {
                Element src = (Element)args.Message.Src;
                if (src != null)
                {
                    try
                    {
                        Gst.Video.VideoOverlayAdapter overlay = new Gst.Video.VideoOverlayAdapter(src.Handle);
                        overlay.WindowHandle = videoHandle;
                        overlay.HandleEvents(true);
                        Console.WriteLine("Hanle");
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("Exception thrown: " + ex.Message);
                    }
                }
            }            
        }
    }
}
