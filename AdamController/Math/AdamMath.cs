﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace AdamController
{
    static class AdmMath
    {
        static public Coord2 Rotate(Coord2 coord, double angle)
        {
            Coord2 newCoord = new Coord2();
            newCoord[0] = coord[0] * Math.Cos(angle * Math.PI / 180) + coord[1] * Math.Sin(angle * Math.PI / 180);
            newCoord[1] = coord[1] * Math.Cos(angle * Math.PI / 180) - coord[0] * Math.Sin(angle * Math.PI / 180);
            return newCoord;
        }

        static public Coord3 RotateX(Coord3 coord, double angle)
        {
            Coord3 newCoord = new Coord3();
            newCoord[0] = coord[0];
            newCoord[1] = coord[1] * Math.Cos(angle * Math.PI / 180) - coord[2] * Math.Sin(angle * Math.PI / 180);
            newCoord[2] = coord[1] * Math.Sin(angle * Math.PI / 180) + coord[2] * Math.Cos(angle * Math.PI / 180);
            return newCoord;
        }

        static public Coord3 RotateY(Coord3 coord, double angle)
        {
            Coord3 newCoord = new Coord3();
            newCoord[0] = coord[0] * Math.Cos(angle * Math.PI / 180) + coord[2] * Math.Sin(angle * Math.PI / 180);
            newCoord[1] = coord[1];
            newCoord[2] = -1 * coord[0] * Math.Sin(angle * Math.PI / 180) + coord[2] * Math.Cos(angle * Math.PI / 180);
            return newCoord;
        }

        static public Coord3 RotateZ(Coord3 coord, double angle)
        {
            Coord3 newCoord = new Coord3();

            newCoord[0] = coord[0] * Math.Cos(angle * Math.PI / 180) - coord[1] * Math.Sin(angle * Math.PI / 180);
            newCoord[1] = coord[0] * Math.Sin(angle * Math.PI / 180) + coord[1] * Math.Cos(angle * Math.PI / 180);
            newCoord[2] = coord[2];

            return newCoord;
        }

        static public Coord3 EulerRotateZ(Coord3 coord, double angle)
        {
            Coord3 newCoord = new Coord3();

            newCoord[0] = coord[0] * Math.Cos(angle * Math.PI / 180) + coord[1] * Math.Sin(angle * Math.PI / 180);
            newCoord[1] = -coord[0] * Math.Sin(angle * Math.PI / 180) + coord[1] * Math.Cos(angle * Math.PI / 180);
            newCoord[2] = coord[2];

            return newCoord;
        }

        static public Coord3 EulerRotateX(Coord3 coord, double angle)
        {
            Coord3 newCoord = new Coord3();

            newCoord[0] = coord[0];
            newCoord[1] = coord[1] * Math.Cos(angle * Math.PI / 180) + coord[2] * Math.Sin(angle * Math.PI / 180);
            newCoord[2] = -coord[1] * Math.Sin(angle * Math.PI / 180) + coord[2] * Math.Cos(angle * Math.PI / 180);

            return newCoord;
        }

        static public Coord3 EulerRotate(Coord3 coord, double a, double b, double c)
        {
            Coord3 newCoord = new Coord3();
            List<List<double>> matrix = new List<List<double>>()
            {
                new List<double>()
                {
                     Math.Cos(a) * Math.Cos(c) - Math.Sin(a) * Math.Cos(b) * Math.Sin(c),
                    -Math.Cos(a) * Math.Sin(c) - Math.Sin(a) * Math.Cos(b) * Math.Cos(c),
                     Math.Sin(a) * Math.Sin(b),
                },
                new List<double>()
                {
                     Math.Sin(a) * Math.Cos(c) + Math.Cos(a) * Math.Cos(b) * Math.Sin(c),
                    -Math.Sin(a) * Math.Sin(c) + Math.Cos(a) * Math.Cos(b) * Math.Cos(c),
                    -Math.Cos(a) * Math.Sin(b),
                },
                new List<double>()
                {
                     Math.Sin(b) * Math.Sin(c),
                     Math.Sin(b) * Math.Cos(c),
                     Math.Cos(b),
                }
            };

            for (int i = 0; i < 3; i++)
            {
                newCoord[i] = 0;
                for (int j = 0; j < 3; j++)
                    newCoord[i] += coord[j] * matrix[i][j];
            }

            return newCoord;
        }

        static public T Move<T>(T coord, T moveCoord) where T : Coord
        {
            T newCoord = (T)Coord.CreateCoord(coord.length); // need to change

            for (var i = 0; i < newCoord.length; i++)
                newCoord[i] = coord[i] + moveCoord[i];

            return newCoord;
        }

        static public T Rescale<T>(T coord, T scale) where T : Coord
        {
            T newCoord = (T)Coord.CreateCoord(coord.length);
                        
            for (var i = 0; i < newCoord.length; i++)
                newCoord[i] = coord[i] * scale[i];

            return newCoord;
        }

        static public T Invert<T>(T coord) where T : Coord
        {
            T newCoord = (T)Coord.CreateCoord(coord.length);

            for (var i = 0; i < newCoord.length; i++)
                newCoord[i] = 1 / coord[i];

            return newCoord;
        }

        static public T Mult<T>(T coord, double value) where T : Coord
        {
            T newCoord = (T)Coord.CreateCoord(coord.length);

            for (var i = 0; i < newCoord.length; i++)
                newCoord[i] = coord[i] * value;

            return newCoord;
        }

        static public double Dot<T>(T coord1, T coord2) where T : Coord
        {
            double result = 0;

            for (var i = 0; i < coord1.length; i++)
                result += coord1[i] * coord2[i];

            return result;
        }

        static public T Diff<T>(T coord1, T coord2) where T : Coord
        {
            T newCoord = (T)Coord.CreateCoord(coord1.length);

            for (var i = 0; i < newCoord.length; i++)
                newCoord[i] = coord1[i] - coord2[i];

            return newCoord;
        }

        static public double GetDist(Coord coord0, Coord coord1)
        {
            double sum = 0;
            for (var i = 0; i < coord0.length; i++)
                sum += Math.Pow(coord0[i] - coord1[i], 2);

            return Math.Sqrt(sum);
        }

        static public double GetDist(Coord coord)
        {
            return GetDist(coord, Coord.CreateCoord(coord.length));
        }

        static public double GetDist(Coord2 coord, Tuple<Coord2, Coord2> vec)
        {
            double length = Math.Pow(vec.Item1.x - vec.Item2.x, 2) + Math.Pow(vec.Item1.y - vec.Item2.y, 2); 
            if (length == 0.0)
                return GetDist(coord, vec.Item1);

            double t = ((coord.x - vec.Item1.x) * (vec.Item2.x - vec.Item1.x) + (coord.y - vec.Item1.y) * (vec.Item2.y - vec.Item1.y)) / length;
            t = Math.Max(0, Math.Min(1, t));
            Coord2 projection = vec.Item1 + t * ((Point)vec.Item2 - (Point)vec.Item1);
            
            return GetDist(coord, projection);


            //double t = ((coord.x - vec.Item1.x) * (vec.Item2.x - vec.Item1.x) + (coord.y - vec.Item1.y) * (vec.Item2.y - vec.Item1.y)) /
            //    (Math.Pow(vec.Item2.x - vec.Item1.x, 2) + Math.Pow(vec.Item2.y - vec.Item1.y, 2));
            //
            //if (t < 0) t = 0;
            //else if (t > 1) t = 1;
            //
            //return Math.Sqrt(Math.Pow(vec.Item1.x - coord.x + t * (vec.Item2.x - vec.Item1.x), 2) +
            //    Math.Pow(vec.Item1.y - coord.y + t * (vec.Item2.y - vec.Item1.y), 2));
        }

        static public double GetDist(double a, double b)
        {
            return GetDist(new Coord2(a, b));
        }

        static public double CosTh(double a, double b, double c)
        {
            return Math.Acos((a * a + b * b - c * c) / (2 * a * b));
        }
    }
}
