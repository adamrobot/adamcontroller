﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace AdamController
{
    public abstract class Coord
    {
        protected abstract double[] coords { get; set; }

        public int length
        {
            get { return coords.Length; }
            protected set { }
        }

        public double this[int i]
        {
            get { return coords[i]; }
            set { coords[i] = value; }
        }

        public static Coord CreateCoord(int length)
        {
            switch (length)
            {
                case 2:
                    return new Coord2();
                case 3:
                    return new Coord3();
                default:
                    throw new Exception();
            }
        }
    }    

    public class Coord2 : Coord
    {
        public double x
        {
            get { return coords[0]; }
            set { coords[0] = value; }
        }

        public double y
        {
            get { return coords[1]; }
            set { coords[1] = value;  }
        }

        protected override double[] coords { get; set; } = new double[2] { 0, 0 };

        public Coord2() { }

        public Coord2(double x, double y)
        {            
            coords[0] = x;
            coords[1] = y;
        }

        public static implicit operator Point(Coord2 coord)
        {
            return new Point(coord.x, coord.y);
        }

        public static implicit operator Coord2(Point point)
        {
            return new Coord2(point.X, point.Y);
        }

        public static implicit operator Vector(Coord2 coord)
        {
            return new Vector(coord.x, coord.y);
        }

        public static implicit operator Coord2(Vector vector)
        {
            return new Coord2(vector.X, vector.Y);
        }
    }

    public class Coord3 : Coord2
    {
        public double z
        {
            get { return coords[2]; }
            set { coords[2] = value; }
        }

        protected override double[] coords { get; set; } = new double[3] { 0, 0, 0 };

        public static implicit operator Coord3(SharpDX.Vector3 vector)
        {
            return new Coord3(vector.X, vector.Y, vector.Z);
        }

        public Coord3() { }

        public Coord3(double x, double y, double z) : base(x, y)
        {
            coords[2] = z;
        }
    }
}
