﻿using Newtonsoft.Json;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdamController
{
    public class DiodeVM : ReactiveObject
    {
        public enum Color
        {
            NONE  = -1,
            RED   =  0,
            GREEN =  1,
            BLUE  =  2
        }

        [Reactive] public Color color { get; set; } = Color.NONE;
        [Reactive] public bool isSelected { get; set; } = false;

        private const string propColor = "Color";

        public void ReadJson(JsonTextReader reader)
        {
            while (reader.TokenType != JsonToken.EndObject)
            {
                if (reader.TokenType == JsonToken.PropertyName)
                {
                    var token = reader.Value;
                    reader.Read();
                    switch (token)
                    {
                        case propColor:
                            color = (Color)(long)reader.Value;
                            break;
                    }
                }
                reader.Read();
            }

        }

        public void WriteJson(JsonWriter writer)
        {
            writer.WriteStartObject();
            writer.WritePropertyName(propColor);
            writer.WriteValue(color);
            writer.WriteEndObject();
        }
    }
}
