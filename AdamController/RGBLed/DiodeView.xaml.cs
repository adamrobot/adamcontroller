﻿using Newtonsoft.Json;
using ReactiveUI;
using System.Reactive.Disposables;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reactive;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;
using System.Collections.ObjectModel;

namespace AdamController
{
    public partial class DiodeView : UserControl, IViewFor<DiodeVM>
    {        
        public static readonly DependencyProperty ViewModelProperty = DependencyProperty
           .Register(nameof(ViewModel), typeof(DiodeVM), typeof(DiodeView));

        public DiodeVM ViewModel
        {
            get => (DiodeVM)GetValue(ViewModelProperty);
            set => SetValue(ViewModelProperty, value);
        }

        object IViewFor.ViewModel
        {
            get => ViewModel;
            set => ViewModel = (DiodeVM)value;
        }

        public DiodeView()
        {
            InitializeComponent();
            DataContext = this;

            ViewModel = new DiodeVM();

            //this.WhenActivated(disposable =>
            //{
            //    this.WhenAnyValue(v => v.ViewModel)
            //        .Subscribe(vm => { UpdateCircle(vm); })
            //        .DisposeWith(disposable);
            //});
        }
    }
}
