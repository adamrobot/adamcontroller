﻿using Newtonsoft.Json;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using ReactiveUI.Legacy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdamController
{
    public class LedVM : ReactiveObject, ITime
    {
        public enum Type
        {
            LEFT  = 0,
            RIGHT = 1
        }

        public const double diodeCount = 80;
        //public const double minRadius  = 100; //need to change
        //public const double maxRadius  = 50;      
        [Reactive] public double rotate { get; set; } = 0;

        [Reactive] public DiodeVM.Color currentColor { get; set; } = DiodeVM.Color.RED;
        [Reactive] public DiodeVM.Color mainColor { get; set; }    = DiodeVM.Color.RED;
        [Reactive] public Type type { get; set; }
        [Reactive] public ReactiveList<DiodeVM> dvms { get; set; } = new ReactiveList<DiodeVM>();

        private const string propType         = "Type";
        private const string propLedType      = "LedType";
        //private const string propCurrentColor = "CurrentColor";
        //private const string propMainColor    = "MainColor";
        private const string propDvms         = "Dvms";

        public LedVM(Type type)
        {
            this.type = type;

            for (int i = 0; i < diodeCount; i++)
                dvms.Add(new DiodeVM());
        }

        public LedVM()
        {
            for (int i = 0; i < diodeCount; i++)
                dvms.Add(new DiodeVM());
        }

        public LedVM(LedVM ledVM)
        {
            type = ledVM.type;

            ReactiveList<DiodeVM> newDvms = new ReactiveList<DiodeVM>();
            foreach (DiodeVM diode in ledVM.dvms)
                newDvms.Add(new DiodeVM() { color = diode.color });
            dvms = newDvms;
        }

        public ITime Clone() => new LedVM(this);
        public ITime Get() => this;

        public void Update(object left, object right, int maxTime, int curTime)
        {
            LedVM currentLvm;
            if (curTime < maxTime) currentLvm = (LedVM)left;
            else currentLvm = (LedVM)right;

            //currentColor = (LedVM)right.currentColor;
            //mainColor = (LedVM)right.mainColor;
            type = currentLvm.type;

            //for (int i = 0; i < diodeCount; i++)
            //    dvms[i].color = ((LedVM)right).dvms[i].color;
            ReactiveList<DiodeVM> newDvms = new ReactiveList<DiodeVM>();
            foreach (DiodeVM diode in currentLvm.dvms)
                newDvms.Add(new DiodeVM() { color = diode.color });
            dvms = newDvms;
        }

        public void ReadJson(JsonTextReader reader)
        {
            while (reader.TokenType != JsonToken.EndObject)
            {
                if (reader.TokenType == JsonToken.PropertyName)
                {
                    var token = reader.Value;
                    reader.Read();
                    switch (token)
                    {
                        case propLedType:
                            type = (Type)(long)reader.Value;
                            break;
                        case propDvms:
                            ReadJsonDvms(reader);
                            break;
                    }
                }
                reader.Read();
            }
        }

        private void ReadJsonDvms(JsonTextReader reader)
        {
            reader.Read(); // read StartArray token            
            //reader.Read(); // read StartArray token

            int index = 0;
            while (reader.TokenType != JsonToken.EndArray)
            {
                reader.Read(); // read StartObject or EndArray token
                DiodeVM diodeVM = new DiodeVM();
                diodeVM.ReadJson(reader);
                dvms[index].color = diodeVM.color;
                reader.Read(); // read EndObject or EndArray token
                index++;
            }
        }

        public void WriteJson(JsonWriter writer)
        {
            writer.WriteStartObject();
            writer.WritePropertyName(propType);
            writer.WriteValue(GetType().FullName);
            writer.WritePropertyName(propLedType);
            writer.WriteValue(type);
            WriteJsonDvms(writer);
            writer.WriteEndObject();
        }

        private void WriteJsonDvms(JsonWriter writer)
        {
            writer.WritePropertyName(propDvms);
            writer.WriteStartArray();
            foreach (var dvm in dvms)
                dvm.WriteJson(writer);
            writer.WriteEnd();
        }

        public Message.LedScheme GetScheme()
        {
            Message.LedScheme outLedMsg = new Message.LedScheme();
            outLedMsg.type = type;

            //if (lv.diodes.Count == 0) return;

            AddDiode(outLedMsg, 0, 0, dvms[0].color);

            for (int i = 1; i < dvms.Count(); i++)
            {
                if (outLedMsg.colors.Last() != dvms[i].color)
                    AddDiode(outLedMsg, i, i, dvms[i].color);
                else
                    outLedMsg.endDiodes[outLedMsg.endDiodes.Count - 1] = i;
            }

            return outLedMsg;
        }

        private void AddDiode(Message.LedScheme msg, int startDiode, int endDiode, DiodeVM.Color color)
        {
            msg.startDiodes.Add(startDiode);
            msg.endDiodes.Add(endDiode);
            msg.colors.Add(color);
        }
    }
}
