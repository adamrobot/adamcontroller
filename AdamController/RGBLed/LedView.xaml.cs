﻿using Newtonsoft.Json;
using ReactiveUI;
using System.Reactive.Disposables;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reactive;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;
using System.Collections.ObjectModel;

namespace AdamController
{
    public partial class LedView : UserControl, IViewFor<LedVM>
    {
        public static readonly DependencyProperty ViewModelProperty = DependencyProperty
           .Register(nameof(ViewModel), typeof(LedVM), typeof(LedView));

        public ObservableCollection<DiodeView> diodes { get; set; } = new ObservableCollection<DiodeView>();

        public LedVM ViewModel
        {
            get => (LedVM)GetValue(ViewModelProperty);
            set => SetValue(ViewModelProperty, value);
        }

        object IViewFor.ViewModel
        {
            get => ViewModel;
            set => ViewModel = (LedVM)value;
        }

        public LedView()
        {
            InitializeComponent();
            DataContext = this;

            this.WhenActivated(disposable =>
            {
                this.WhenAnyValue(v => v.ViewModel)
                    .Subscribe(t => { UpdateCircle(); })
                    .DisposeWith(disposable);

                this.WhenAnyValue(v => v.ViewModel.dvms)
                    .Subscribe(t => { UpdateDiodes(); })
                    .DisposeWith(disposable);
            });
        }

        public void Init(LedVM.Type type, double rotate)
        {
            ViewModel = new LedVM(type);
            ViewModel.rotate = rotate;
        }

        private void UpdateCircle()
        {
            diodes.Clear();

            double length = 80;
            double radius = 150;

            double spaceLength = 5;
            double diodeCircleCount = 4;
            double diodeCount = LedVM.diodeCount;

            double rectHeight = (length - spaceLength * (diodeCircleCount + 2)) / diodeCircleCount;
            double rectWidth = 2 * Math.PI * (radius - length + spaceLength) / (diodeCount / diodeCircleCount);

            int increment = (int)(360 * diodeCircleCount / diodeCount);

            for (int angle = (int)ViewModel.rotate; angle < 360 + ViewModel.rotate; angle += increment)
            {
                for (int i = 0; i < 4; i++)
                {
                    DiodeView diode = new DiodeView();
                    diode.ViewModel = ViewModel.dvms[i + ((int)(angle - ViewModel.rotate) / increment) * 4];
                    diode.Width = rectWidth;
                    diode.Height = rectHeight;

                    double X = -diode.Width / 2;
                    double Y = -radius + diode.Height * i + spaceLength * (i + 1);

                    Canvas.SetLeft(diode, Math.Cos(angle * Math.PI / 180) * X - Math.Sin(angle * Math.PI / 180) * Y + radius);
                    Canvas.SetTop(diode, Math.Sin(angle * Math.PI / 180) * X + Math.Cos(angle * Math.PI / 180) * Y + radius);
                    diode.RenderTransform = new RotateTransform(angle);
                    diodes.Add(diode);
                }
            }
        }

        private void UpdateDiodes()
        {
            for (int i = 0; i < LedVM.diodeCount; i++)
                diodes[i].ViewModel = ViewModel.dvms[i];
        }

        private void CanvasMouseMove(object sender, MouseEventArgs e)
        {
            Canvas.SetLeft(cursorEllipse, e.GetPosition(canvas).X - cursorEllipse.Width / 2);
            Canvas.SetTop(cursorEllipse, e.GetPosition(canvas).Y - cursorEllipse.Height / 2);

            foreach (DiodeView diode in diodes)
                diode.ViewModel.isSelected = IsIntersectDiode(diode);

            ViewModel.currentColor = ViewModel.mainColor;

            if (Mouse.RightButton == MouseButtonState.Pressed)
                ViewModel.currentColor = DiodeVM.Color.NONE;

            if (Mouse.LeftButton == MouseButtonState.Pressed || Mouse.RightButton == MouseButtonState.Pressed)
                foreach (DiodeView diode in diodes)
                    if (IsIntersectDiode(diode)) diode.ViewModel.color = ViewModel.currentColor;
        }

        private bool IsIntersectDiode(DiodeView diode)
        {
            Coord2 size = diode.RenderTransform.Transform(new Point(diode.Width, diode.Height));
            Coord2 A = new Coord2(Canvas.GetLeft(diode), Canvas.GetTop(diode));
            Coord2 B = new Coord2(A.x + size.x, A.y);
            Coord2 C = new Coord2(A.x + size.x, A.y + size.y);
            Coord2 D = new Coord2(A.x, A.y + size.y);
            double radius = cursorEllipse.Height / 2;
            Coord2 P = new Coord2(Canvas.GetLeft(cursorEllipse) + radius, Canvas.GetTop(cursorEllipse) + radius);

            if (0 <= AdmMath.Dot(AdmMath.Diff(P, A), AdmMath.Diff(B, A)) && 
                AdmMath.Dot(AdmMath.Diff(P, A), AdmMath.Diff(B, A)) <= AdmMath.Dot(AdmMath.Diff(B, A), AdmMath.Diff(B, A)) &&
                0 <= AdmMath.Dot(AdmMath.Diff(P, A), AdmMath.Diff(D, A)) && 
                AdmMath.Dot(AdmMath.Diff(P, A), AdmMath.Diff(D, A)) <= AdmMath.Dot(AdmMath.Diff(D, A), AdmMath.Diff(D, A)))
                return true;            

            if (Math.Abs(AdmMath.GetDist(P, new Tuple<Coord2, Coord2>(A, B))) <= radius ||
                Math.Abs(AdmMath.GetDist(P, new Tuple<Coord2, Coord2>(B, C))) <= radius ||
                Math.Abs(AdmMath.GetDist(P, new Tuple<Coord2, Coord2>(C, D))) <= radius ||
                Math.Abs(AdmMath.GetDist(P, new Tuple<Coord2, Coord2>(D, A))) <= radius)
                return true;

            return false;
        }

        private void CanvasMouseEnter(object sender, MouseEventArgs e)
        {
            Cursor = Cursors.None;
            cursorEllipse.Visibility = Visibility.Visible;
        }

        private void CanvasMouseLeave(object sender, MouseEventArgs e)
        {
            Cursor = Cursors.Arrow;
            cursorEllipse.Visibility = Visibility.Hidden;
        }

        private void CanvasMouseMove(object sender, MouseButtonEventArgs e)
        {
            if (e.GetPosition(canvas).X > canvas.Width || e.GetPosition(canvas).X < 0 ||
                e.GetPosition(canvas).Y > canvas.Height || e.GetPosition(canvas).Y < 0)
            {
                Cursor = Cursors.Arrow;
                cursorEllipse.Visibility = Visibility.Hidden;
            }
        }
    }
}
