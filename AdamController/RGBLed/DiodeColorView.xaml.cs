﻿using Newtonsoft.Json;
using ReactiveUI;
using System.Reactive.Disposables;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reactive;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;
using System.Collections.ObjectModel;

namespace AdamController
{
    public partial class DiodeColorView : UserControl, IViewFor<DiodeVM>
    {
        public static readonly DependencyProperty ViewModelProperty = DependencyProperty
           .Register(nameof(ViewModel), typeof(DiodeVM), typeof(DiodeColorView));

        public DiodeVM ViewModel
        {
            get => (DiodeVM)GetValue(ViewModelProperty);
            set => SetValue(ViewModelProperty, value);
        }

        object IViewFor.ViewModel
        {
            get => ViewModel;
            set => ViewModel = (DiodeVM)value;
        }

        public DiodeColorView()
        {
            InitializeComponent();
            DataContext = this;

            ViewModel = new DiodeVM();

            InitDiodesColors();
            Select(redDiode);
            //this.WhenActivated(disposable =>
            //{
            //    this.WhenAnyValue(v => v.ViewModel)
            //        .Subscribe(vm => { UpdateCircle(vm); })
            //        .DisposeWith(disposable);
            //});
        }

        private void InitDiodesColors()
        {
            redDiode.ViewModel.color   = DiodeVM.Color.RED;
            greenDiode.ViewModel.color = DiodeVM.Color.GREEN;
            blueDiode.ViewModel.color  = DiodeVM.Color.BLUE;
        }

        private void UnselectAll()
        {
            redDiode.ViewModel.isSelected   = false;
            greenDiode.ViewModel.isSelected = false;
            blueDiode.ViewModel.isSelected  = false;
        }

        private void Select(DiodeView diode)
        {
            diode.ViewModel.isSelected = true;
            ViewModel.color = diode.ViewModel.color;
        }

        private void DiodeMouseDown(object sender, MouseButtonEventArgs e)
        {
            UnselectAll();
            Select((DiodeView)sender);
        }
    }
}
