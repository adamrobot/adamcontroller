﻿using Newtonsoft.Json;
using ReactiveUI;
using System.Reactive.Disposables;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reactive;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;
using System.Collections.ObjectModel;

namespace AdamController
{
    public partial class LedsView : UserControl, IViewFor<LedsVM>
    {
        public static readonly DependencyProperty ViewModelProperty = DependencyProperty
           .Register(nameof(ViewModel), typeof(LedsVM), typeof(LedsView));
        
        public ReactiveCommand<Unit, Unit> sendCommand { get; set; }

        public LedsVM ViewModel
        {
            get => (LedsVM)GetValue(ViewModelProperty);
            set => SetValue(ViewModelProperty, value);
        }

        object IViewFor.ViewModel
        {
            get => ViewModel;
            set => ViewModel = (LedsVM)value;
        }

        public LedsView()
        {
            InitializeComponent();
            DataContext = this;

            InitLedViews();
            InitCommand();

            this.WhenActivated(disposable =>
            {
                //diodeColorView.Bind(ViewModel, x => x.color, x => x.ViewModel.color);
                //this.Bind(ViewModel, x => x.color, x => x.leftLedView.ViewModel.mainColor)
                //    .DisposeWith(disposable);
                //this.Bind(ViewModel, x => x.color, x => x.leftLedView.ViewModel.mainColor)
                //    .DisposeWith(disposable);

                diodeColorView.WhenAnyValue(x => x.ViewModel.color)
                    .BindTo(leftLedView, view => view.ViewModel.mainColor)
                    .DisposeWith(disposable);
                diodeColorView.WhenAnyValue(x => x.ViewModel.color)
                    .BindTo(rightLedView, view => view.ViewModel.mainColor)
                    .DisposeWith(disposable);
            });
        }

        private void InitLedViews()
        {
            leftLedView.Init(LedVM.Type.LEFT, 90);
            rightLedView.Init(LedVM.Type.RIGHT, 90);
        }

        private void InitCommand()
        {
            sendCommand = ReactiveCommand.Create(
                () =>
                {
                    SendLedMsg(leftLedView);
                    SendLedMsg(rightLedView);
                }
                );
        }

        private void SendLedMsg(LedView lv)
        {
            ViewModel.client.SendData(lv.ViewModel.GetScheme().Msg(Message.Command.SETLED).GetData());
        }

        public void Init(AdamTcpClient client)
        {
            ViewModel = new LedsVM(client);
        }
    }
}
