﻿using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdamController
{
    public class LedsVM : ReactiveObject
    {
        public AdamTcpClient client { get; set; }

        public LedsVM(AdamTcpClient client)
        {
            this.client = client;
        }
    }
}
