﻿using Newtonsoft.Json;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using System.Reactive.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace AdamController
{
    public class WheelsVM : ReactiveObject
    {
        static public Dictionary<DirectionVM.Direction, List<double>> directions { get; private set; } = new Dictionary<DirectionVM.Direction, List<double>>
        {
            { DirectionVM.Direction.D0,   new List<double> {  1,  1,  1,  1 } },
            { DirectionVM.Direction.D45,  new List<double> {  1,  0,  0,  1 } },
            { DirectionVM.Direction.D90,  new List<double> {  1, -1, -1,  1 } },
            { DirectionVM.Direction.D135, new List<double> {  0, -1, -1,  0 } },
            { DirectionVM.Direction.D180, new List<double> { -1, -1, -1, -1 } },
            { DirectionVM.Direction.D225, new List<double> { -1,  0,  0, -1 } },
            { DirectionVM.Direction.D270, new List<double> { -1,  1,  1, -1 } },
            { DirectionVM.Direction.D315, new List<double> {  0,  1,  1,  0 } },
            { DirectionVM.Direction.CV,   new List<double> {  1, -1,  1, -1 } },
            { DirectionVM.Direction.CCV,  new List<double> { -1,  1, -1,  1 } },
            { DirectionVM.Direction.STOP, new List<double> {  0,  0,  0,  0 } }
        };

        [Reactive] public WheelVM LeftForwardWheelVM  { get; set; }
        [Reactive] public WheelVM RightForwardWheelVM { get; set; }
        [Reactive] public WheelVM LeftBackWheelVM     { get; set; }
        [Reactive] public WheelVM RightBackWheelVM    { get; set; }
        static public AdamUdpClient client;

        public WheelsVM()
        {
            LeftForwardWheelVM  = new WheelVM(WheelVM.WheelID.LeftForwrad);
            RightForwardWheelVM = new WheelVM(WheelVM.WheelID.RightForwrad);
            LeftBackWheelVM     = new WheelVM(WheelVM.WheelID.LeftBack);
            RightBackWheelVM    = new WheelVM(WheelVM.WheelID.RightBack);
        }

        public WheelsVM(WheelsVM wheelsVM)
        {
            LeftForwardWheelVM = new WheelVM(wheelsVM.LeftForwardWheelVM);
            RightForwardWheelVM = new WheelVM(wheelsVM.RightForwardWheelVM);
            LeftBackWheelVM = new WheelVM(wheelsVM.LeftBackWheelVM);
            RightBackWheelVM = new WheelVM(wheelsVM.RightBackWheelVM);
        }

        static public void Send(DirectionVM.Direction direction)
        {
            Message.WheelsScheme scheme = new Message.WheelsScheme();
            scheme.ids = new List<WheelVM.WheelID>();
            scheme.speeds = new List<double>();
            for (int i = 0; i < 4; i++)
            {
                scheme.ids.Add((WheelVM.WheelID)i);
                scheme.speeds.Add(directions[direction][i]);
            }
            client.SendData(scheme.Msg(Message.Command.WRITE).GetData());
        }
    }
}
