﻿using ReactiveUI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reactive.Disposables;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace AdamController
{
    public partial class DirectionV : UserControl, IViewFor<DirectionVM>
    {
        public static readonly DependencyProperty ViewModelProperty = DependencyProperty
            .Register(nameof(ViewModel), typeof(DirectionVM), typeof(DirectionV));

        public DirectionV()
        {
            InitializeComponent();
            DataContext = this;

            ViewModel = new DirectionVM();
        }

        public DirectionVM ViewModel
        {
            get => (DirectionVM)GetValue(ViewModelProperty);
            set => SetValue(ViewModelProperty, value);
        }

        object IViewFor.ViewModel
        {
            get => ViewModel;
            set => ViewModel = (DirectionVM)value;
        }
    }
}
