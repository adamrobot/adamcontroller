﻿using Newtonsoft.Json;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using System.Reactive.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace AdamController
{
    public class DirectionVM : ReactiveObject
    {
        public enum Direction
        {
            D0   = 0,
            D45  = 1,
            D90  = 2,
            D135 = 3,
            D180 = 4,
            D225 = 5,
            D270 = 6,
            D315 = 7,
            CV   = 8,
            CCV  = 9,
            STOP = 10
        }

        static public Dictionary<Direction, double> angles { get; private set; } = new Dictionary<Direction, double>
        {
            { Direction.D0,   0 },
            { Direction.D45,  45 },
            { Direction.D90,  90 },
            { Direction.D135, 135 },
            { Direction.D180, 180 },
            { Direction.D225, 225 },
            { Direction.D270, 270 },
            { Direction.D315, 315 },
            { Direction.CV,   0 },
            { Direction.CCV,  180 },
            { Direction.STOP, -1 }
        };

        [Reactive] public Direction direction { get; set; } = Direction.STOP;
        [Reactive] public bool isHighlighted { get; set; } = false;

        private readonly ObservableAsPropertyHelper<double> _angle;
        public double angle => _angle.Value;

        private readonly ObservableAsPropertyHelper<string> _type;
        public string type => _type.Value;

        public DirectionVM()
        {
            _angle = this.WhenAnyValue(m => m.direction)
                .Select(d => angles[d])
                .ToProperty(this, m => m.angle);

            _type = this.WhenAnyValue(m => m.direction)
                .Select(d =>
                {
                    if (d == Direction.CV || d == Direction.CCV)
                        return "Semicircle";
                    if (d == Direction.STOP)
                        return "Circle";
                    return "Arrow";
                })
                .ToProperty(this, m => m.type);
        }
    }
}
