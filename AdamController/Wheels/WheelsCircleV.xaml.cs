﻿using Newtonsoft.Json;
using ReactiveUI;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reactive;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Text.RegularExpressions;
using System.Windows.Media;

namespace AdamController
{
    public partial class WheelsCircleV : UserControl, IViewFor<WheelsVM>
    {
        public static readonly DependencyProperty ViewModelProperty = DependencyProperty
           .Register(nameof(ViewModel), typeof(WheelsVM), typeof(WheelsCircleV));

        public WheelsVM ViewModel
        {
            get => (WheelsVM)GetValue(ViewModelProperty);
            set => SetValue(ViewModelProperty, value);
        }

        object IViewFor.ViewModel
        {
            get => ViewModel;
            set => ViewModel = (WheelsVM)value;
        }

        private DirectionV selectedDirectionV = null; 
        public WheelsCircleV()
        {
            InitializeComponent();
            ViewModel = new WheelsVM();
            DataContext = ViewModel;

            foreach (DirectionVM.Direction direction in Enum.GetValues(typeof(DirectionVM.Direction)))
            {    
                DirectionV directionV = new DirectionV();
                directionV.ViewModel.direction = direction;
                directionV.MouseDown += DirectionVMouseDown;

                if (direction == DirectionVM.Direction.CV || direction == DirectionVM.Direction.CCV)
                {
                    directionV.RenderTransform = new RotateTransform(directionV.ViewModel.angle) { CenterX = 75, CenterY = 75 };
                    Canvas.SetLeft(directionV, 125);
                    Canvas.SetTop(directionV, 125);
                    canvas.Children.Add(directionV);
                }
                else if (direction == DirectionVM.Direction.STOP)
                {
                    Canvas.SetLeft(directionV, 150);
                    Canvas.SetTop(directionV, 150);
                    canvas.Children.Add(directionV);
                }
                else
                {
                    directionV.RenderTransform = new RotateTransform(directionV.ViewModel.angle) { CenterX = 100, CenterY = 200 };
                    Canvas.SetLeft(directionV, 100);
                    Canvas.SetTop(directionV, 0);
                    canvas.Children.Add(directionV);
                }

                if (direction == DirectionVM.Direction.STOP)
                    SelectDirectionV(directionV);
            }
        }

        private void DirectionVMouseDown(object sender, MouseButtonEventArgs e)
        {
            if (Mouse.RightButton != MouseButtonState.Pressed)
                return;

            SelectDirectionV((DirectionV)(sender));
            //WheelsVM.Send(((DirectionV)sender).ViewModel.direction);
        }

        private void SelectDirectionV(DirectionV directionV)
        {
            DirectionVM.Direction direction = directionV.ViewModel.direction;
            if (selectedDirectionV != null)
                selectedDirectionV.ViewModel.isHighlighted = false;
            selectedDirectionV = directionV;
            selectedDirectionV.ViewModel.isHighlighted = true;

            ViewModel.LeftForwardWheelVM.speed = WheelsVM.directions[direction][0];
            ViewModel.RightForwardWheelVM.speed = WheelsVM.directions[direction][1];
            ViewModel.LeftBackWheelVM.speed = WheelsVM.directions[direction][2];
            ViewModel.RightBackWheelVM.speed = WheelsVM.directions[direction][3];
        }
    }
}
