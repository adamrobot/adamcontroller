﻿using Newtonsoft.Json;
using ReactiveUI;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reactive;
using System.Text;
using System.Threading.Tasks;
using System.Reactive.Disposables;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Text.RegularExpressions;

namespace AdamController
{
    public partial class WheelV : UserControl, IViewFor<WheelVM>
    {
        public static readonly DependencyProperty ViewModelProperty = DependencyProperty
           .Register(nameof(ViewModel), typeof(WheelVM), typeof(WheelV));

        public WheelVM ViewModel
        {
            get => (WheelVM)GetValue(ViewModelProperty);
            set => SetValue(ViewModelProperty, value);
        }

        object IViewFor.ViewModel
        {
            get => ViewModel;
            set => ViewModel = (WheelVM)value;
        }

        public WheelV()
        {
            InitializeComponent();
            ViewModel = new WheelVM(0);
            DataContext = ViewModel;

            //foreach (var i in Enum.GetValues(typeof(WheelVM.Direction))) 
            //    directionCB.Items.Add(i);

            this.WhenActivated(disposable =>
            {
                //this.Bind(ViewModel, x => x.direction, x => x.directionCB.SelectedItem)
                //    .DisposeWith(disposable);
                this.Bind(ViewModel, x => x.speed, x => x.speedSlider.Value)
                    .DisposeWith(disposable);
            });      

            //this.WhenActivated(disposable =>
            //{
            //    //diodeColorView.Bind(ViewModel, x => x.color, x => x.ViewModel.color);
            //    //this.Bind(ViewModel, x => x.color, x => x.leftLedView.ViewModel.mainColor)
            //    //    .DisposeWith(disposable);
            //    //this.Bind(ViewModel, x => x.color, x => x.leftLedView.ViewModel.mainColor)
            //    //    .DisposeWith(disposable);
            //
            //    directionCB.WhenAnyValue(x => x.SelectedItem)
            //        .BindTo(leftLedView, view => view.ViewModel.mainColor)
            //        .DisposeWith(disposable);
            //    diodeColorView.WhenAnyValue(x => x.ViewModel.color)
            //        .BindTo(rightLedView, view => view.ViewModel.mainColor)
            //        .DisposeWith(disposable);
            //});
        }


        private void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            //Regex regex = new Regex("[^0-9]+");
            //e.Handled = regex.IsMatch(e.Text);
        }
    }
}
