﻿using Newtonsoft.Json;
using ReactiveUI;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reactive;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Text.RegularExpressions;

namespace AdamController
{
    public partial class WheelsV : UserControl, IViewFor<WheelsVM>
    {
        public static readonly DependencyProperty ViewModelProperty = DependencyProperty
           .Register(nameof(ViewModel), typeof(WheelsVM), typeof(WheelsV));

        public WheelsVM ViewModel
        {
            get => (WheelsVM)GetValue(ViewModelProperty);
            set => SetValue(ViewModelProperty, value);
        }

        object IViewFor.ViewModel
        {
            get => ViewModel;
            set => ViewModel = (WheelsVM)value;
        }

        public WheelsV()
        {
            InitializeComponent();
            ViewModel = new WheelsVM();
            DataContext = ViewModel;

            this.WhenAnyValue(v => v.ViewModel)
                .Subscribe(vm => {
                    LeftForwardWheelV.ViewModel  = vm.LeftForwardWheelVM;
                    RightForwardWheelV.ViewModel = vm.RightForwardWheelVM;
                    LeftBackWheelV.ViewModel     = vm.LeftBackWheelVM;
                    RightBackWheelV.ViewModel    = vm.RightBackWheelVM;
                });
        }
    }
}
