﻿using Newtonsoft.Json;
using ReactiveUI;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reactive;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Text.RegularExpressions;

namespace AdamController
{
    public partial class WheelsStudio : UserControl
    {
        public WheelsStudio()
        {
            InitializeComponent();

            WheelsV.ViewModel = new WheelsVM();
            WheelsScriptCreatorView.Init(WheelsV.ViewModel);
            ////model3DView.ViewModel = ViewModel.robotModel3DVM;
            //scriptTreeV.ViewModel = new ScriptTreeVM();
            //scriptListView.Init(ScriptVM.Extension.SCRIPT);
            //Gst.Application.Init();
            ////moveScriptCreatorView.Init(ViewModel.robotVM);
            //this.WhenAnyValue(v => v.scriptTreeV.ViewModel.selectedScript)
            //    .Subscribe(t => { moveSetControl.scriptVM = t; });
        }
    }
}
