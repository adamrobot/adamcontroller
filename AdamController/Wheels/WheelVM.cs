﻿using Newtonsoft.Json;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using System;
using System.Collections.Generic;
using System.Reactive.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdamController
{
    public class WheelVM : ReactiveObject, ITime
    {
        public enum WheelID
        {
            LeftForwrad  = 0,
            RightForwrad = 1,
            LeftBack     = 2,
            RightBack    = 3
        }

        [Reactive] public double speed { get; set; } = 0.0;
        [Reactive] public WheelID id { get; private set; }

        private const string propType  = "Type";
        private const string propSpeed = "Speed";
        private const string propWheelID = "WheelID";

        public ITime Clone() => new WheelVM(this);
        public ITime Get() => this;

        public WheelVM() {}

        public WheelVM(WheelID id)
        {
            this.id = id;
        }

        public WheelVM(WheelVM vm)
        {
            speed = vm.speed;
            id = vm.id;
        }

        public Message.WheelScheme GetScheme()
        {
            Message.WheelScheme outWheelMsg = new Message.WheelScheme();
            outWheelMsg.id = id;
            outWheelMsg.speed = speed;

            return outWheelMsg;
        }

        public void Update(object left, object right, int maxTime, int curTime)
        {
            //WheelVM currentLvm;
            //if (curTime < maxTime) currentLvm = (WheelVM)left;
            //else currentLvm = (WheelVM)right;

            speed = ((WheelVM)left).speed + (((WheelVM)right).speed - ((WheelVM)left).speed) / maxTime * curTime;
            //rotate = ((Head)left).rotate + (((Head)right).rotate - ((Head)left).rotate) / maxTime * curTime;
            //direction = currentLvm.direction;
            //speed = currentLvm.speed;
        }

        public void ReadJson(JsonTextReader reader)
        {
            while (reader.TokenType != JsonToken.EndObject)
            {
                if (reader.TokenType == JsonToken.PropertyName)
                {
                    var token = reader.Value;
                    reader.Read();
                    switch (token)
                    {
                        case propSpeed:
                            speed = (double)reader.Value;
                            break;
                        case propWheelID:
                            id = (WheelID)((Int64)(reader.Value));
                            break;
                    }
                }
                reader.Read();
            }
        }

        public void WriteJson(JsonWriter writer)
        {
            writer.WriteStartObject();
            writer.WritePropertyName(propType);
            writer.WriteValue(GetType().FullName);
            writer.WritePropertyName(propSpeed);
            writer.WriteValue(speed);
            writer.WritePropertyName(propWheelID);
            writer.WriteValue((int)id);
            writer.WriteEndObject();
        }
    }
}
