﻿using HelixToolkit.Wpf.SharpDX;
using ReactiveUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Windows.Media.Media3D;

namespace AdamController
{    
    public abstract class UnitModel3D : ReactiveObject
    {        
        //public Model3DGroup model;
        public ObservableElement3DCollection groupModel { get; private set; } = new ObservableElement3DCollection();
        //protected Dictionary<AdamModelName, ModelLink> links = new Dictionary<AdamModelName, ModelLink>();        
        
        public void InitGroupModel(params Model3DBone[] modelBones)
        {
            foreach (var bone in modelBones)
            {
                //links.Add(link.name, link);
                groupModel.Add(bone.model);
                Update(bone);
            }
        }

        public void SetAngle(double angle, Model3DBone bone, Model3DBone parent)
        {
            bone.model.rotationAngle = angle;
            Update(bone, parent);
        }

        public void SetAngle(double angle, Model3DBone bone)
        {
            foreach(var parent in bone.children.Keys)
                SetAngle(angle, bone, parent);
        }

        public void Update(Model3DBone bone)
        {
            foreach (var parent in bone.children.Keys)
                Update(bone, parent);
        }

        public void Update(Model3DBone bone, Model3DBone parent)
        {
            var transformGroup = new Transform3DGroup();
            
            if (parent != null)
                transformGroup.Children.Add(parent.model.geometryModel.Transform);

            var adamModel = bone.model;
            var origin = transformGroup.Transform(adamModel.baseAM.rotationPoint);

            RotateTransform3D transform = new RotateTransform3D(new AxisAngleRotation3D(transformGroup.Transform(adamModel.baseAM.rotationVector), adamModel.rotationAngle));
            transform.CenterX = origin.X;
            transform.CenterY = origin.Y;
            transform.CenterZ = origin.Z;

            transformGroup.Children.Add(transform);

            adamModel.geometryModel.Transform = transformGroup;

            foreach (var child in bone.children[parent])
                Update(child, bone);
        }          
    }    
}
