﻿using System;
using HelixToolkit.Wpf.SharpDX;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Media3D;

namespace AdamController
{
    public class HeadModel3DVM : ReactiveObject
    {
        [Reactive] public HeadModel3D headModel3D { get; set; }

        private Point3D headPrevPoint;
        private System.Windows.Point mousePrevPoint;

        public HeadModel3DVM(Robot.Head head, Model3DBone headBone, Model3DBone neckBone)
        {
            headModel3D = new HeadModel3D(head, headBone, neckBone);

            var geometry = headBone.model.geometryModel;
            geometry.Mouse3DDown += ModelMouse3DDown;
            geometry.Mouse3DMove += ModelMouse3DMove;
        }

        private void ModelMouse3DMove(object sender, Mouse3DEventArgs e)
        {
            var normal = e.Viewport.Camera.LookDirection;
            var newHit = e.Viewport.UnProjectOnPlane(e.Position, headPrevPoint, normal);
            if (newHit.HasValue)
            {
                var offset = newHit.Value - headPrevPoint;
                var result = (mousePrevPoint - e.Position) * offset.Length / 5;

                mousePrevPoint = e.Position;
                headPrevPoint = newHit.Value;

                headModel3D.head.nod -= result.Y;
                headModel3D.head.rotate -= result.X;
            }
        }

        private void ModelMouse3DDown(object sender, Mouse3DEventArgs e)
        {
            mousePrevPoint = e.Position;
            headPrevPoint = e.HitTestResult.PointHit.ToPoint3D();
        }
    }
}
