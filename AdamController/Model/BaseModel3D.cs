﻿using HelixToolkit.Wpf.SharpDX;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Media3D;

namespace AdamController
{
    public enum ModelName
    {
        BodyBond,
        BodyPress,
        BodyPelvis,
        Body,
        LeftLeg1,
        LeftLeg2,
        LeftLeg3,
        RightLeg1,
        RightLeg2,
        RightLeg3,
        LeftHandRotate,
        LeftHand1,
        LeftHand2,
        LeftHand3,
        RightHandRotate,
        RightHand1,
        RightHand2,
        RightHand3,
        Head,
        Neck,
        None
    }

    public class BaseAdamModel3D
    {
        static public Dictionary<ModelName, BaseAdamModel3D> bases { get; private set; } = new Dictionary<ModelName, BaseAdamModel3D>
        {
            { ModelName.BodyBond,        new BaseAdamModel3D(@"models/BodyBond.obj",        new Point3D(312.5424, 445.3992, -563.8701), new Vector3D(1, 0, 0),  90 ) },
            { ModelName.BodyPress,       new BaseAdamModel3D(@"models/BodyPress.obj",       new Point3D(312.5424, 385.4094, -562.7674), new Vector3D(1, 0, 0), -90 ) },
            { ModelName.BodyPelvis,      new BaseAdamModel3D(@"models/BodyPelvis.obj",      new Point3D(383.2424, 340.1096, -598.5868), new Vector3D(1, 0, 0),  60) },
            { ModelName.Body,            new BaseAdamModel3D(@"models/Body.obj",            new Point3D(340.7424, 538.3992, -575.4074), new Vector3D(0, 1, 0)) },
            { ModelName.LeftLeg1,        new BaseAdamModel3D(@"models/LeftLeg1.obj",        new Point3D(383.1358, 221.0639, -557.2979), new Vector3D(1, 0, 0), -110) },
            { ModelName.LeftLeg2,        new BaseAdamModel3D(@"models/LeftLeg2.obj",        new Point3D(291.3491, 87.0977,  -573.9726), new Vector3D(1, 0, 0),  60) },
            { ModelName.LeftLeg3,        new BaseAdamModel3D(@"models/LeftLeg3.obj",        new Point3D(291.3491, 87.0977,  -573.9726), new Vector3D(1, 0, 0)) },
            { ModelName.RightLeg1,       new BaseAdamModel3D(@"models/RightLeg1.obj",       new Point3D(298.2424, 221.0639, -557.2979), new Vector3D(1, 0, 0), -110) },
            { ModelName.RightLeg2,       new BaseAdamModel3D(@"models/RightLeg2.obj",       new Point3D(390.1358, 87.0977,  -573.9726), new Vector3D(1, 0, 0),  60) },
            { ModelName.RightLeg3,       new BaseAdamModel3D(@"models/RightLeg3.obj",       new Point3D(390.1358, 87.0977,  -573.9726), new Vector3D(1, 0, 0)) },
            { ModelName.LeftHandRotate,  new BaseAdamModel3D(@"models/LeftHandRotate.obj",  new Point3D(504.7424, 570.5834, -562.9543), new Vector3D(0, 1, 0)) },
            { ModelName.LeftHand1,       new BaseAdamModel3D(@"models/LeftHand1.obj",       new Point3D(454.2424, 547.2334, -562.9542), new Vector3D(1, 0, 0)) },
            { ModelName.LeftHand2,       new BaseAdamModel3D(@"models/LeftHand2.obj",       new Point3D(525.5432, 556.0265, -407.5806), new Vector3D(1, 0, 0), -180) },
            { ModelName.LeftHand3,       new BaseAdamModel3D(@"models/LeftHand3.obj",       new Point3D(525.4932, 553.5264, -320.3808), new Vector3D(1, 0, 0), -180) },
            { ModelName.RightHandRotate, new BaseAdamModel3D(@"models/RightHandRotate.obj", new Point3D(175.9233, 570.5834, -562.9543), new Vector3D(0, 1, 0)) },
            { ModelName.RightHand1,      new BaseAdamModel3D(@"models/RightHand1.obj",      new Point3D(226.4233, 547.2334, -562.9542), new Vector3D(1, 0, 0)) },
            { ModelName.RightHand2,      new BaseAdamModel3D(@"models/RightHand2.obj",      new Point3D(155.1225, 556.0265, -407.5806), new Vector3D(1, 0, 0), -180) },
            { ModelName.RightHand3,      new BaseAdamModel3D(@"models/RightHand3.obj",      new Point3D(155.1725, 553.5264, -320.3808), new Vector3D(1, 0, 0), -180) },
            { ModelName.Neck,            new BaseAdamModel3D(@"models/Neck.obj",            new Point3D(340.7424, 603.4992, -582.4074), new Vector3D(0, 1, 0)) },
            { ModelName.Head,            new BaseAdamModel3D(@"models/Head.obj",            new Point3D(361.3328, 676.8992, -578.0193), new Vector3D(1, 0, 0)) }
        };

        //static public Dictionary<ModelName, BaseAdamModel> bases { get; private set; } = new Dictionary<ModelName, BaseAdamModel>
        //{
        //    { ModelName.BodyBond,        new BaseAdamModel(@"models/empty.obj",        new Point3D(312.5424, 445.3992, -563.8701), new Vector3D(1, 0, 0),  90 ) },
        //    { ModelName.BodyPress,       new BaseAdamModel(@"models/empty.obj",       new Point3D(312.5424, 385.4094, -562.7674), new Vector3D(1, 0, 0), -90 ) },
        //    { ModelName.BodyPelvis,      new BaseAdamModel(@"models/empty.obj",      new Point3D(383.2424, 340.1096, -598.5868), new Vector3D(1, 0, 0),  60) },
        //    { ModelName.Body,            new BaseAdamModel(@"models/empty.obj",            new Point3D(340.7424, 538.3992, -575.4074), new Vector3D(0, 1, 0)) },
        //    { ModelName.LeftLeg1,        new BaseAdamModel(@"models/empty.obj",        new Point3D(383.1358, 221.0639, -557.2979), new Vector3D(1, 0, 0), -110) },
        //    { ModelName.LeftLeg2,        new BaseAdamModel(@"models/empty.obj",        new Point3D(291.3491, 87.0977,  -573.9726), new Vector3D(1, 0, 0),  60) },
        //    { ModelName.LeftLeg3,        new BaseAdamModel(@"models/empty.obj",        new Point3D(291.3491, 87.0977,  -573.9726), new Vector3D(1, 0, 0)) },
        //    { ModelName.RightLeg1,       new BaseAdamModel(@"models/empty.obj",       new Point3D(298.2399, 340.1096, -598.5868), new Vector3D(1, 0, 0), -10) },
        //    { ModelName.RightLeg2,       new BaseAdamModel(@"models/empty.obj",       new Point3D(298.2424, 221.0639, -557.2979), new Vector3D(1, 0, 0),  100) },
        //    { ModelName.RightLeg3,       new BaseAdamModel(@"models/empty.obj",       new Point3D(390.1358, 87.0977,  -573.9726), new Vector3D(1, 0, 0)) },
        //    { ModelName.LeftHandRotate,  new BaseAdamModel(@"models/empty.obj",  new Point3D(504.7424, 562.9543, -570.5834), new Vector3D(1, 0, 0)) },
        //    { ModelName.LeftHand1,       new BaseAdamModel(@"models/empty.obj",       new Point3D(454.2424, 562.9542, -547.2334), new Vector3D(1, 0, 0)) },
        //    { ModelName.LeftHand2,       new BaseAdamModel(@"models/empty.obj",       new Point3D(525.5432, 556.0265, -407.5806), new Vector3D(1, 0, 0), -180) },
        //    { ModelName.LeftHand3,       new BaseAdamModel(@"models/empty.obj",       new Point3D(525.4932, 553.5264, -320.3808), new Vector3D(1, 0, 0), -180) },
        //    { ModelName.RightHandRotate, new BaseAdamModel(@"models/empty.obj", new Point3D(175.9233, 570.5834, -562.9543), new Vector3D(0, 1, 0)) },
        //    { ModelName.RightHand1,      new BaseAdamModel(@"models/empty.obj",      new Point3D(226.4233, 547.2334, -562.9542), new Vector3D(1, 0, 0)) },
        //    { ModelName.RightHand2,      new BaseAdamModel(@"models/empty.obj",      new Point3D(155.1225, 556.0265, -407.5806), new Vector3D(1, 0, 0), -180) },
        //    { ModelName.RightHand3,      new BaseAdamModel(@"models/empty.obj",      new Point3D(155.1725, 553.5264, -320.3808), new Vector3D(1, 0, 0), -180) },
        //    { ModelName.Neck,            new BaseAdamModel(@"models/empty.obj",            new Point3D(340.7424, 603.4992, -582.4074), new Vector3D(0, 1, 0)) },
        //    { ModelName.Head,            new BaseAdamModel(@"models/empty.obj",            new Point3D(361.3328, 676.8992, -578.0193), new Vector3D(1, 0, 0)) }
        //};

        private string path;
        public Point3D rotationPoint;
        public Vector3D rotationVector;
        public double offsetAngle;
        public HelixToolkit.Wpf.SharpDX.Geometry3D geometry { get; private set; }
        public HelixToolkit.Wpf.SharpDX.Material material { get; private set; }

        private BaseAdamModel3D(string path, Point3D rotationPoint, Vector3D rotationVector, double offsetAngle = 0)
        {
            this.path = path;
            this.rotationPoint = rotationPoint;
            this.rotationVector = rotationVector;
            this.offsetAngle = offsetAngle;

            ReadObject();
        }

        //public BaseAdamModel(BaseAdamModel baseAM)
        //{
        //    path = baseAM.path;
        //    rotationPoint = baseAM.rotationPoint;
        //    rotationVector = baseAM.rotationVector;
        //    offsetAngle = baseAM.offsetAngle;
        //    geometry = baseAM.geometry;
        //    material = baseAM.material;
        //}

        private void ReadObject()
        {
            var reader = new ObjReader();
            var list = reader.Read(path);

            geometry = list[0].Geometry;
            material = PhongMaterials.Bisque;
        }
    }
}
