﻿using HelixToolkit.Wpf.SharpDX;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using SharpDX;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Media3D;

namespace AdamController
{
    public class BodyModel3D : UnitModel3D
    {
        [Reactive] public Robot.Body body { get; set; }
        
        [Reactive] public Model3DBone bodyBone { get; set; }
        [Reactive] public Model3DBone bodyBondBone { get; set; }
        [Reactive] public Model3DBone bodyPressBone { get; set; }
        [Reactive] public Model3DBone bodyPelvisBone{ get; set; }

        public BodyModel3D(Robot.Body body, Model3DBone bodyBone, Model3DBone bodyBondBone, Model3DBone bodyPressBone, Model3DBone bodyPelvisBone)
        {                    
            InitBones(bodyBone, bodyBondBone, bodyPressBone, bodyPelvisBone);
            InitGroupModel(bodyBone, bodyBondBone, bodyPressBone, bodyPelvisBone);

            this.body = body;
            this.WhenAnyValue(m => m.body.nod, m => m.body.rotate)
                .Subscribe(t => { SetAngles(-t.Item1, t.Item1, -t.Item2); });
        }

        private void InitBones(params Model3DBone[] bones)
        {
            bodyBone       = bones[0];
            bodyBondBone   = bones[1];
            bodyPressBone  = bones[2];
            bodyPelvisBone = bones[3];
        }

        public void SetAngles(double angle1, double angle2, double angle3)
        {
            SetAngle(angle1, bodyBondBone);
            SetAngle(angle2, bodyPressBone);
            SetAngle(angle3, bodyBone);
        }
    }
}
