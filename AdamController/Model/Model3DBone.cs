﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Media3D;

namespace AdamController
{
    public class Model3DBone
    {
        public AdamModel3D model;
        public Dictionary<Model3DBone, List<Model3DBone>> children;

        public Model3DBone(AdamModel3D model, Dictionary<Model3DBone, List<Model3DBone>> children)
        {
            this.model = model;
            this.children = children;
        }

        public Model3DBone(AdamModel3D model) : this(model, new Dictionary<Model3DBone, List<Model3DBone>>()) { }

        public void AddChildren(Model3DBone parent, Model3DBone child)
        {
            AddChildren(parent);
            children[parent].Add(child);
        }

        public void AddChildren(Model3DBone parent)
        {
            if (!children.ContainsKey(parent)) children.Add(parent, new List<Model3DBone>());
        }
    }
}
