﻿using System;
using HelixToolkit.Wpf.SharpDX;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Media3D;

namespace AdamController
{
    public class HandModel3DVM : ReactiveObject
    {
        [Reactive] public HandModel3D handModel3D { get; set; }

        private Point3D handPrevPoint;

        public HandModel3DVM(Robot.Hand hand, Model3DBone handRotateBone, Model3DBone hand1Bone, Model3DBone hand2Bone, Model3DBone hand3Bone)
        {
            handModel3D = new HandModel3D(hand, handRotateBone, hand1Bone, hand2Bone, hand3Bone);

            var geometry = hand3Bone.model.geometryModel;
            geometry.Mouse3DDown += ModelMouse3DDown;
            geometry.Mouse3DMove += ModelMouse3DMove;
        }

        private void ModelMouse3DMove(object sender, Mouse3DEventArgs e)
        {
            var normal = e.Viewport.Camera.LookDirection;

            var newHit = e.Viewport.UnProjectOnPlane(e.Position, handPrevPoint, normal);
            if (newHit.HasValue)
            {
                var stMax = handModel3D.hand3Bone.model.geometryModel.Bounds.Maximum.ToPoint3D();
                var stMin = handModel3D.handRotateBone.model.baseAM.rotationPoint;
                var d = Convert.ToSingle(AdmMath.GetDist((Coord3)stMin.ToVector3(), (Coord3)stMax.ToVector3()));
                var scale = handModel3D.hand.maxDist / d;

                RotateTransform3D transform = new RotateTransform3D(new AxisAngleRotation3D(BaseAdamModel3D.bases[ModelName.Body].rotationVector, -handModel3D.hand1Bone.children.Keys.First().model.rotationAngle));
                var offset = newHit.Value - handPrevPoint;
                offset = transform.Transform(newHit.Value - handPrevPoint) * scale;
                handModel3D.hand.coord = new Coord3(handModel3D.hand.coord.x + offset.Z, handModel3D.hand.coord.y + offset.Y, handModel3D.hand.coord.z - offset.X);

                handPrevPoint = newHit.Value;
            }
        }

        private void ModelMouse3DDown(object sender, Mouse3DEventArgs e)
        {
            handModel3D.transformGroup = new Transform3DGroup();
            handModel3D.transformGroup.Children.Add(handModel3D.hand3Bone.model.geometryModel.Transform);
            handPrevPoint = e.HitTestResult.PointHit.ToPoint3D();
        }
    }
}
