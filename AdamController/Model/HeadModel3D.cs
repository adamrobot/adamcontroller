﻿using HelixToolkit.Wpf.SharpDX;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Media3D;

namespace AdamController
{
    public class HeadModel3D : UnitModel3D
    {
        [Reactive] public Robot.Head head { get; set; }

        [Reactive] public Model3DBone headBone { get; set; }
        [Reactive] public Model3DBone neckBone { get; set; }

        public HeadModel3D(Robot.Head head, Model3DBone headBone, Model3DBone neckBone)
        {
            InitBones(headBone, neckBone);
            InitGroupModel(headBone, neckBone);

            this.head = head;
            this.WhenAnyValue(m => m.head.rotate, m => m.head.nod)
                .Subscribe(t => { SetAngles(t.Item1, t.Item2); });
        }

        private void InitBones(params Model3DBone[] bones)
        {
            headBone = bones[0];
            neckBone = bones[1];
        }        

        public void SetAngles(double rotate, double nod)
        {
            SetAngle(nod,    headBone);
            SetAngle(rotate, neckBone);
        }
    }
}
