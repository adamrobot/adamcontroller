﻿using System;
using HelixToolkit.Wpf.SharpDX;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Media3D;
using System.Windows.Input;

namespace AdamController
{
    public class LegModel3DVM : ReactiveObject
    {        
        [Reactive] public LegModel3D legModel3D { get; set; }

        private Point3D legPrevPoint;

        public LegModel3DVM(Robot.Leg leg, Model3DBone leg1Bone, Model3DBone leg2Bone, Model3DBone leg3Bone, Model3DBone bodyPelvisBone)
        {
            legModel3D = new LegModel3D(leg, leg1Bone, leg2Bone, leg3Bone, bodyPelvisBone);

            var geometry = bodyPelvisBone.model.geometryModel;
            geometry.Mouse3DDown += ModelMouse3DDown;
            geometry.Mouse3DMove += ModelMouse3DMove;
        }

        private void ModelMouse3DMove(object sender, Mouse3DEventArgs e)
        {
            var normal = e.Viewport.Camera.LookDirection;

            var newHit = e.Viewport.UnProjectOnPlane(e.Position, legPrevPoint, normal);
            if (newHit.HasValue)
            {
                var offset = newHit.Value - legPrevPoint;
                var model = e.HitTestResult.ModelHit as Element3D;
                offset = model.Transform.Transform(offset);

                legPrevPoint = newHit.Value;

                if (Mouse.LeftButton == MouseButtonState.Pressed)
                    legModel3D.leg.height += offset.Y;
                else if (Mouse.MiddleButton == MouseButtonState.Pressed)
                    legModel3D.leg.kneeHeight += offset.Y;
            }
        }

        private void ModelMouse3DDown(object sender, Mouse3DEventArgs e)
        {
            legPrevPoint = e.HitTestResult.PointHit.ToPoint3D();
        }
    }
}
