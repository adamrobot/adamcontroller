﻿using HelixToolkit.Wpf.SharpDX;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using System.Reactive.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Media3D;

namespace AdamController
{
    public class AdamModel3D : ReactiveObject
    {
        [Reactive] public BaseAdamModel3D baseAM { get; set; }
        [Reactive] public MeshGeometryModel3D geometryModel { get; set; } = new MeshGeometryModel3D();
        //[Reactive] public double rotationAngle { get; set; }
        private double _rotationAngle;
        public double rotationAngle 
        {
            get { return _rotationAngle + baseAM.offsetAngle; }
            set { _rotationAngle = value; }
        }        

        public static implicit operator Element3D(AdamModel3D adamModel)
        {
            return adamModel.geometryModel;
        }

        public AdamModel3D(BaseAdamModel3D baseAdamModel)
        {
            baseAM = baseAdamModel;
            
            this.WhenAnyValue(vm => vm.baseAM)
               .Subscribe(t => { geometryModel.Geometry = t.geometry; geometryModel.Material = t.material; });
        }
    }
}
