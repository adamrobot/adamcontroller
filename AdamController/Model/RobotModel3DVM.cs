﻿using System;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HelixToolkit.Wpf.SharpDX;

namespace AdamController
{
    public class RobotModel3DVM : ReactiveObject
    {
        //[Reactive] public RobotModel3D robotM3D { get; set; }
        public Robot.RobotVM robotVM { get; set; }
        public HeadModel3DVM headM3DVM { get; set; }
        public BodyModel3DVM bodyM3DVM { get; set; }
        public HandModel3DVM leftHandM3DVM { get; set; }
        public HandModel3DVM rightHandM3DVM { get; set; }
        public LegModel3DVM leftLegM3DVM { get; set; }
        public LegModel3DVM rightLegM3DVM { get; set; }

        [Reactive] public ObservableElement3DCollection groupModel { get; private set; } = new ObservableElement3DCollection();

        private Dictionary<ModelName, Model3DBone> bones { get; } = new Dictionary<ModelName, Model3DBone>();

        public RobotModel3DVM(Robot.RobotVM robotVM)
        {
            this.robotVM = robotVM;

            InitBones();

            InitHeadModel();
            InitLegModels();
            InitBodyModel();
            InitHandModels();            
        }        

        private void AddModels(ObservableElement3DCollection models)
        {
            foreach (var model in models)
                groupModel.Add(model);
        }

        private void InitHeadModel()
        {
            headM3DVM = new HeadModel3DVM(robotVM.headVM.head, bones[ModelName.Head], bones[ModelName.Neck]);
            AddModels(headM3DVM.headModel3D.groupModel);
        }

        private void InitLegModels()
        {
            InitLeftLegModel();
            InitRightLegModel();
        }

        private void InitLeftLegModel()
        {
            leftLegM3DVM = new LegModel3DVM(robotVM.leftLegVM.leg, bones[ModelName.LeftLeg1], bones[ModelName.LeftLeg2],
                bones[ModelName.LeftLeg3], bones[ModelName.BodyPelvis]);
            AddModels(leftLegM3DVM.legModel3D.groupModel);
        }

        private void InitRightLegModel()
        {
            rightLegM3DVM = new LegModel3DVM(robotVM.rightLegVM.leg, bones[ModelName.RightLeg1], bones[ModelName.RightLeg2],
                bones[ModelName.RightLeg3], bones[ModelName.BodyPelvis]);        
            AddModels(rightLegM3DVM.legModel3D.groupModel);
        }

        private void InitBodyModel()
        {
            bodyM3DVM = new BodyModel3DVM(robotVM.bodyVM.body, bones[ModelName.Body], bones[ModelName.BodyBond], 
                bones[ModelName.BodyPress], bones[ModelName.BodyPelvis]);
            AddModels(bodyM3DVM.bodyModel3D.groupModel);
        }

        private void InitHandModels()
        {
            InitLeftHandModel();
            InitRightHandModel();
        }

        private void InitLeftHandModel()
        {
            leftHandM3DVM = new HandModel3DVM(robotVM.leftHandVM.hand, bones[ModelName.LeftHandRotate], bones[ModelName.LeftHand1],
                bones[ModelName.LeftHand2], bones[ModelName.LeftHand3]);        
            AddModels(leftHandM3DVM.handModel3D.groupModel);
        }

        private void InitRightHandModel()
        {
            rightHandM3DVM = new HandModel3DVM(robotVM.rightHandVM.hand, bones[ModelName.RightHandRotate], bones[ModelName.RightHand1],
                bones[ModelName.RightHand2], bones[ModelName.RightHand3]);
            AddModels(rightHandM3DVM.handModel3D.groupModel);
        }

        private void InitBones()
        {
            foreach (var baseAM in BaseAdamModel3D.bases)
                bones.Add(baseAM.Key, new Model3DBone(new AdamModel3D(baseAM.Value)));

            bones[ModelName.Body].AddChildren(bones[ModelName.BodyBond], bones[ModelName.LeftHand1]);
            bones[ModelName.Body].AddChildren(bones[ModelName.BodyBond], bones[ModelName.RightHand1]);
            bones[ModelName.Body].AddChildren(bones[ModelName.BodyBond], bones[ModelName.Neck]);
            bones[ModelName.BodyBond].AddChildren(bones[ModelName.BodyPress], bones[ModelName.Body]);
            bones[ModelName.BodyPress].AddChildren(bones[ModelName.BodyPelvis], bones[ModelName.BodyBond]);
            bones[ModelName.BodyPelvis].AddChildren(bones[ModelName.LeftLeg1], bones[ModelName.BodyPress]);
            bones[ModelName.BodyPelvis].AddChildren(bones[ModelName.RightLeg1], bones[ModelName.BodyPress]);

            bones[ModelName.LeftLeg1].AddChildren(bones[ModelName.LeftLeg2], bones[ModelName.BodyPelvis]);
            bones[ModelName.LeftLeg2].AddChildren(bones[ModelName.LeftLeg3], bones[ModelName.LeftLeg1]);

            bones[ModelName.RightLeg1].AddChildren(bones[ModelName.RightLeg2], bones[ModelName.BodyPelvis]);
            bones[ModelName.RightLeg2].AddChildren(bones[ModelName.RightLeg3], bones[ModelName.RightLeg1]);

            bones[ModelName.LeftHand1].AddChildren(bones[ModelName.Body], bones[ModelName.LeftHandRotate]);
            bones[ModelName.LeftHandRotate].AddChildren(bones[ModelName.LeftHand1], bones[ModelName.LeftHand2]);
            bones[ModelName.LeftHand2].AddChildren(bones[ModelName.LeftHandRotate], bones[ModelName.LeftHand3]);
            bones[ModelName.LeftHand3].AddChildren(bones[ModelName.LeftHand2]);

            bones[ModelName.RightHand1].AddChildren(bones[ModelName.Body], bones[ModelName.RightHandRotate]);
            bones[ModelName.RightHandRotate].AddChildren(bones[ModelName.RightHand1], bones[ModelName.RightHand2]);
            bones[ModelName.RightHand2].AddChildren(bones[ModelName.RightHandRotate], bones[ModelName.RightHand3]);
            bones[ModelName.RightHand3].AddChildren(bones[ModelName.RightHand2]);

            bones[ModelName.Head].AddChildren(bones[ModelName.Neck]);
            bones[ModelName.Neck].AddChildren(bones[ModelName.Body], bones[ModelName.Head]);
        }
    }
}
