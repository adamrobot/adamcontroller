﻿using HelixToolkit.Wpf.SharpDX;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Media3D;

namespace AdamController
{
    public class BodyModel3DVM : ReactiveObject
    {
        [Reactive] public BodyModel3D bodyModel3D { get; set; }

        private Point3D bodyPrevPoint;
        private System.Windows.Point mousePrevPoint;

        public BodyModel3DVM(Robot.Body body, Model3DBone bodyBone, Model3DBone bodyBondBone, Model3DBone bodyPressBone, Model3DBone bodyPelvisBone)
        {
            bodyModel3D = new BodyModel3D(body, bodyBone, bodyBondBone, bodyPressBone, bodyPelvisBone);

            var geometry = bodyModel3D.bodyBone.model.geometryModel;
            geometry.Mouse3DDown += ModelMouse3DDown;
            geometry.Mouse3DMove += ModelMouse3DMove;
        }

        private void ModelMouse3DMove(object sender, Mouse3DEventArgs e)
        {
            var normal = e.Viewport.Camera.LookDirection;
            var newHit = e.Viewport.UnProjectOnPlane(e.Position, bodyPrevPoint, normal);
            if (newHit.HasValue)
            {
                var offset = newHit.Value - bodyPrevPoint;
                var result = (mousePrevPoint - e.Position) * offset.Length / 5;

                mousePrevPoint = e.Position;
                bodyPrevPoint = newHit.Value;

                bodyModel3D.body.rotate += result.X;
                bodyModel3D.body.height += result.Y;
            }
        }

        private void ModelMouse3DDown(object sender, Mouse3DEventArgs e)
        {
            mousePrevPoint = e.Position;
            bodyPrevPoint = e.HitTestResult.PointHit.ToPoint3D();
        }
    }
}
