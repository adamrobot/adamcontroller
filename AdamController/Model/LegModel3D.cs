﻿using HelixToolkit.Wpf.SharpDX;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Media.Media3D;

namespace AdamController
{
    public class LegModel3D : UnitModel3D
    {
        [Reactive] public Robot.Leg leg { get; set; }
        
        private Model3DBone leg1Bone;
        private Model3DBone leg2Bone;
        private Model3DBone leg3Bone;
        private Model3DBone bodyPelvisBone;

        public LegModel3D(Robot.Leg leg, Model3DBone leg1Bone, Model3DBone leg2Bone, Model3DBone leg3Bone, Model3DBone bodyPelvisBone)
        {
            InitBones(leg1Bone, leg2Bone, leg3Bone, bodyPelvisBone);
            InitGroupModel(leg1Bone, leg2Bone, leg3Bone, bodyPelvisBone);            

            this.leg = leg;
            this.WhenAnyValue(m => m.leg.angles)
                        .Subscribe(t => { SetAngles(-t[0], t[1], -t[2]); });
        }

        private void InitBones(params Model3DBone[] bones)
        {
            leg1Bone       = bones[0];
            leg2Bone       = bones[1];
            leg3Bone       = bones[2];
            bodyPelvisBone = bones[3];
        }        

        public void SetAngles(double angle1, double angle2, double angle3)
        {
            SetAngle(angle1, bodyPelvisBone);
            SetAngle(angle2, leg1Bone); 
            SetAngle(angle3, leg2Bone);
        }
    }
}
