﻿using HelixToolkit.Wpf.SharpDX;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Media3D;

namespace AdamController
{
    //public class RobotModel3D : ReactiveObject
    //{
    //    [Reactive] public HeadModel3D headModel3D { get; set; }
    //    [Reactive] public BodyModel3D bodyModel3D { get; set; }
    //    [Reactive] public HandModel3D leftHandModel3D { get; set; }
    //    [Reactive] public HandModel3D rightHandModel3D { get; set; }
    //    [Reactive] public LegModel3D  leftLegModel3D { get; set; }
    //    [Reactive] public LegModel3D  rightLegModel3D { get; set; }        
    //
    //    public Dictionary<ModelName, Model3DBone> bones { get; private set; } = new Dictionary<ModelName, Model3DBone>();        
    //
    //    public RobotModel3D()
    //    {
    //        InitBones();
    //    }
    //
    //    public void InitBones()
    //    {
    //        foreach (var baseAM in BaseAdamModel3D.bases)
    //            bones.Add(baseAM.Key,  new Model3DBone(new AdamModel3D(baseAM.Value)));
    //
    //        bones[ModelName.Body].AddChildren(bones[ModelName.BodyBond], bones[ModelName.LeftHand1]);
    //        bones[ModelName.Body].AddChildren(bones[ModelName.BodyBond], bones[ModelName.RightHand1]);            
    //        bones[ModelName.Body].AddChildren(bones[ModelName.BodyBond], bones[ModelName.Neck]);
    //        bones[ModelName.BodyBond].AddChildren(bones[ModelName.BodyPress], bones[ModelName.Body]);
    //        bones[ModelName.BodyPress].AddChildren(bones[ModelName.BodyPelvis], bones[ModelName.BodyBond]);
    //        bones[ModelName.BodyPelvis].AddChildren(bones[ModelName.LeftLeg1], bones[ModelName.BodyPress]);
    //        bones[ModelName.BodyPelvis].AddChildren(bones[ModelName.RightLeg1], bones[ModelName.BodyPress]);
    //
    //        bones[ModelName.LeftLeg1].AddChildren(bones[ModelName.LeftLeg2], bones[ModelName.BodyPelvis]);
    //        bones[ModelName.LeftLeg2].AddChildren(bones[ModelName.LeftLeg3], bones[ModelName.LeftLeg1]);
    //
    //        bones[ModelName.RightLeg1].AddChildren(bones[ModelName.RightLeg2], bones[ModelName.BodyPelvis]);
    //        bones[ModelName.RightLeg2].AddChildren(bones[ModelName.RightLeg3], bones[ModelName.RightLeg1]);
    //
    //        bones[ModelName.LeftHand1].AddChildren(bones[ModelName.Body], bones[ModelName.LeftHandRotate]);
    //        bones[ModelName.LeftHandRotate].AddChildren(bones[ModelName.LeftHand1], bones[ModelName.LeftHand2]);
    //        bones[ModelName.LeftHand2].AddChildren(bones[ModelName.LeftHandRotate], bones[ModelName.LeftHand3]);
    //        bones[ModelName.LeftHand3].AddChildren(bones[ModelName.LeftHand2]);
    //
    //        bones[ModelName.RightHand1].AddChildren(bones[ModelName.Body], bones[ModelName.RightHandRotate]);
    //        bones[ModelName.RightHandRotate].AddChildren(bones[ModelName.RightHand1], bones[ModelName.RightHand2]);
    //        bones[ModelName.RightHand2].AddChildren(bones[ModelName.RightHandRotate], bones[ModelName.RightHand3]);
    //        bones[ModelName.RightHand3].AddChildren(bones[ModelName.RightHand2]);
    //        
    //        bones[ModelName.Head].AddChildren(bones[ModelName.Neck]);
    //        bones[ModelName.Neck].AddChildren(bones[ModelName.Body], bones[ModelName.Head]);
    //    }
    //}
}
