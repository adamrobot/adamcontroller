﻿using HelixToolkit.Wpf.SharpDX;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Media3D;

namespace AdamController
{
    public class HandModel3D : UnitModel3D
    {
        [Reactive] public Robot.Hand hand { get; set; }
        
        [Reactive] public Transform3DGroup transformGroup { get; set; }
        [Reactive] public Model3DBone handRotateBone { get; set; }
        [Reactive] public Model3DBone hand1Bone { get; set; }
        [Reactive] public Model3DBone hand2Bone { get; set; }
        [Reactive] public Model3DBone hand3Bone { get; set; }

        public HandModel3D(Robot.Hand hand, Model3DBone handRotateBone, Model3DBone hand1Bone, Model3DBone hand2Bone, Model3DBone hand3Bone)
        {
            InitBones(handRotateBone, hand1Bone, hand2Bone, hand3Bone);
            InitGroupModel(handRotateBone, hand1Bone, hand2Bone, hand3Bone);

            this.hand = hand;
            this.WhenAnyValue(m => m.hand.angles)
                        .Subscribe(t => { SetAngles(-t[0], -t[1] + 90, t[2], t[3]); });
        }

        private void InitBones(params Model3DBone[] bones)
        {
            handRotateBone = bones[0];
            hand1Bone      = bones[1];
            hand2Bone      = bones[2];
            hand3Bone      = bones[3];
        }        

        public void SetAngles(double angle1, double angle2, double angle3, double angle4)
        {            
            SetAngle(angle1, handRotateBone);
            SetAngle(angle2, hand1Bone);
            SetAngle(angle3, hand2Bone);
            SetAngle(angle4, hand3Bone);
        }
    }
}
