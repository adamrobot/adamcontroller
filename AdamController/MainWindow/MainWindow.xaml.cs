﻿using HelixToolkit.Wpf.SharpDX;
using Newtonsoft.Json;
using ReactiveUI;
using ReactiveUI.Legacy;
using SharpDX;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using ReactiveUI.Fody.Helpers;
using System.Reactive.Linq;
using System.Reactive;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AdamController
{
    public partial class MainWindow : Window, IViewFor<MainWindowVM>
    {
        public static readonly DependencyProperty ViewModelProperty = DependencyProperty
            .Register(nameof(ViewModel), typeof(MainWindowVM), typeof(MainWindow));

        public MainWindowVM ViewModel
        {
            get => (MainWindowVM)GetValue(ViewModelProperty);
            set => SetValue(ViewModelProperty, value);
        }

        object IViewFor.ViewModel
        {
            get => ViewModel;
            set => ViewModel = (MainWindowVM)value;
        }

        [Reactive] public ReactiveCommand<Unit, Unit> connectCommand { get; set; }
        [Reactive] public ReactiveCommand<Unit, Unit> uploadCommand { get; set; }
        [Reactive] public ReactiveCommand<Unit, Unit> runCommand { get; set; }
        [Reactive] public ReactiveCommand<Unit, Unit> defaultCommand { get; set; }

        [Reactive] public ReactiveCommand<Unit, Unit> runAudioCommand { get; set; }
        private Func<CancellationToken, System.Threading.Tasks.Task> run;

        public MainWindow()
        {
            InitializeComponent();
            ViewModel = new MainWindowVM();
            DataContext = this;

            ////model3DView.ViewModel = ViewModel.robotModel3DVM;
            //scriptTreeV.ViewModel = new ScriptTreeVM();
            //scriptListView.Init(ScriptVM.Extension.SCRIPT);
            //Gst.Application.Init();
            ////moveScriptCreatorView.Init(ViewModel.robotVM);
            //this.WhenAnyValue(v => v.scriptTreeV.ViewModel.selectedScript)
            //    .Subscribe(t => { moveSetControl.scriptVM = t; });
            model3DView.ViewModel = new RobotModel3DVM(ViewModel.robotVM);
            moveScriptCreatorView.Init(ViewModel.robotVM);
            ledsScriptCreatorView.Init(ledsView.leftLedView.ViewModel, ledsView.rightLedView.ViewModel);
            audioView.Init(ViewModel.mainWindowM.audioClient);

            allElements.Init(wheelsStudio.WheelsV.ViewModel, ViewModel.robotVM, ledsView.leftLedView.ViewModel, ledsView.rightLedView.ViewModel, fingersStudio.LeftFingersV.ViewModel, fingersStudio.RightFingersV.ViewModel);
            InitCommands();
        }

        private void InitCommands()
        {
            InitConnectCommand();
            InitRunCommand();
            InitUploadCommand();
            //InitDefaultCommand();
            //InitRunAudioCommand();
        }

        private void InitConnectCommand()
        {
            connectCommand = ReactiveCommand.Create(
                () =>
                {
                    ViewModel.mainWindowM.robotClient.ConnectToServer();
                    ViewModel.mainWindowM.ledsClient.ConnectToServer();
                    ViewModel.mainWindowM.audioClient.ConnectToServer();
                    ViewModel.mainWindowM.wheelsClient.ConnectToServer();
                    ViewModel.mainWindowM.armsClient.ConnectToServer();
                }
                );
        }

        private void InitUploadCommand()
        {
            uploadCommand = ReactiveCommand.Create(
                () =>
                {
                    UploadServoScript();
                    UploadLedScript();
                    UploadWheelsScript();
                    UploadArmsScript();
                }
                );
        }

        private void UploadServoScript()
        {
            Message.ServoScriptScheme outScriptMsg = new Message.ServoScriptScheme();
            //outScriptMsg.units = new List<List<(int time, List<Message.ServoField> fields)>>();
            //outScriptMsg.units.Add(CreateListTF(moveScriptCreatorView.timelinesControllerView.timelines[0].ViewModel.tuvms, ViewModel.robotVM.headVM.head));
            //outScriptMsg.units.Add(CreateListTF(moveScriptCreatorView.timelinesControllerView.timelines[1].ViewModel.tuvms, ViewModel.robotVM.leftHandVM.hand));
            //outScriptMsg.units.Add(CreateListTF(moveScriptCreatorView.timelinesControllerView.timelines[2].ViewModel.tuvms, ViewModel.robotVM.rightHandVM.hand));
            //outScriptMsg.units.Add(CreateListTF(moveScriptCreatorView.timelinesControllerView.timelines[3].ViewModel.tuvms, ViewModel.robotVM.bodyVM.body));
            //outScriptMsg.units.Add(CLTFLeftLeg(moveScriptCreatorView.timelinesControllerView.timelines[4].ViewModel.tuvms, ViewModel.robotVM.leftLegVM.leg));
            //outScriptMsg.units.Add(CLTFRightLeg(moveScriptCreatorView.timelinesControllerView.timelines[4].ViewModel.tuvms, ViewModel.robotVM.rightLegVM.leg));

            ReactiveList<TimeUnitVM> tuvms = allElements.ScriptCreatorView.timelinesControllerView.timelines[0].ViewModel.tuvms;
            tuvms.Sort((x, y) => x.time.CompareTo(y.time));
            outScriptMsg.units = new List<List<(int time, List<Message.ServoField> fields)>>();
            
            outScriptMsg.units.Add(CreateListTF(tuvms, ViewModel.robotVM.headVM.head, x => {
                ((AllElementsVM)x).robotVM.headVM.head.servos = ViewModel.robotVM.headVM.head.servos;
                return ViewModel.robotVM.headVM.head.GetServoScheme(((AllElementsVM)x).robotVM.headVM.head); 
            }));
            outScriptMsg.units.Add(CreateListTF(tuvms, ViewModel.robotVM.bodyVM.body, x => {
                ((AllElementsVM)x).robotVM.bodyVM.body.servos = ViewModel.robotVM.bodyVM.body.servos;
                return ViewModel.robotVM.bodyVM.body.GetServoScheme(((AllElementsVM)x).robotVM.bodyVM.body);
            }));
            outScriptMsg.units.Add(CreateListTF(tuvms, ViewModel.robotVM.leftHandVM.hand, x => {
                ((AllElementsVM)x).robotVM.leftHandVM.hand.servos = ViewModel.robotVM.leftHandVM.hand.servos;
                return ViewModel.robotVM.leftHandVM.hand.GetServoScheme(((AllElementsVM)x).robotVM.leftHandVM.hand); 
            }));
            outScriptMsg.units.Add(CreateListTF(tuvms, ViewModel.robotVM.rightHandVM.hand, x => {
                ((AllElementsVM)x).robotVM.rightHandVM.hand.servos = ViewModel.robotVM.rightHandVM.hand.servos;
                return ViewModel.robotVM.rightHandVM.hand.GetServoScheme(((AllElementsVM)x).robotVM.rightHandVM.hand); 
            }));
            outScriptMsg.units.Add(CreateListTF(tuvms, ViewModel.robotVM.leftLegVM.leg, x => {
                ((AllElementsVM)x).robotVM.leftLegVM.leg.servos = ViewModel.robotVM.leftLegVM.leg.servos;
                return ViewModel.robotVM.leftLegVM.leg.GetServoScheme(((AllElementsVM)x).robotVM.leftLegVM.leg);
            }));
            outScriptMsg.units.Add(CreateListTF(tuvms, ViewModel.robotVM.rightLegVM.leg, x => { 
                ((AllElementsVM)x).robotVM.rightLegVM.leg.servos = ViewModel.robotVM.rightLegVM.leg.servos;
                return ViewModel.robotVM.rightLegVM.leg.GetServoScheme(((AllElementsVM)x).robotVM.leftLegVM.leg);
            }));

            ViewModel.mainWindowM.robotClient.SendData(outScriptMsg.Msg(Message.Command.UPLOADSERVOSCRIPT).GetData());
        }

        private void UploadLedScript()
        {
            Message.LedScriptScheme outLedScriptMsg = new Message.LedScriptScheme();
            //outLedScriptMsg.units = new List<List<(int time, LedVM.Type type, List<Message.LedField> fields)>>();
            //outLedScriptMsg.units.Add(CreateListTF(ledsScriptCreatorView.timelinesControllerView.timelines[0].ViewModel.tuvms, ledsView.leftLedView.ViewModel));
            //outLedScriptMsg.units.Add(CreateListTF(ledsScriptCreatorView.timelinesControllerView.timelines[1].ViewModel.tuvms, ledsView.rightLedView.ViewModel));

            ReactiveList<TimeUnitVM> tuvms = allElements.ScriptCreatorView.timelinesControllerView.timelines[0].ViewModel.tuvms;
            tuvms.Sort((x, y) => x.time.CompareTo(y.time));
            outLedScriptMsg.units = new List<List<(int time, LedVM.Type type, List<Message.LedField> fields)>>();

            outLedScriptMsg.units.Add(CreateListTF(tuvms, allElements.ledsView.leftLedView.ViewModel, x => {
                return ((AllElementsVM)x).leftLedVM.GetScheme();
            }));
            outLedScriptMsg.units.Add(CreateListTF(tuvms, allElements.ledsView.rightLedView.ViewModel, x => {
                return ((AllElementsVM)x).rightLedVM.GetScheme();
            }));

            ViewModel.mainWindowM.ledsClient.SendData(outLedScriptMsg.Msg(Message.Command.UPLOADLEDSCRIPT).GetData());
        }

        private void UploadWheelsScript()
        {
            Message.WheelScriptScheme outWheelScriptMsg = new Message.WheelScriptScheme();
            //outWheelScriptMsg.units = new List<List<(int time, WheelVM.WheelID wheelID, List<Message.WheelField> fields)>>();
            //outWheelScriptMsg.units.Add(CreateListTF(wheelsStudio.WheelsScriptCreatorView.timelinesControllerView.timelines[0].ViewModel.tuvms, wheelsStudio.WheelsV.ViewModel.LeftForwardWheelVM));
            //outWheelScriptMsg.units.Add(CreateListTF(wheelsStudio.WheelsScriptCreatorView.timelinesControllerView.timelines[1].ViewModel.tuvms, wheelsStudio.WheelsV.ViewModel.RightForwardWheelVM));
            //outWheelScriptMsg.units.Add(CreateListTF(wheelsStudio.WheelsScriptCreatorView.timelinesControllerView.timelines[2].ViewModel.tuvms, wheelsStudio.WheelsV.ViewModel.LeftBackWheelVM));
            //outWheelScriptMsg.units.Add(CreateListTF(wheelsStudio.WheelsScriptCreatorView.timelinesControllerView.timelines[3].ViewModel.tuvms, wheelsStudio.WheelsV.ViewModel.RightBackWheelVM));

            ReactiveList<TimeUnitVM> tuvms = allElements.ScriptCreatorView.timelinesControllerView.timelines[0].ViewModel.tuvms;
            tuvms.Sort((x, y) => x.time.CompareTo(y.time));
            //tuvms.Sort((emp1, emp2) => emp1.time > emp2.time);
            outWheelScriptMsg.units = new List<List<(int time, WheelVM.WheelID wheelID, List<Message.WheelField> fields)>>();

            outWheelScriptMsg.units.Add(CreateListTF(tuvms, allElements.WheelsV.ViewModel.LeftForwardWheelVM, x => {
                return ((AllElementsVM)x).wheelsVM.LeftForwardWheelVM.GetScheme();
            }));
            outWheelScriptMsg.units.Add(CreateListTF(tuvms, allElements.WheelsV.ViewModel.RightForwardWheelVM, x => {
                return ((AllElementsVM)x).wheelsVM.RightForwardWheelVM.GetScheme();
            }));
            outWheelScriptMsg.units.Add(CreateListTF(tuvms, allElements.WheelsV.ViewModel.LeftBackWheelVM, x => {
                return ((AllElementsVM)x).wheelsVM.LeftBackWheelVM.GetScheme();
            }));
            outWheelScriptMsg.units.Add(CreateListTF(tuvms, allElements.WheelsV.ViewModel.RightBackWheelVM, x => {
                return ((AllElementsVM)x).wheelsVM.RightBackWheelVM.GetScheme();
            }));

            ViewModel.mainWindowM.wheelsClient.SendData(outWheelScriptMsg.Msg(Message.Command.UPLOADWHEELSSCRIPT).GetData());
        }
        private void UploadArmsScript()
        {
            Message.FingersScriptScheme outScriptMsg = new Message.FingersScriptScheme();
            //outScriptMsg.units = new List<List<(int time, FingersVM.FingersID fingersID, List<Message.FingersField> fields)>>();
            //outScriptMsg.units.Add(CreateListTF(fingersStudio.FingersScriptCreatorView.timelinesControllerView.timelines[0].ViewModel.tuvms, fingersStudio.LeftFingersV.ViewModel));
            //outScriptMsg.units.Add(CreateListTF(fingersStudio.FingersScriptCreatorView.timelinesControllerView.timelines[1].ViewModel.tuvms, fingersStudio.RightFingersV.ViewModel));

            ReactiveList<TimeUnitVM> tuvms = allElements.ScriptCreatorView.timelinesControllerView.timelines[0].ViewModel.tuvms;
            tuvms.Sort((x, y) => x.time.CompareTo(y.time));
            outScriptMsg.units = new List<List<(int time, FingersVM.FingersID fingersID, List<Message.FingersField> fields)>>();

            outScriptMsg.units.Add(CreateListTF(tuvms, allElements.LeftFingersV.ViewModel, x => {
                return ((AllElementsVM)x).leftFingersVM.GetScheme();
            }));
            outScriptMsg.units.Add(CreateListTF(tuvms, allElements.RightFingersV.ViewModel, x => {
                return ((AllElementsVM)x).rightFingersVM.GetScheme();
            }));

            ViewModel.mainWindowM.armsClient.SendData(outScriptMsg.Msg(Message.Command.UPLOADARMSSCRIPT).GetData());
        }

        private void InitRunCommand()
        {
            runCommand = ReactiveCommand.Create(
                () =>
                {
                    Message.RunScriptScheme outScriptMsg = new Message.RunScriptScheme();
                    ViewModel.mainWindowM.robotClient.SendData(outScriptMsg.Msg(Message.Command.RUNSERVOSCRIPT).GetData());
                    ViewModel.mainWindowM.ledsClient.SendData(outScriptMsg.Msg(Message.Command.RUNLEDSCRIPT).GetData());
                    ViewModel.mainWindowM.wheelsClient.SendData(outScriptMsg.Msg(Message.Command.RUNWHEELSSCRIPT).GetData());
                    ViewModel.mainWindowM.armsClient.SendData(outScriptMsg.Msg(Message.Command.RUNARMSSSCRIPT).GetData());

                    //runAudioCommand.Execute();
                }
                );
        }

        private List<(int time, List<Message.ServoField> fields)> CreateListTF(ReactiveList<TimeUnitVM> tuvms, Robot.Unit unit)
        {
            var listTF = new List<(int time, List<Message.ServoField> fields)>();
            foreach (var tuvm in tuvms)
            {
                ((Robot.Unit)tuvm.timeM).servos = unit.servos;
                var scheme = unit.GetServoScheme((Robot.Unit)tuvm.timeM);
                var fields = new List<Message.ServoField>();
                for (var i = 0; i < scheme.ids.Length; i++)
                    fields.Add(new Message.ServoField(scheme.ids[i], scheme.data[i].OfType<byte>().ToList(), scheme.size, scheme.addr));
                listTF.Add((tuvm.time, fields));
            }
            return listTF;
        }

        private List<(int time, List<Message.ServoField> fields)> CreateListTF(ReactiveList<TimeUnitVM> tuvms, Robot.Unit unit, Func<ITime, Message.ServoScheme> getScheme)
        {
            var listTF = new List<(int time, List<Message.ServoField> fields)>();
            foreach (var tuvm in tuvms)
            {
                //((Robot.Unit)tuvm.timeM).servos = unit.servos;
                var scheme = getScheme(tuvm.timeM);
                var fields = new List<Message.ServoField>();
                for (var i = 0; i < scheme.ids.Length; i++)
                    fields.Add(new Message.ServoField(scheme.ids[i], scheme.data[i].OfType<byte>().ToList(), scheme.size, scheme.addr));
                listTF.Add((tuvm.time, fields));
            }
            return listTF;
        }

        private List<(int time, List<Message.ServoField> fields)> CLTFLeftLeg(ReactiveList<TimeUnitVM> tuvms, Robot.Unit unit)
        {
            var listTF = new List<(int time, List<Message.ServoField> fields)>();
            foreach (var tuvm in tuvms)
            {
                ((Robot.Legs)tuvm.timeM).leftLegVM.leg.servos = unit.servos;
                var scheme = unit.GetServoScheme(((Robot.Legs)tuvm.timeM).leftLegVM.leg);
                var fields = new List<Message.ServoField>();
                for (var i = 0; i < scheme.ids.Length; i++)
                    fields.Add(new Message.ServoField(scheme.ids[i], scheme.data[i].OfType<byte>().ToList(), scheme.size, scheme.addr));
                listTF.Add((tuvm.time, fields));
            }
            return listTF;
        }

        private List<(int time, List<Message.ServoField> fields)> CLTFRightLeg(ReactiveList<TimeUnitVM> tuvms, Robot.Unit unit)
        {
            var listTF = new List<(int time, List<Message.ServoField> fields)>();
            foreach (var tuvm in tuvms)
            {
                ((Robot.Legs)tuvm.timeM).rightLegVM.leg.servos = unit.servos;
                var scheme = unit.GetServoScheme(((Robot.Legs)tuvm.timeM).rightLegVM.leg);
                var fields = new List<Message.ServoField>();
                for (var i = 0; i < scheme.ids.Length; i++)
                    fields.Add(new Message.ServoField(scheme.ids[i], scheme.data[i].OfType<byte>().ToList(), scheme.size, scheme.addr));
                listTF.Add((tuvm.time, fields));
            }
            return listTF;
        }

        private List<(int time, LedVM.Type type, List<Message.LedField> fields)> CreateListTF(ReactiveList<TimeUnitVM> tuvms, LedVM unit)
        {
            var listTF = new List<(int time, LedVM.Type type, List<Message.LedField> fields)>();
        
            foreach (var tuvm in tuvms)
            {
                var scheme = ((LedVM)tuvm.timeM).GetScheme();
                var fields = new List<Message.LedField>();
                for (var i = 0; i < scheme.startDiodes.Count(); i++)
                    fields.Add(new Message.LedField(scheme.startDiodes[i], scheme.endDiodes[i], scheme.colors[i]));
                listTF.Add((tuvm.time, unit.type, fields));
            }
        
            return listTF;
        }

        private List<(int time, LedVM.Type type, List<Message.LedField> fields)> CreateListTF(ReactiveList<TimeUnitVM> tuvms, LedVM unit, Func<ITime, Message.LedScheme> getScheme)
        {
            var listTF = new List<(int time, LedVM.Type type, List<Message.LedField> fields)>();

            foreach (var tuvm in tuvms)
            {
                var scheme = getScheme(tuvm.timeM);// ((LedVM)tuvm.timeM).GetScheme();
                var fields = new List<Message.LedField>();
                for (var i = 0; i < scheme.startDiodes.Count(); i++)
                    fields.Add(new Message.LedField(scheme.startDiodes[i], scheme.endDiodes[i], scheme.colors[i]));
                listTF.Add((tuvm.time, unit.type, fields));
            }

            return listTF;
        }

        private List<(int time, WheelVM.WheelID id, List<Message.WheelField> fields)> CreateListTF(ReactiveList<TimeUnitVM> tuvms, WheelVM unit)
        {
            var listTF = new List<(int time, WheelVM.WheelID id, List<Message.WheelField> fields)>();

            foreach (var tuvm in tuvms)
            {
                var scheme = ((WheelVM)tuvm.timeM).GetScheme();
                var fields = new List<Message.WheelField>();
                fields.Add(new Message.WheelField(scheme.speed, unit.id));
                listTF.Add((tuvm.time, unit.id, fields));
            }

            return listTF;
        }

        private List<(int time, WheelVM.WheelID id, List<Message.WheelField> fields)> CreateListTF(ReactiveList<TimeUnitVM> tuvms, WheelVM unit, Func<ITime, Message.WheelScheme> getScheme)
        {
            var listTF = new List<(int time, WheelVM.WheelID id, List<Message.WheelField> fields)>();

            foreach (var tuvm in tuvms)
            {
                var scheme = getScheme(tuvm.timeM);
                var fields = new List<Message.WheelField>();
                fields.Add(new Message.WheelField(scheme.speed, unit.id));
                listTF.Add((tuvm.time, unit.id, fields));
            }

            return listTF;
        }

        private List<(int time, FingersVM.FingersID id, List<Message.FingersField> fields)> CreateListTF(ReactiveList<TimeUnitVM> tuvms, FingersVM unit)
        {
            var listTF = new List<(int time, FingersVM.FingersID id, List<Message.FingersField> fields)>();

            foreach (var tuvm in tuvms)
            {
                var scheme = ((FingersVM)tuvm.timeM).GetScheme();
                var fields = new List<Message.FingersField>();
                fields.Add(new Message.FingersField(scheme.angle, unit.id));
                listTF.Add((tuvm.time, unit.id, fields));
            }

            return listTF;
        }

        private List<(int time, FingersVM.FingersID id, List<Message.FingersField> fields)> CreateListTF(ReactiveList<TimeUnitVM> tuvms, FingersVM unit, Func<ITime, Message.FingersScheme> getScheme)
        {
            var listTF = new List<(int time, FingersVM.FingersID id, List<Message.FingersField> fields)>();

            foreach (var tuvm in tuvms)
            {
                var scheme = getScheme(tuvm.timeM); //((FingersVM)tuvm.timeM).GetScheme();
                var fields = new List<Message.FingersField>();
                fields.Add(new Message.FingersField(scheme.angle, unit.id));
                listTF.Add((tuvm.time, unit.id, fields));
            }

            return listTF;
        }
    }
}
