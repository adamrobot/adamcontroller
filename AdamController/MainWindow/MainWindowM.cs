﻿using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using System.Reactive.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Reactive;

namespace AdamController
{
    public class MainWindowM : ReactiveObject
    {
        public const int robotClientPort  = 5000;
        public const int ledsClientPort   = 6100;
        public const int audioClientPort  = 7000;
        public const int wheelsClientPort = 6200;
        public const int armsClientPort   = 6300;

        [Reactive] public AdamTcpClient robotClient { get; set; }
        [Reactive] public AdamUdpClient armsClient { get; set; }
        [Reactive] public AdamTcpClient ledsClient { get; set; }
        [Reactive] public AdamTcpClient audioClient { get; set; }
        [Reactive] public AdamUdpClient wheelsClient { get; set; }

        //private MoveScriptWindow moveScriptWindow;        

        [Reactive] public string address { get; set; } = "192.168.1.121";
    }
}
