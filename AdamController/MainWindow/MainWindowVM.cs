﻿using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using System.Reactive.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reactive;
using System.Threading;
using System.Diagnostics;
using System.Windows.Threading;
using Newtonsoft.Json;

namespace AdamController
{
    public class MainWindowVM : ReactiveObject
    {
        public ScriptView sv { get; set; }

        int scriptSize = 100;
        [Reactive] public MainWindowM mainWindowM { get; set; }

        [Reactive] public Robot.RobotVM robotVM { get; set; }
        //[Reactive] public RobotModel3DVM robotModel3DVM { get; set; }
        [Reactive] public ReactiveCommand<Unit, Unit> connectCommand { get; set; }
        [Reactive] public ReactiveCommand<Unit, Unit> uploadCommand { get; set; }
        [Reactive] public ReactiveCommand<Unit, Unit> runCommand { get; set; }
        [Reactive] public ReactiveCommand<Unit, Unit> defaultCommand { get; set; }

        [Reactive] public ReactiveCommand<Unit, Unit> runAudioCommand { get; set; }
        private Func<CancellationToken, System.Threading.Tasks.Task> run;

        public MainWindowVM()
        {
            mainWindowM = new MainWindowM();

            InitRobotClient();
            InitRobotVM();
            InitControlPanel();
            InitCtController();
            InitMoveSetControl();
            InitMoveScriptCreatorView();
            InitModel3DView();
            //InitCommands();
            InitVRView();
            InitLedsClient();
            InitArmsClient();
            //InitLedsView();
            //InitLedsScriptCreatorView();
            InitAudioClient();
            //InitAudioView();
            //InitAuidoScriptCreatorView();
            InitWheelsClient();


            //InitScriptWindows();

            //robotModel3DVM = new RobotModel3DVM(robotVM);
        }

        //private void InitScriptWindows()
        //{
        //    moveScriptWindow = new MoveScriptWindow(robot);
        //    moveScriptWindow.Show();
        //        //Window1 w1;
        //        //w1 = new Window1();
        //        //w1.model3DView.Update(robot.viewModel);
        //        //robot.updatedVM += w1.model3DView.Update;
        //        //w1.Show();
        //}

        private void InitAudioClient()
        {
            mainWindowM.audioClient = new AdamTcpClient(MainWindowM.audioClientPort);
            mainWindowM.audioClient.address = mainWindowM.address;
        }

        //private void InitAudioView()
        //{
        //    audioView.Init(audioClient);
        //}
        //
        //private void InitAuidoScriptCreatorView()
        //{
        //    audioScriptCreatorView.Init(audioView.ViewModel);
        //}
        //
        //private void InitLedsScriptCreatorView()
        //{
        //    ledsScriptCreatorView.Init(ledsView.leftLedView.ViewModel, ledsView.rightLedView.ViewModel); //need to change
        //}

        private void InitLedsClient()
        {
            mainWindowM.ledsClient = new AdamTcpClient(MainWindowM.ledsClientPort);

            //robotClient.conntected += InitRobotServos;
            //robotClient.conntected += InitServoModelNumber;
            //robotClient.blockReaded += UpdateServoMsgs;

            //ledClient.address = "192.168.31.152";
            mainWindowM.ledsClient.address = mainWindowM.address;
        }

        private void InitArmsClient()
        {
            mainWindowM.armsClient = new AdamUdpClient(MainWindowM.armsClientPort);
            mainWindowM.armsClient.address = mainWindowM.address;
        }

        private void InitWheelsClient()
        {
            mainWindowM.wheelsClient = new AdamUdpClient(MainWindowM.wheelsClientPort);
            mainWindowM.wheelsClient.address = mainWindowM.address;
            WheelsVM.client = mainWindowM.wheelsClient;
        }

        //private void InitLedsView()
        //{
        //    ledsView.Init(ledsClient);
        //}

        private void InitVRView()
        {
            //vrView.Init(robot);
            //vrView.Start();
        }

        //private void InitCommands()
        //{
        //    InitConnectCommand();
        //    InitRunCommand();
        //    InitUploadCommand();
        //    //InitDefaultCommand();
        //    //InitRunAudioCommand();
        //}

        private bool IsOutdated(Stopwatch stopwatch, int time)
        {
            stopwatch.Stop();
            var ts = stopwatch.Elapsed;
            stopwatch.Start();
            return ts.TotalMilliseconds > time;
        }

        private void InitModel3DView()
        {
            //model3DView.Update(robot.viewModel);
            //robot.updatedVM += model3DView.Update;

            //Window1 w1;
            //w1 = new Window1();
            //w1.model3DView.Update(robot.viewModel);
            //robot.updatedVM += w1.model3DView.Update;
            //w1.Show();
        }

        private void InitMoveScriptCreatorView()
        {
            //moveScriptCreatorView.Init(robotVM);
        }

        private void InitMoveSetControl()
        {
            //moveSetControl.Init(robot);
        }

        private void InitRobotClient()
        {
            mainWindowM.robotClient = new AdamTcpClient(MainWindowM.robotClientPort);

            mainWindowM.robotClient.connected += InitRobotServos;
            mainWindowM.robotClient.connected += InitServoModelNumber;
            //mainWindowM.robotClient.blockReaded += UpdateServoMsgs;

            mainWindowM.robotClient.address = mainWindowM.address;
        }

        private void InitRobotServos()
        {
            //robot.head.SetSlope(new byte[2] { 128, 128 });
            //robot.body.SetSlope(new byte[2] { 128, 128 });
            //robot.leftHand.SetSlope(new byte[2] { 128, 128 });
            //robot.rightHand.SetSlope(new byte[2] { 128, 128 });

            //robotVM.headVM.head.SetTorqueLimit(1023);
            //robotVM.bodyVM.body.SetTorqueLimit(1023);
            //robotVM.leftHandVM.hand.SetTorqueLimit(1023);
            //robotVM.rightHandVM.hand.SetTorqueLimit(1023);
            //robotVM.leftLegVM.leg.SetTorqueLimit(1023);
            //robotVM.rightLegVM.leg.SetTorqueLimit(1023);

            robotVM.headVM.head.SetSlope(new byte[2] { 128, 128 });
            robotVM.bodyVM.body.SetPID(new byte[3] { 10, 0, 0 });
            robotVM.leftHandVM.hand.SetPID(new byte[3] { 8, 0, 0 });
            robotVM.rightHandVM.hand.SetPID(new byte[3] { 8, 0, 0 });
            robotVM.leftLegVM.leg.SetPID(new byte[3] { 8, 0, 0 });
            robotVM.rightLegVM.leg.SetPID(new byte[3] { 8, 0, 0 });
            //robotVM.leftHandVM.hand.SetTorqueStatus(0);
            //robotVM.bodyVM.body.SetTorqueStatus(0);
            //robotVM.leftLegVM.leg.SetTorqueStatus(1);
            //robotVM.rightLegVM.leg.SetTorqueStatus(1);
        }

        private void InitServoModelNumber()
        {
            //foreach (ServoID servoId in Enum.GetValues(typeof(ServoID)))
            //    Servo.servos[servoId].SendInitModelNumberMsg(mainWindowM.robotClient);
            //Servo.servos[ServoID.RightLeg2].SendInitModelNumberMsg(ref robotClient);
        }

        private void UpdateServoMsgs()
        {
            List<byte> data = new List<byte>(mainWindowM.robotClient.currentBlock);
            //Dispatcher.BeginInvoke(new Action(delegate
            //{
            //    string json = Encoding.UTF8.GetString(data.ToArray());
            //    Message.Message msg = JsonConvert.DeserializeObject<Message.Message>(json, Message.Message.jsonSettings);
            //    Servo servo = Servo.servos[(ServoID)msg.id];
            //    servo.field = (Message.ServoField)msg.fields.Values.ElementAt(0);
            //    if (servo.modelNumber == null)
            //    {
            //        servo.InitModelNumber();
            //        //servo.SendStartRepeatMsg(ref robotClient);
            //    }
            //}));
            string json = Encoding.UTF8.GetString(data.ToArray());
            Message.Message msg = JsonConvert.DeserializeObject<Message.Message>(json, Message.Message.jsonSettings);
            Servo servo = Servo.servos[(ServoID)msg.id];
            servo.field = (Message.ServoField)msg.fields.Values.ElementAt(0);
            if (servo.modelNumber == null)
            {
                servo.InitModelNumber();
                //servo.SendStartRepeatMsg(ref robotClient);
            }
        }

        private void InitCtController()
        {
            ControlTable.ControlTable.CtController ctController = ControlTable.ControlTable.CtController.Instance;
        }

        private void InitRobotVM()
        {
            robotVM = new Robot.RobotVM();
            Robot.Unit.client = mainWindowM.robotClient;
            //robot.cathedException += CathedException;
        }

        private void InitControlPanel()
        {
            //allControl.Update(robot.viewModel);
            //robot.updatedVM += allControl.Update;
        }
    }


}
