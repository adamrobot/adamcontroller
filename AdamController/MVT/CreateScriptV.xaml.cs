﻿using HelixToolkit.Wpf.SharpDX;
using Newtonsoft.Json;
using ReactiveUI;
using ReactiveUI.Legacy;
using SharpDX;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using ReactiveUI.Fody.Helpers;
using System.Reactive.Linq;
using System.Reactive;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AdamController
{
    public partial class CreateScriptV : Window, IViewFor<CreateScriptVM>
    {
        public static readonly DependencyProperty ViewModelProperty = DependencyProperty
            .Register(nameof(ViewModel), typeof(CreateScriptVM), typeof(CreateScriptV));

        public CreateScriptVM ViewModel
        {
            get => (CreateScriptVM)GetValue(ViewModelProperty);
            set => SetValue(ViewModelProperty, value);
        }

        object IViewFor.ViewModel
        {
            get => ViewModel;
            set => ViewModel = (CreateScriptVM)value;
        }

        private TimelinesControllerView tcv = null;
        private UIElement element = null;

        public CreateScriptV()
        {
            InitializeComponent();
            ViewModel = new CreateScriptVM();
            DataContext = ViewModel;

            //ViewModel = new MoveControlVM(new Robot.RobotVM());
            foreach (ScriptVM.Extension extension in Enum.GetValues(typeof(ScriptVM.Extension)))
                typeCB.Items.Add(extension);
            typeCB.Items.Remove(ScriptVM.Extension.SCRIPT);
            typeCB.Items.Remove(ScriptVM.Extension.ARROW);
        }

        private void TypeCBSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (tcv != null)
                grid.Children.Remove(tcv);

            if (element != null)
                grid.Children.Remove(element);

            ScriptVM.Extension extension = (ScriptVM.Extension)typeCB.SelectedItem;
            
            switch (extension)
            {
                case ScriptVM.Extension.MOVEMENT:
                    {
                        Model3DView m3dv = new Model3DView();
                        Robot.RobotVM rvm = new Robot.RobotVM();
                        m3dv.ViewModel = new RobotModel3DVM(rvm);
                        element = m3dv;

                        tcv = new TimelinesControllerView();
                        tcv.Init(rvm, (ScriptVM.Extension)typeCB.SelectedItem);

                        break;
                    }
                case ScriptVM.Extension.AUDIO:
                    {
                        AudioView av = new AudioView();
                        av.Init(new AdamTcpClient(1000));
                        element = av;

                        tcv = new TimelinesControllerView();
                        tcv.Init(av.ViewModel, (ScriptVM.Extension)typeCB.SelectedItem);

                        break;
                    }
                case ScriptVM.Extension.LED:
                    {
                        LedsView lsv = new LedsView();
                        lsv.Init(new AdamTcpClient(1000));
                        element = lsv;

                        tcv = new TimelinesControllerView();
                        tcv.Init(lsv.leftLedView.ViewModel, lsv.rightLedView.ViewModel, (ScriptVM.Extension)typeCB.SelectedItem);

                        break;
                    }
                case ScriptVM.Extension.WHEELS:
                    {
                        WheelsVM av = new WheelsVM();
                        //av.Init(new AdamTcpClient(1000));
                        //element = av;

                        tcv = new TimelinesControllerView();
                        tcv.Init(av, (ScriptVM.Extension)typeCB.SelectedItem);

                        break;
                    }
            }

            if (tcv == null || element == null)
                return;

            Grid.SetColumn(tcv, 0);
            Grid.SetColumnSpan(tcv, 3);
            Grid.SetRow(tcv, 2);
            grid.Children.Add(tcv);

            Grid.SetColumn(element, 0);
            Grid.SetColumnSpan(element, 3);
            Grid.SetRow(element, 1);
            grid.Children.Add(element);
        }
    }
}
