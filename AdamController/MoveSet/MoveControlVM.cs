﻿using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using System;
using System.Collections.Generic;
using System.Reactive.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdamController
{
    public class MoveControlVM : ReactiveObject
    {
        [Reactive] public Coord2 coord { get; set; } = new Coord2();

        private readonly ObservableAsPropertyHelper<Coord2> _inCoord;
        public Coord2 inCoord => _inCoord.Value;

        private readonly ObservableAsPropertyHelper<Coord2> _outCoord;
        public Coord2 outCoord => _outCoord.Value;

        [Reactive] public string MySource { get; set; } = "/Images/audio.png"; // must change
        [Reactive] public string value { get; set; } = ""; // must change
        [Reactive] public string type { get; set; }

        [Reactive] public double width { get; set; } = 70; // must change
        [Reactive] public double height { get; set; } = 70; // must change
        [Reactive] public bool isHighlighted { get; set; } = false;

        [Reactive] public ScriptVM scriptVM { get; set; }
        [Reactive] public ActionVM actionVM { get; set; }

        public MoveControlVM(ScriptVM scriptVM)
        {
            this.scriptVM = scriptVM;

            _inCoord = this.WhenAnyValue(vm => vm.coord, vm => vm.width, vm => vm.height)
                .Select(t => new Coord2(t.Item1.x, t.Item1.y + t.Item3 / 2))
                .ToProperty(this, vm => vm.inCoord);

            _outCoord = this.WhenAnyValue(vm => vm.coord, vm => vm.width, vm => vm.height)
                .Select(t => new Coord2(t.Item1.x + t.Item2, t.Item1.y + t.Item3 / 2))
                .ToProperty(this, vm => vm.outCoord);

            this.WhenAnyValue(v => v.scriptVM)
                .Subscribe(t => {
                    if (t != null)
                        type = ScriptVM.extensions[t.extension]; });
        }
    }
}
