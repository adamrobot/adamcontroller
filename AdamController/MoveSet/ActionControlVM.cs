﻿using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using System;
using System.Collections.Generic;
using System.Reactive.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdamController
{
    public class ActionControlVM : ReactiveObject
    {
        //public MoveControl fromMC { get; set; }
        //public MoveControl toMC { get; set; }
        //private MoveControl _toMC;
        //public MoveControl toMC
        //{
        //    get { return _toMC; }
        //    set
        //    {
        //        _toMC = value;
        //        ReBind((line, "X2", "toMC.inCoord.x"), (line, "Y2", "toMC.inCoord.y"));
        //    }
        //}

        [Reactive] public MoveControlVM fromMC { get; set; }

        [Reactive] public MoveControlVM toMC { get; set; }
        [Reactive] public bool isHighlighted { get; set; } = false;
        [Reactive] public ActionVM actionVM { get; set; } = new ActionVM();
        public MoveControlVM defaultMC { get; private set; } = new MoveControlVM(null);

        public ActionControlVM(MoveControlVM fromMC, MoveControlVM toMC = null)
        {
            this.fromMC = fromMC;
            if (toMC == null) this.toMC = defaultMC;
            else this.toMC = toMC;
        }

        public void SetCoord(Coord2 coord)
        {
            defaultMC.height = 0;
            defaultMC.width = 0;

            defaultMC.coord = coord;
        }
    }
}
