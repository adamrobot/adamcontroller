﻿using ReactiveUI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reactive.Disposables;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;

namespace AdamController
{
    public partial class ActionControl : UserControl, IViewFor<ActionControlVM>
    {
        public static readonly DependencyProperty ViewModelProperty = DependencyProperty
            .Register(nameof(ViewModel), typeof(ActionControlVM), typeof(ActionControl));

        public ActionControl(MoveControlVM fromMC, MoveControlVM toMC = null)
        {
            InitializeComponent();
            DataContext = this;
            ViewModel = new ActionControlVM(fromMC, toMC);

            //this.WhenActivated(disposable =>
            //{
            //    this.Bind(ViewModel, x => x.fromMC.outCoord.x, x => x.line.X1)
            //        .DisposeWith(disposable);
            //    this.Bind(ViewModel, x => x.fromMC.outCoord.y, x => x.line.Y1)
            //        .DisposeWith(disposable);
            //    this.Bind(ViewModel, x => x.toMC.inCoord.x, x => x.line.X2)
            //        .DisposeWith(disposable);
            //    this.Bind(ViewModel, x => x.toMC.inCoord.y, x => x.line.Y2)
            //        .DisposeWith(disposable);
            //});
        }

        public ActionControlVM ViewModel
        {
            get => (ActionControlVM)GetValue(ViewModelProperty);
            set => SetValue(ViewModelProperty, value);
        }

        object IViewFor.ViewModel
        {
            get => ViewModel;
            set => ViewModel = (ActionControlVM)value;
        }
    }
}
