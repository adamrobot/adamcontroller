﻿using ReactiveUI;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Reactive.Linq;
using System.Reactive;
using System.Reactive.Subjects;

namespace AdamController
{
    public partial class MoveSetControl : UserControl
    {
        public ObservableCollection<MoveControl> moves { get; set; } = new ObservableCollection<MoveControl>();
        public ObservableCollection<ActionControl> actions { get; set; } = new ObservableCollection<ActionControl>();

        private Dictionary<MoveControlVM, ActionControlVM> setup = new Dictionary<MoveControlVM, ActionControlVM>();

        private Coord2 offsetMC;
        private MoveControl selectedElement = new MoveControl(new ScriptVM(ScriptVM.Extension.MOVEMENT));
        private ActionControl selectedAC = new ActionControl(null, null);
        //private Robot.RobotVM robotVM;
        public ScriptVM scriptVM { get; set; } = null;

        public ReactiveCommand<Unit, Unit> stop { get; set; }
        public ReactiveCommand<Unit, Unit> run { get; set; }
        private Func<CancellationToken, Task> step;

        public MoveSetControl()
        {
            InitializeComponent();
            DataContext = this;            

            MoveControl start = new MoveControl(new ScriptVM(ScriptVM.Extension.MOVEMENT));
            //mc.viewModel.robotVM.bodyVM.height = 19;
            Point a = new Point(50, 50);
            start.ViewModel.coord = a;
            //mc.x = a.X;
            //mc.y = a.Y;    
            BindMC(start);
            moves.Add(start);
            observer.ViewModel = start.ViewModel;

            selectedElement.ViewModel.isHighlighted = false;
            selectedElement = start;
            selectedElement.ViewModel.isHighlighted = true;

            start.Background = Brushes.Red;

            InitCommands();
        }

        private void InitCommands()
        {
            run = ReactiveCommand.CreateFromObservable(
                () => Observable.StartAsync(ct => step(ct))
                    .TakeUntil(stop));

            stop = ReactiveCommand.Create(
                () => { },
                run.IsExecuting);

            step = async (ct) =>
            {
                if (ct.IsCancellationRequested)
                    return;
                //ts = stopWatch.Elapsed;
                Console.WriteLine(5);
            };
        }

        //public void Init(Robot.RobotVM robotVM)
        //{
        //    this.robotVM = robotVM;
        //}

        public MoveControl GetCurrentMC(Point position)
        {            
            var element = grid.InputHitTest(position) as FrameworkElement;

            while (element.Parent != null) element = element.Parent as FrameworkElement; // may be to change

            return element as MoveControl;
        }

        private void ACMouseMove(object sender, MouseEventArgs e)
        {
            if (Mouse.Captured == null) return;
            if (Mouse.RightButton != MouseButtonState.Pressed) return;

            var action = e.Source as ActionControl;
            var position = e.GetPosition(grid);
            action.ViewModel.SetCoord(position);

            //if (GetCurrentMC(position) is var mc && mc != null) mc.viewModel.isHighlighted = true;
        }

        private void ACMouseDown(object sender, MouseEventArgs e)
        {
            if (Mouse.LeftButton == MouseButtonState.Pressed)
            {
                selectedAC.ViewModel.isHighlighted = false;
                selectedAC = e.Source as ActionControl;
                selectedElement.ViewModel.isHighlighted = false;
                selectedAC.ViewModel.isHighlighted = true;
                observer.ViewModel = new MoveControlVM(new ScriptVM(ScriptVM.Extension.MOVEMENT));
                observer.ViewModel.actionVM = selectedAC.ViewModel.actionVM;
                Mouse.Capture(selectedAC);
            }
            else if (Mouse.MiddleButton == MouseButtonState.Pressed)
            {
                var ac = e.Source as ActionControl;
                actions.Remove(ac);
            }
        }

        private void ACMouseUp(object sender, MouseEventArgs e)
        {
            //if (Mouse.RightButton != MouseButtonState.Pressed) return;

            var action = e.Source as ActionControl;
            var position = e.GetPosition(grid);

            if (GetCurrentMC(position) is var mc && mc != null)
                action.ViewModel.toMC = mc.ViewModel;
            else if (action.ViewModel.toMC == null)
                actions.Remove(action);

            Mouse.Capture(null);
        }

        private void MCMouseMove(object sender, MouseEventArgs e)
        {
            if (Mouse.Captured == null) return;
            if (Mouse.LeftButton != MouseButtonState.Pressed) return;

            var mc = e.Source as MoveControl;
            var pos = e.GetPosition(grid);
            mc.ViewModel.coord = pos - (Point)offsetMC;
        }

        private void MCMouseDown(object sender, MouseEventArgs e)
        {
            if (Mouse.Captured != null) return;
            if (Mouse.RightButton == MouseButtonState.Pressed)
            {
                Point pos = e.GetPosition(grid);
                var move = e.Source as MoveControl;

                ActionControl action = new ActionControl(move.ViewModel, null);
                setup[move.ViewModel] = action.ViewModel;
                //action.from = new Coord2(move.coord.x + move.ActualWidth, move.coord.y + move.ActualHeight / 2);
                //action.to = pos;
                action.ViewModel.SetCoord(pos);
                BindAC(action);

                actions.Add(action);

                Mouse.Capture(action);
            }
            else if (Mouse.LeftButton == MouseButtonState.Pressed)
            {
                var mc = e.Source as MoveControl;
                var tmp = e.GetPosition(grid);
                offsetMC = e.GetPosition(grid) - (Point)mc.ViewModel.coord;

                selectedElement.ViewModel.isHighlighted = false;
                if (selectedAC != null)
                    selectedAC.ViewModel.isHighlighted = false;
                selectedElement = mc;
                selectedElement.ViewModel.isHighlighted = true;

                observer.ViewModel = mc.ViewModel;

                Mouse.Capture(mc);
            }
            else if (Mouse.MiddleButton == MouseButtonState.Pressed)
            {
                var mc = e.Source as MoveControl;
                moves.Remove(mc);

                List<ActionControl> tmp = new List<ActionControl>();
                foreach (var ac in actions)
                {
                    if (ac.ViewModel.fromMC == mc.ViewModel || ac.ViewModel.toMC == mc.ViewModel)
                        tmp.Add(ac);
                }
                foreach (var ac in tmp)
                    actions.Remove(ac);

                Mouse.Capture(mc);
            }
        }

        private void MCMouseUp(object sender, MouseEventArgs e)
        {
            Mouse.Capture(null);
        }

        private void CanvasMouseMove(object sender, MouseEventArgs e)
        {
            if (Mouse.LeftButton != MouseButtonState.Pressed) return;
        }

        private void CanvasMouseDown(object sender, MouseEventArgs e)
        {
            if (Mouse.Captured != null) return;
            if (Mouse.LeftButton != MouseButtonState.Pressed) return;

            //Mouse.Capture(canvas);
            //MoveControl mc = new MoveControl(new Robot.RobotVM(robotVM.));
            if (scriptVM == null) return;
            MoveControl mc = new MoveControl(scriptVM);
            //mc.viewModel.robotVM.bodyVM.height = 19;
            Point a = e.GetPosition(grid);
            mc.ViewModel.coord = a;
            //mc.x = a.X;
            //mc.y = a.Y;    
            BindMC(mc);
            moves.Add(mc);
            //observer.Tmp();
            observer.ViewModel = mc.ViewModel;
            
            //moves = new ObservableCollection<MoveControl>() { new MoveControl() };
            //_moves.Add(new MoveControl());            
            //Point a = e.GetPosition(canvas);

            selectedElement.ViewModel.isHighlighted = false;
            selectedElement = mc;
            selectedElement.ViewModel.isHighlighted = true;

            Mouse.Capture(grid);
        }

        private void CanvasMouseUp(object sender, MouseEventArgs e)
        {
            Mouse.Capture(null);
        }

        private void BindMC(MoveControl mc)
        {
            mc.MouseDown += MCMouseDown;
            mc.MouseMove += MCMouseMove;
            mc.MouseUp   += MCMouseUp;
        }

        private void UnBindMC(MoveControl mc)
        {
            mc.MouseDown -= MCMouseDown;
            mc.MouseMove -= MCMouseMove;
            mc.MouseUp   -= MCMouseUp;
        }

        private void BindAC(ActionControl ac)
        {
            ac.MouseDown += ACMouseDown;
            ac.MouseMove += ACMouseMove;
            ac.MouseUp   += ACMouseUp;
        }

        private void UnBindAC(ActionControl ac)
        {
            ac.MouseDown -= ACMouseDown;
            ac.MouseMove -= ACMouseMove;
            ac.MouseUp   -= ACMouseUp;
        }
    }
}
