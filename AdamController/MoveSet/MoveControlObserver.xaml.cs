﻿using ReactiveUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace AdamController
{
    public partial class MoveControlObserver : UserControl, IViewFor<MoveControlVM>
    {
        public static readonly DependencyProperty ViewModelProperty = DependencyProperty
            .Register(nameof(ViewModel), typeof(MoveControlVM), typeof(MoveControlObserver));

        private TimelinesControllerView tcv = null;
        private UIElement element = null;

        public MoveControlObserver()
        {
            InitializeComponent();

            DataContext = this;

            this.WhenAnyValue(v => v.ViewModel)
                .Subscribe(vm => {
                    if (vm == null) return;

                    if (tcv != null)
                        grid.Children.Remove(tcv);

                    if (element != null)
                        grid.Children.Remove(element);

                    ScriptVM.Extension extension = vm.scriptVM.extension;

                    switch (extension)
                    {
                        case ScriptVM.Extension.MOVEMENT:
                            {
                                Model3DView m3dv = new Model3DView();
                                Robot.RobotVM rvm = new Robot.RobotVM();
                                m3dv.ViewModel = new RobotModel3DVM(rvm);
                                element = m3dv;

                                tcv = new TimelinesControllerView();
                                tcv.Init(rvm, extension);                                

                                break;
                            }
                        case ScriptVM.Extension.AUDIO:
                            {
                                AudioView av = new AudioView();
                                av.Init(new AdamTcpClient(1000));
                                element = av;

                                tcv = new TimelinesControllerView();
                                tcv.Init(av.ViewModel, extension);

                                break;
                            }
                        case ScriptVM.Extension.LED:
                            {
                                LedsView lsv = new LedsView();
                                lsv.Init(new AdamTcpClient(1000));
                                element = lsv;

                                tcv = new TimelinesControllerView();
                                tcv.Init(lsv.leftLedView.ViewModel, lsv.rightLedView.ViewModel, extension);

                                break;
                            }
                        case ScriptVM.Extension.WHEELS:
                            {
                                WheelsVM av = new WheelsVM();
                                //av.Init(new AdamTcpClient(1000));
                                //element = av;

                                tcv = new TimelinesControllerView();
                                tcv.Init(av, extension);

                                break;
                            }
                    }

                    if (tcv == null || element == null)
                        return;

                    tcv.ViewModel = vm.scriptVM;

                    Grid.SetColumn(tcv, 0);
                    Grid.SetRow(tcv, 1);
                    grid.Children.Add(tcv);

                    Grid.SetColumn(element, 0);
                    Grid.SetRow(element, 0);
                    grid.Children.Add(element);
                });

            this.WhenAnyValue(v => v.ViewModel.actionVM)
                .Subscribe(vm => {
                    if (vm == null) return;

                    if (tcv != null)
                        grid.Children.Remove(tcv);

                    if (element != null)
                        grid.Children.Remove(element);

                    ActionV av = new ActionV();
                    av.ViewModel = vm;
                    element = av;

                    Grid.SetColumn(element, 0);
                    Grid.SetRow(element, 0);
                    grid.Children.Add(element);
                });
        }

        public void Tmp()
        {
            //Type.Text = "Type";
            //TypeValue.Text = "Audio";
            //Text.Text = "Text";
            //TextValue.Text = "Однин, два, три";
        }

        public MoveControlVM ViewModel
        {
            get => (MoveControlVM)GetValue(ViewModelProperty);
            set => SetValue(ViewModelProperty, value);
        }

        object IViewFor.ViewModel
        {
            get => ViewModel;
            set => ViewModel = (MoveControlVM)value;
        }

        public void CB_Selected(object sender, RoutedEventArgs e)
        {
            //if ((string)CB.SelectedItem == "Audio")
            //{
            //    ViewModel.MySource = "/Images/audio.png";
            //    Value.Visibility = Visibility.Visible;
            //    model3dView.Visibility = Visibility.Hidden;
            //    Blocks.Visibility = Visibility.Hidden;
            //    //ViewModel.type = "Audio";
            //}
            //else
            //{
            //    ViewModel.MySource = "/Images/robot.png";
            //    Value.Visibility = Visibility.Hidden;
            //    model3dView.Visibility = Visibility.Visible;
            //    Blocks.Visibility = Visibility.Visible;
            //    //ViewModel.type = "Movement";
            //}
        }
    }
}
