﻿using ReactiveUI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reactive.Disposables;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace AdamController
{
    public partial class MoveControl : UserControl, IViewFor<MoveControlVM>
    {
        public static readonly DependencyProperty ViewModelProperty = DependencyProperty
            .Register(nameof(ViewModel), typeof(MoveControlVM), typeof(MoveControl));

        public MoveControl(ScriptVM scriptVM)
        {            
            InitializeComponent();
            DataContext = this;

            ViewModel = new MoveControlVM(scriptVM);

            //this.WhenActivated(disposable =>
            //{
            //    this.Bind(ViewModel, x => x.robotVM, x => x.model3DView.ViewModel)
            //        .DisposeWith(disposable);
            //});            
        }

        public MoveControlVM ViewModel
        {
            get => (MoveControlVM)GetValue(ViewModelProperty);
            set => SetValue(ViewModelProperty, value);
        }

        object IViewFor.ViewModel
        {
            get => ViewModel;
            set => ViewModel = (MoveControlVM)value;
        }
    }
}
