﻿using Newtonsoft.Json;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using System;
using System.Collections.Generic;
using System.Reactive.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdamController
{
    public class ActionVM : ReactiveObject, ITime
    {
        public enum Direction
        {
            D0 = 0,
            D45 = 45,
            D90 = 90,
            D135 = 135,
            D180 = 180,
            D225 = 225,
            D270 = 270,
            D315 = 315,
            D360 = 360,
        }

        [Reactive] public int maxRepeat { get; set; } = 1;
        [Reactive] public int time { get; set; } = 1;

        public ITime Clone() => new ActionVM(this);
        public ITime Get() => this;

        public ActionVM() { }

        public ActionVM(ActionVM vm)
        {
            maxRepeat = vm.maxRepeat;
            time = vm.time;
        }

        public void Update(object left, object right, int maxTime, int curTime)
        {
            ActionVM currentLvm;
            if (curTime < maxTime) currentLvm = (ActionVM)left;
            else currentLvm = (ActionVM)right;
            
            maxRepeat = currentLvm.maxRepeat;
            time = currentLvm.time;
        }

        public void ReadJson(JsonTextReader reader)
        {
            while (reader.TokenType != JsonToken.EndObject)
            {
            }
        }

        public void WriteJson(JsonWriter writer)
        {
            writer.WriteStartObject();
            writer.WriteEndObject();
        }
    }
}
