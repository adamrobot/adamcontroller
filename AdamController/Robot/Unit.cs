using Newtonsoft.Json;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using System.Reactive.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdamController.Robot
{
    public abstract class Unit : ReactiveObject, ITime
    {
        public Dictionary<ServoID, Servo> servos;
        static public AdamTcpClient client;

        [Reactive] public bool isUpdated { get; set; } = false;
        public abstract ITime Clone();
        public abstract ITime Get();
        public abstract void Update(object left, object right, int maxTime, int curTime);
        public abstract void WriteJson(JsonWriter writer);
        public abstract void ReadJson(JsonTextReader reader);
        public abstract int[] angles { get; }
        public abstract int[] speeds { get; }

        public virtual Message.ServoScheme GetServoScheme(Unit u)
        {
            return GetAngleAndSpeedScheme(u.angles, u.speeds);
        }

        public Unit()
        {

        }
        public Unit(Servo[] servoArr)
        {
            servos = new Dictionary<ServoID, Servo>();
            //this.client = client;

            for (var i = 0; i < servoArr.Length; i++)
                servos.Add(servoArr[i].id, servoArr[i]);
        }

        protected void InitMessage(int size, byte addr, byte dataSize, ref Message.ServoScheme outServoMsg)
        {
            outServoMsg.ids = new byte[size];
            outServoMsg.data = new byte[size][];
            outServoMsg.addr = addr;
            outServoMsg.size = dataSize;

            for (var i = 0; i < size; i++)
                outServoMsg.data[i] = new byte[dataSize];
        }

        public void SetPID(byte[] data)
        {
            Message.ServoScheme outServoMsg = new Message.ServoScheme();
            ControlTable.CtField dGain = 
                ControlTable.ControlTable.CtController.Instance.controlTables[ControlTable.CtType.MX].fields[ControlTable.CtFieldName.D_GAIN];
            InitMessage(servos.Count, dGain.addr, 3, ref outServoMsg);

            for (var i = 0; i < servos.Count; i++)
            {
                outServoMsg.ids[i] = (byte)servos.ElementAt(i).Value.id;
                outServoMsg.data[i][0] = data[2];
                outServoMsg.data[i][1] = data[1];
                outServoMsg.data[i][2] = data[0];
            }

            client.SendData(outServoMsg.Msg(Message.Command.WRITE).GetData());
        }

        public void SetSlope(byte[] data)
        {
            Message.ServoScheme outServoMsg = new Message.ServoScheme();
            ControlTable.CtField cwComplanceSlope = 
                ControlTable.ControlTable.CtController.Instance.controlTables[ControlTable.CtType.RX].fields[ControlTable.CtFieldName.CW_COMPLIANCE_SLOPE];
            InitMessage(servos.Count, cwComplanceSlope.addr, 2, ref outServoMsg);

            for (var i = 0; i < servos.Count; i++)
            {
                outServoMsg.ids[i] = (byte)servos.ElementAt(i).Value.id;
                //outServoMsg.data[i] = new byte[2];
                outServoMsg.data[i][0] = data[0];
                outServoMsg.data[i][1] = data[1];
            }

            client.SendData(outServoMsg.Msg(Message.Command.WRITE).GetData());
        }

        public void SetTorqueStatus(byte status)
        {
            Message.ServoScheme outServoMsg = new Message.ServoScheme();
            ControlTable.CtField torqueEnable = 
                ControlTable.ControlTable.CtController.Instance.controlTables[ControlTable.CtType.RX].fields[ControlTable.CtFieldName.TORQUE_ENABLE];
            InitMessage(servos.Count, torqueEnable.addr, torqueEnable.size, ref outServoMsg);

            for (var i = 0; i < servos.Count; i++)
            {
                outServoMsg.ids[i] = (byte)servos.ElementAt(i).Value.id;
                //outServoMsg.data[i] = new byte[1];
                outServoMsg.data[i][0] = status;
            }

            client.SendData(outServoMsg.Msg(Message.Command.WRITE).GetData());
        }

        public void SetTorqueLimit(int limit)
        {
            Message.ServoScheme outServoMsg = new Message.ServoScheme();
            ControlTable.CtField torqueLimit = 
                ControlTable.ControlTable.CtController.Instance.controlTables[ControlTable.CtType.RX].fields[ControlTable.CtFieldName.TORQUE_LIMIT];
            InitMessage(servos.Count, torqueLimit.addr, torqueLimit.size, ref outServoMsg);

            for (var i = 0; i < servos.Count; i++)
            {
                outServoMsg.ids[i] = (byte)servos.ElementAt(i).Value.id;
                byte[] limitData = BitConverter.GetBytes(limit).Take(2).ToArray();
                limitData.CopyTo(outServoMsg.data[i], 0);
            }

            client.SendData(outServoMsg.Msg(Message.Command.WRITE).GetData());
        }

        public Message.ServoScheme GetAngleAndSpeedScheme(int[] angles, int[] speeds)
        {
            return GetAngleAndSpeedScheme(angles, speeds, servos.Keys.ToArray());
        }

        public Message.ServoScheme GetAngleAndSpeedScheme()
        {
            return GetAngleAndSpeedScheme(angles, speeds, servos.Keys.ToArray());
        }

        public Message.ServoScheme GetAngleAndSpeedScheme(int[] angles, int[] speeds, ServoID[] servoIds)
        {
            Message.ServoScheme outServoMsg = new Message.ServoScheme();
            ControlTable.CtField goalPosition =
                ControlTable.ControlTable.CtController.Instance.controlTables[ControlTable.CtType.RX].fields[ControlTable.CtFieldName.GOAL_POSITION];
            InitMessage(angles.Length, goalPosition.addr, 4, ref outServoMsg);

            int[] pos = new int[angles.Length];
            for (var i = 0; i < angles.Length; i++)
            {
                outServoMsg.ids[i] = (byte)servos[servoIds[i]].id;
                pos[i] = (int)(angles[i] * servos[servoIds[i]].ratio * servos[servoIds[i]].scale);
                
                pos[i] += servos[servoIds[i]].homePos;

                if (pos[i] < 0)
                    pos[i] += 65536;

                servos[servoIds[i]].estimatedPos = pos[i];

                byte[] posData = BitConverter.GetBytes(pos[i]).Take(2).ToArray();
                byte[] speedData = BitConverter.GetBytes(speeds[i]).Take(2).ToArray();
                posData.CopyTo(outServoMsg.data[i], 0);
                speedData.CopyTo(outServoMsg.data[i], posData.Length);
            }

            return outServoMsg;
        }

        public void SetAngleAndSpeed()
        {
            SetAngleAndSpeed(angles, speeds, servos.Keys.ToArray());
        }

        public void SetAngleAndSpeed(int[] angles, int[] speeds)
        {
            SetAngleAndSpeed(angles, speeds, servos.Keys.ToArray());
        }

        protected void SetAngleAndSpeed(int[] angles, int[] speeds, ServoID[] servoIds)
        {
            Message.ServoScheme outServoMsg = GetAngleAndSpeedScheme(angles, speeds);

            //if (servoIds[0] == ServoID.LeftLeg1 || servoIds[0] == ServoID.RightLeg1)
            //    return;
            //if (client != null)
            //    client.SendData(outServoMsg.Msg(Message.Command.WRITE).GetData());

            //isUpdated = false;
        }
    }
}
