﻿using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdamController.Robot
{
    public class RobotVM : ReactiveObject
    {
        [Reactive] public HeadVM headVM { get; set; }
        [Reactive] public BodyVM bodyVM { get; set; }
        [Reactive] public LeftHandVM leftHandVM { get; set; }
        [Reactive] public RightHandVM rightHandVM { get; set; }
        [Reactive] public LeftLegVM leftLegVM { get; set; }
        [Reactive] public RightLegVM rightLegVM { get; set; }

        public RobotVM()
        {
            headVM = new HeadVM();
            bodyVM = new BodyVM();
            leftHandVM = new LeftHandVM();
            rightHandVM = new RightHandVM();
            leftLegVM = new LeftLegVM();
            rightLegVM = new RightLegVM();

            this.WhenAnyValue(
                vm => vm.leftLegVM.leg.height,
                vm => vm.rightLegVM.leg.height
                )
                .Subscribe(h => {
                    if (rightLegVM.leg.height == leftLegVM.leg.height)
                        return;
                    leftLegVM.leg.height = rightLegVM.leg.height;
                });

            //this.WhenAnyValue(
            //    vm => vm.leftLegVM.leg.isUpdated,
            //    vm => vm.rightLegVM.leg.isUpdated
            //    )
            //    .Subscribe(isUpdated => {
            //        if (isUpdated.Item1 && isUpdated.Item2)
            //        {
            //            for (int i = 0; i < rightLegVM.leg.angles.Length; i++)
            //                rightLegVM.leg.angles[i] = leftLegVM.leg.angles[i];
            //            Message.ServoScheme outServoMsg = leftLegVM.leg.GetAngleAndSpeedScheme() + rightLegVM.leg.GetAngleAndSpeedScheme();
            //
            //            if (Unit.client != null)
            //                Unit.client.SendData(outServoMsg.Msg(Message.Command.WRITE).GetData());
            //
            //            leftLegVM.leg.isUpdated = false;
            //            rightLegVM.leg.isUpdated = false;
            //        }
            //    });
        }
    }
}
