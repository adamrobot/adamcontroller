﻿using Newtonsoft.Json;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdamController.Robot
{
    public class Legs : Unit
    {
        [Reactive] public LeftLegVM leftLegVM { get; set; }
        [Reactive] public RightLegVM rightLegVM { get; set; }

        private const string propType = "Type";
        private const string propLeftLeg = "LeftLeg";
        private const string propRightLeg = "RightLeg";

        public override int[] angles => new int[0];
        public override int[] speeds => new int[0];

        public Legs()
        {
            leftLegVM = new LeftLegVM();
            rightLegVM = new RightLegVM();
        }

        public Legs(Leg leftLeg, Leg rightLeg) : this()
        {
            leftLegVM.leg = leftLeg;
            rightLegVM.leg = rightLeg;
        }

        public Legs(Legs legs) : this()
        {
            leftLegVM.leg = (Leg)legs.leftLegVM.leg.Clone();
            rightLegVM.leg = (Leg)legs.rightLegVM.leg.Clone();
        }

        public override ITime Clone() => new Legs(this);
        public override ITime Get() => this;
        
        public override void Update(object left, object right, int maxTime, int curTime)
        {
            leftLegVM.leg.Update(((Legs)left).leftLegVM.leg, ((Legs)right).leftLegVM.leg, maxTime, curTime);
            rightLegVM.leg.Update(((Legs)left).rightLegVM.leg, ((Legs)right).rightLegVM.leg, maxTime, curTime);
        }
        
        public override void ReadJson(JsonTextReader reader)
        {
            while (reader.TokenType != JsonToken.EndObject)
            {
                if (reader.TokenType == JsonToken.PropertyName)
                {
                    var token = reader.Value;
                    reader.Read();
                    switch (token)
                    {
                        case propLeftLeg:
                            leftLegVM.leg.ReadJson(reader);
                            break;
                        case propRightLeg:
                            rightLegVM.leg.ReadJson(reader);
                            break;
                    }
                }
                reader.Read();
            }
        }
        
        public override void WriteJson(JsonWriter writer)
        {
            writer.WriteStartObject();
            writer.WritePropertyName(propType);
            writer.WriteValue(GetType().FullName);
            writer.WritePropertyName(propLeftLeg);
            leftLegVM.leg.WriteJson(writer);
            writer.WritePropertyName(propRightLeg);
            rightLegVM.leg.WriteJson(writer);
            writer.WriteEndObject();
        }
    }
}
