﻿using Newtonsoft.Json;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdamController.Robot
{
    public class Leg : Unit
    {
        [Reactive] public (double l1, double l2) lengths { get; private set; } = (126.1, 135);
        [Reactive] public double height { get; set; } = 0;
        [Reactive] public double kneeHeight { get; set; } = 0;
        [Reactive] public double incline { get; set; } = 0;
        [Reactive] public double beta { get; private set; } = 0;
        [Reactive] public double phi { get; private set; } = 0;
        [Reactive] public double alpha { get; private set; } = 0;
        [Reactive] public bool isActive { get; set; } = true;
        public const double maxHeight = 160;
        public const double minHeight = 0;
        public const double maxKneeHeight = 100;
        public const double minKneeHeight = 0;

        private readonly ObservableAsPropertyHelper<int[]> _angles;
        public override int[] angles => new int[3] { 
            _angles.Value != null ? _angles.Value[0] : 0,
            _angles.Value != null ? _angles.Value[1] : 0,
            _angles.Value != null ? _angles.Value[2] : 0 };
        public override int[] speeds => new int[3] { 0, 0, 0 };

        private const string propType = "Type";
        private const string propHeight = "Height";
        private const string propKneeHeight = "KneeHeight";

        public Leg() 
        {
            this.WhenAnyValue(m => m.height, m => m.kneeHeight)
                .Subscribe(t => { UpdateHeights(t.Item1, t.Item2); });

            this.WhenAnyValue(m => m.height)
                .Subscribe(t => { CalcAngles(t); });

            this.WhenAnyValue(m => m.kneeHeight)
                .Subscribe(t => { CalcKneeAngles(t); });

            _angles = this.WhenAnyValue(m => m.phi, m => m.incline)
                .Select(t => new int[3] { (int)(t.Item1 * 180 / Math.PI + t.Item2), (int)(beta * 180 / Math.PI), (int)(alpha * 180 / Math.PI) })
                .ToProperty(this, m => m.angles);
        }

        public Leg(Servo[] servoArr) : base(servoArr)
        {
            if (servoArr.Length != 3) throw new Exception(); // need change

            this.WhenAnyValue(m => m.height, m => m.kneeHeight)
                .Subscribe(t => { UpdateHeights(t.Item1, t.Item2); });

            this.WhenAnyValue(m => m.height)
                .Subscribe(t => { CalcAngles(t); });

            this.WhenAnyValue(m => m.kneeHeight)
                .Subscribe(t => { CalcKneeAngles(t); });

            _angles = this.WhenAnyValue(m => m.phi, m => m.incline)
                .Select(t => new int[3] { (int)(t.Item1 * 180 / Math.PI + t.Item2), (int)(beta * 180 / Math.PI), (int)(alpha * 180 / Math.PI) })
                .ToProperty(this, m => m.angles);

            this.WhenAnyValue(m => m.angles)
                .Subscribe(t => {
                    //isUpdated = true;
                    SetAngleAndSpeed(angles, speeds); 
                });
        }

        public Leg(Leg leg) : this(leg.servos.Values.ToArray())
        {
            lengths = leg.lengths;
            kneeHeight = leg.kneeHeight;
            incline = leg.incline;
            beta = leg.beta;
            phi = leg.phi;
            alpha = leg.alpha;
            height = leg.height;
        }

        public override ITime Clone() => new Leg(this);
        public override ITime Get() => this;

        public (double height, double kneeHeight) GetOffest(Leg leg)
        {
            return (leg.height - height, leg.kneeHeight - kneeHeight);
        }

        public override void Update(object left, object right, int maxTime, int curTime)
        {
            height = ((Leg)left).height + (((Leg)right).height - ((Leg)left).height) / maxTime * curTime;
            kneeHeight = ((Leg)left).kneeHeight + (((Leg)right).kneeHeight - ((Leg)left).kneeHeight) / maxTime * curTime;
        }

        public override void ReadJson(JsonTextReader reader)
        {
            while (reader.TokenType != JsonToken.EndObject)
            {
                if (reader.TokenType == JsonToken.PropertyName)
                {
                    var token = reader.Value;
                    reader.Read();
                    switch (token)
                    {
                        case propHeight:
                            height = (double)reader.Value;
                            break;
                        case propKneeHeight:
                            kneeHeight = (double)reader.Value;
                            break;
                    }
                }
                reader.Read();
            }
        }

        public override void WriteJson(JsonWriter writer)
        {
            writer.WriteStartObject();
            writer.WritePropertyName(propType);
            writer.WriteValue(GetType().FullName);
            writer.WritePropertyName(propHeight);
            writer.WriteValue(height);
            writer.WritePropertyName(propKneeHeight);
            writer.WriteValue(kneeHeight);
            writer.WriteEndObject();
        }

        private void UpdateHeights(double newHeight, double newKneeHeight)
        {
            if (newHeight > maxHeight) height = maxHeight;
            else if (newHeight < minHeight) height = minHeight;
            else height = newHeight;

            if (newKneeHeight > maxKneeHeight) kneeHeight = maxKneeHeight;
            else if (newKneeHeight < minKneeHeight) kneeHeight = minKneeHeight;
            else kneeHeight = newKneeHeight;
        }

        private void CalcKneeAngles(double height)
        {
            double angle = Math.Asin(height / lengths.l1);

            beta = angle;
            alpha = 0;
            phi = angle;
        }

        private void CalcAngles(double height)
        {
            if (height < lengths.l2 - lengths.l1)
            {
                CalcKneeAngles(height);
                return;

                //int[] legAngles = CalcLegAngles(height);
                //return new int[] { legAngles[0], legAngles[1] / 2, legAngles[1] - legAngles[1] / 2 };
            }

            double zetta = AdmMath.CosTh(lengths.l1, height, lengths.l2);
            double gamma;

            if (zetta > Math.PI / 2)
            {
                beta = Math.Acos(lengths.l1 / lengths.l2);
                gamma = Math.Asin(lengths.l1 / lengths.l2);
                alpha = Math.PI / 2 - gamma;

                double coeff = height / Math.Sqrt(Math.Abs(lengths.l1 * lengths.l1 + lengths.l2 * lengths.l2 - 2 * lengths.l1 * lengths.l2 * Math.Cos(beta)));
                beta *= coeff;
                alpha *= coeff;

                phi = beta - alpha;

                return;

                //CalcLegAngles(height).CopyTo(result, 0);
                //return result;
            }

            beta = AdmMath.CosTh(lengths.l1, lengths.l2, height);
            gamma = AdmMath.CosTh(height, lengths.l2, lengths.l1);
            alpha = Math.PI / 2 - gamma;
            phi = beta - alpha;

            return;
        }
    }
}
