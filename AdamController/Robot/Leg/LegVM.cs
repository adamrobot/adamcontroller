﻿using Newtonsoft.Json;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using System;
using System.Collections.Generic;
using System.Reactive.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdamController.Robot
{
    public abstract class LegVM : ReactiveObject
    {
        [Reactive] public Leg leg { get; set; }
    }

    public class LeftLegVM : LegVM
    {
        public LeftLegVM()
        {
            Servo[] data = {
                Servo.servos[ServoID.LeftLeg1], Servo.servos[ServoID.LeftLeg2], Servo.servos[ServoID.LeftLeg3]
            };

            leg = new Leg(data);
        }
    }

    public class RightLegVM : LegVM
    {
        public RightLegVM()
        {
            Servo[] data = {
                Servo.servos[ServoID.RightLeg1], Servo.servos[ServoID.RightLeg2], Servo.servos[ServoID.RightLeg3]
            };

            leg = new Leg(data);
        }
    }
}
