﻿using Newtonsoft.Json;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdamController.Robot
{
    public class Head : Unit
    {
        //[Reactive] public HeadVM viewModel { get; set; } = new HeadVM();

        [Reactive] public double nod { get; set; }
        [Reactive] public double rotate { get; set; }
        public const double maxNod = 30;
        public const double minNod = -30;
        public const double maxRotate = 60;
        public const double minRotate = -60;
        [Reactive] public bool isActive { get; set; } = true;
        public override int[] angles => new int[2] { (int)rotate, (int)nod };
        public override int[] speeds => new int[2] { 200, 200 };

        private const string propType = "Type";
        private const string propNod = "Nod";
        private const string propRotate = "Rotate";

        //public Head(Servo[] servoArr, ref AdamTcpClient client) : base(servoArr, ref client)
        //{
        //    if (servoArr.Length != 2) throw new Exception(); // need change
        //
        //    this.WhenAnyValue(vm => vm.viewModel.rotate, vm => vm.viewModel.nod)
        //        .Subscribe(t => { SetAngleAndSpeed(viewModel.angles, viewModel.speeds); });
        //}

        public Head() 
        {
            this.WhenAnyValue(h => h.rotate, h => h.nod)
                .Subscribe(t => {
                    UpdateRotateAndNod(t.Item1, t.Item2);
                });
        }

        public Head(Servo[] servoArr) : base(servoArr)
        {
            if (servoArr.Length != 2) throw new Exception(); // need change
            
            this.WhenAnyValue(h => h.rotate, h => h.nod)
                .Subscribe(t => {
                    UpdateRotateAndNod(t.Item1, t.Item2);
                    //isUpdated = false;
                    //isUpdated = true;
                    SetAngleAndSpeed(angles, speeds);
                });
        }

        public Head(Head head) : this(head.servos.Values.ToArray())
        {
            nod = head.nod;
            rotate = head.rotate;
        }

        public override ITime Clone() => new Head(this);
        public override ITime Get() => this;

        public (double nod, double rotate) GetOffest(Head head)
        {
            return (head.nod - nod, head.rotate - rotate);
        }

        public override void Update(object left, object right, int maxTime, int curTime)
        {
            nod = ((Head)left).nod + (((Head)right).nod - ((Head)left).nod) / maxTime * curTime;
            rotate = ((Head)left).rotate + (((Head)right).rotate - ((Head)left).rotate) / maxTime * curTime;
        }

        public override void ReadJson(JsonTextReader reader)
        {
            while (reader.TokenType != JsonToken.EndObject)
            {
                if (reader.TokenType == JsonToken.PropertyName)
                {
                    var token = reader.Value;
                    reader.Read();
                    switch (token)
                    {
                        case propNod:
                            nod = (double)reader.Value;
                            break;
                        case propRotate:
                            rotate = (double)reader.Value;
                            break;
                    }
                }
                reader.Read();
            }
        }

        public override void WriteJson(JsonWriter writer)
        {
            writer.WriteStartObject();
            writer.WritePropertyName(propType);
            writer.WriteValue(GetType().FullName);
            writer.WritePropertyName(propNod);
            writer.WriteValue(nod);
            writer.WritePropertyName(propRotate);
            writer.WriteValue(rotate);
            writer.WriteEndObject();
        }

        private void UpdateRotateAndNod(double newRotate, double newNod)
        {
            if (newRotate > maxRotate) rotate = maxRotate;
            else if (newRotate < minRotate) rotate = minRotate;
            else rotate = newRotate;

            if (newNod > maxNod) nod = maxNod;
            else if (newNod < minNod) nod = minNod;
            else nod = newNod;
        }
    }
}
