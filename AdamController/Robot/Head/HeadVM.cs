﻿using Newtonsoft.Json;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AdamController.Robot
{
    public class HeadVM : ReactiveObject
    {
        [Reactive] public Head head { get; set; }

        public HeadVM()
        {
            Servo[] data = {
                Servo.servos[ServoID.HeadRotate], Servo.servos[ServoID.HeadNod]
            };

            head = new Head(data);
        }
    }
}
