﻿using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using System.Reactive.Linq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AdamController.Robot
{
    public class Robot : ReactiveObject
    {
        private AdamUdpClient client;

        //[Reactive] public RobotVM viewModel { get; set; } = new RobotVM();

        [Reactive] public Head head { get; set; }
        [Reactive] public Body body { get; set; }
        [Reactive] public Hand leftHand { get; set; }
        [Reactive] public Hand rightHand { get; set; }
        [Reactive] public Leg leftLeg { get; set; }
        [Reactive] public Leg rightLeg { get; set; }

        public Robot(AdamUdpClient client)
        {
            this.client = client;
        }
    }
}
