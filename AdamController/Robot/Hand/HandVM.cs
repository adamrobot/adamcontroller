﻿using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using System.Reactive.Linq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace AdamController.Robot
{
    public abstract class HandVM : ReactiveObject
    {
        [Reactive] public Hand hand { get; set; }
    }

    public class LeftHandVM : HandVM
    {
        public LeftHandVM()
        {
            Servo[] data = {
                Servo.servos[ServoID.LeftHandRotate], Servo.servos[ServoID.LeftHand1], Servo.servos[ServoID.LeftHand2],
                Servo.servos[ServoID.LeftHand3]
            };

            hand = new Hand(data);
        }
    }

    public class RightHandVM : HandVM
    {
        public RightHandVM()
        {
            Servo[] data = {
                Servo.servos[ServoID.RightHandRotate], Servo.servos[ServoID.RightHand1], Servo.servos[ServoID.RightHand2],
                Servo.servos[ServoID.RightHand3]
            };

            hand = new Hand(data);
        }
    }
}
