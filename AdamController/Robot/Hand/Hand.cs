﻿using Newtonsoft.Json;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdamController.Robot
{
    public class Hand : Unit
    {
        private readonly ObservableAsPropertyHelper<double> _maxDist;
        public double maxDist => _maxDist.Value;

        private readonly ObservableAsPropertyHelper<double> _minDist;
        public double minDist => _minDist.Value;

        [Reactive] public double gamma { get; set; } = 0;
        [Reactive] public double zeta { get; set; } = 0;

        private readonly ObservableAsPropertyHelper<double> _beta;
        public double beta => _beta != null ? _beta.Value : 0;
        private readonly ObservableAsPropertyHelper<double> _phi;
        public double phi => _phi != null ? _phi.Value : 0;
        private readonly ObservableAsPropertyHelper<double> _alpha;
        public double alpha => _alpha != null ? _alpha.Value : 0;

        [Reactive] public Coord3 coord { get; set; } = new Coord3(150, -120, 0);
        [Reactive] public (double l1, double l2, double l3) lengths { get; private set; } = (132.96, 89.82, 143.94);

        private readonly ObservableAsPropertyHelper<int[]> _angles;
        public override int[] angles => new int[4] { 
            _angles.Value != null ? _angles.Value[0] : 0,
            _angles.Value != null ? _angles.Value[1] : 0,
            _angles.Value != null ? _angles.Value[2] : 0,
            _angles.Value != null ? _angles.Value[3] : 0 };
        public override int[] speeds => new int[4] { 0, 0, 0, 0 };

        [Reactive] public double dist { get; set; } = 0;
        [Reactive] public bool isActive { get; set; } = true;

        private const string propType = "Type";
        private const string propGamma = "Gamma";
        private const string propZeta = "Zeta";
        private const string propDist = "Dist";
        private const string propX = "X";
        private const string propY = "Y";
        private const string propZ = "Z";

        public Hand(Servo[] servoArr) : base(servoArr)
        {
            if (servoArr.Length != 4) throw new Exception(); // need change

            _maxDist = this.WhenAnyValue(m => m.lengths)
                .Select(t => t.l1 + t.l2 + t.l3)
                .ToProperty(this, m => m.maxDist);

            _minDist = this.WhenAnyValue(m => m.lengths)
                .Select(t => t.l1 - t.l2 + t.l3)
                .ToProperty(this, m => m.minDist);

            _angles = this.WhenAnyValue(m => m.zeta, m => m.alpha, m => m.beta)
                .Select(t => new int[4] { (int)(t.Item1 * 180 / Math.PI), (int)(t.Item2 * 180 / Math.PI) + 90, (int)(t.Item3 * 180 / Math.PI), (int)(-t.Item3 * 180 / Math.PI) })
                .ToProperty(this, m => m.angles);

            //this.WhenAnyValue(vm => vm.zeta, vm => vm.alpha, vm => vm.beta)
            //    .Subscribe(t => { UpdateAngles(t.Item1, t.Item2, t.Item3); });

            _alpha = this.WhenAnyValue(m => m.gamma, m => m.phi)
                .Select(t => t.Item1 - t.Item2)
                .ToProperty(this, m => m.alpha);

            _beta = this.WhenAnyValue(m => m.dist, m => m.lengths)
                .Select(t => CalcBeta(t.Item1, t.Item2))
                .ToProperty(this, m => m.beta);

            _phi = this.WhenAnyValue(m => m.dist, m => m.lengths)
                .Select(t => CalcPhi(t.Item1, t.Item2))
                .ToProperty(this, m => m.phi);

            this.WhenAnyValue(m => m.coord)
                .Subscribe(t => { UpdateAngles(t); });

            this.WhenAnyValue(m => m.angles)
                .Subscribe(t => {
                    if (_angles.Value != null)
                        //isUpdated = true;
                        SetAngleAndSpeed(angles, speeds); 
                });
        }

        public Hand()
        {
            _maxDist = this.WhenAnyValue(m => m.lengths)
                .Select(t => t.l1 + t.l2 + t.l3)
                .ToProperty(this, m => m.maxDist);

            _minDist = this.WhenAnyValue(m => m.lengths)
                .Select(t => t.l1 - t.l2 + t.l3)
                .ToProperty(this, m => m.minDist);

            _angles = this.WhenAnyValue(m => m.zeta, m => m.alpha, m => m.beta)
                .Select(t => new int[4] { (int)(t.Item1 * 180 / Math.PI), (int)(t.Item2 * 180 / Math.PI) + 90, (int)(t.Item3 * 180 / Math.PI), (int)(-t.Item3 * 180 / Math.PI) })
                .ToProperty(this, m => m.angles);

            //this.WhenAnyValue(vm => vm.zeta, vm => vm.alpha, vm => vm.beta)
            //    .Subscribe(t => { UpdateAngles(t.Item1, t.Item2, t.Item3); });

            _alpha = this.WhenAnyValue(m => m.gamma, m => m.phi)
                .Select(t => t.Item1 - t.Item2)
                .ToProperty(this, m => m.alpha);

            _beta = this.WhenAnyValue(m => m.dist, m => m.lengths)
                .Select(t => CalcBeta(t.Item1, t.Item2))
                .ToProperty(this, m => m.beta);

            _phi = this.WhenAnyValue(m => m.dist, m => m.lengths)
                .Select(t => CalcPhi(t.Item1, t.Item2))
                .ToProperty(this, m => m.phi);

            this.WhenAnyValue(m => m.coord)
                .Subscribe(t => { UpdateAngles(t); });
        }

        public Hand(Hand hand) : this(hand.servos.Values.ToArray())
        {
            coord = hand.coord; //need to change
            gamma = hand.gamma;
            zeta = hand.zeta;
            lengths = hand.lengths;
            dist = hand.dist;
        }

        public override ITime Clone() => new Hand(this);
        public override ITime Get() => this;

        public (double gamma, double zeta, double dist) GetOffest(Hand hand)
        {
            return (hand.gamma - gamma, hand.zeta - zeta, hand.dist - dist);
        }

        public override void Update(object left, object right, int maxTime, int curTime)
        {
            gamma = ((Hand)left).gamma + (((Hand)right).gamma - ((Hand)left).gamma) / maxTime * curTime;
            zeta = ((Hand)left).zeta + (((Hand)right).zeta - ((Hand)left).zeta) / maxTime * curTime;
            dist = ((Hand)left).dist + (((Hand)right).dist - ((Hand)left).dist) / maxTime * curTime;
            for (int i = 0; i < 3; i++)
                coord[i] = ((Hand)left).coord[i] + (((Hand)right).coord[i] - ((Hand)left).coord[i]) / maxTime * curTime;
        }

        public override void ReadJson(JsonTextReader reader)
        {
            while (reader.TokenType != JsonToken.EndObject)
            {
                if (reader.TokenType == JsonToken.PropertyName)
                {
                    var token = reader.Value;
                    reader.Read();
                    switch (token)
                    {
                        case propX:
                            coord[0] = (double)reader.Value;
                            break;
                        case propY:
                            coord[1] = (double)reader.Value;
                            break;
                        case propZ:
                            coord[2] = (double)reader.Value;
                            break;
                        case propGamma:
                            gamma = (double)reader.Value;
                            break;
                        case propZeta:
                            zeta = (double)reader.Value;
                            break;
                        case propDist:
                            dist = (double)reader.Value;
                            break;
                    }
                }
                reader.Read();
            }
        }

        public override void WriteJson(JsonWriter writer)
        {
            writer.WriteStartObject();
            writer.WritePropertyName(propType);
            writer.WriteValue(GetType().FullName);
            writer.WritePropertyName(propX);
            writer.WriteValue(coord[0]);
            writer.WritePropertyName(propY);
            writer.WriteValue(coord[1]);
            writer.WritePropertyName(propZ);
            writer.WriteValue(coord[2]);
            writer.WritePropertyName(propGamma);
            writer.WriteValue(gamma);
            writer.WritePropertyName(propZeta);
            writer.WriteValue(zeta);
            writer.WritePropertyName(propDist);
            writer.WriteValue(dist);
            writer.WriteEndObject();
        }

        private double CalcBeta(double dist, (double l1, double l2, double l3) lengths)
        {
            var sides = GetSides(dist, lengths);

            return AdmMath.CosTh(sides.a, sides.c, sides.b);
        }

        private double CalcPhi(double dist, (double l1, double l2, double l3) lengths)
        {
            var sides = GetSides(dist, lengths);

            return AdmMath.CosTh(sides.b, sides.c, sides.a);
        }

        private (double a, double b, double c) GetSides(double dist, (double l1, double l2, double l3) lengths)
        {
            double coeff = lengths.l3 / lengths.l1;

            double a = lengths.l2 / (coeff + 1);
            double b = dist / (coeff + 1);
            double c = lengths.l1;

            return (a, b, c);
        }

        private void UpdateAngles(Coord3 newCoord)
        {
            var newDist = AdmMath.GetDist(newCoord);

            double coeff = 1;
            if (newDist > maxDist - 1)
            {
                coeff = maxDist / newDist;
                dist = maxDist - 1;
            }
            else if (newDist < minDist + 1)
            {
                coeff = minDist / newDist;
                dist = minDist + 1;
            }
            else
            {
                dist = newDist;
            }

            newCoord = new Coord3(newCoord.x * coeff, newCoord.y * coeff, newCoord.z * coeff);

            zeta = Math.Asin(newCoord.z / dist);

            gamma = Math.Atan2(newCoord.y, newCoord.x);
        }
    }
}
