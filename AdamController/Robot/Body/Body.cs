﻿using Newtonsoft.Json;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using System.Reactive.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdamController.Robot
{
    public class Body : Unit
    {
        private const double pressLength = 83.18;
        [Reactive] public double height { get; set; } = 0;
        public const double maxHeight = 80;
        public const double minHeight = 0;
        [Reactive] public double rotate { get; set; } = 0;
        public const double maxRotate = 90;
        public const double minRotate = -90;
        [Reactive] public bool isActive { get; set; } = true;

        private readonly ObservableAsPropertyHelper<double> _nod;
        public double nod => _nod.Value;
        public override int[] angles => new int[3] { (int)nod, (int)nod, (int)rotate };
        public override int[] speeds => new int[3] { 0, 0, 0 };

        private const string propType = "Type";
        private const string propHeight = "Height";
        private const string propRotate = "Rotate";

        public Body(Servo[] servoArr) : base(servoArr)
        {
            if (servoArr.Length != 3) throw new Exception(); // need change

            this.WhenAnyValue(m => m.rotate, m => m.height)
                .Subscribe(t => { UpdateRotateAndHeight(t.Item1, t.Item2); }); //must change

            _nod = this.WhenAnyValue(vm => vm.height)
                .Select(t => Math.Asin(t / pressLength) * 180 / Math.PI)
                .ToProperty(this, vm => vm.nod);

            this.WhenAnyValue(m => m.nod, m => m.rotate)
                .Subscribe(t => {
                    //isUpdated = true;
                    SetAngleAndSpeed(angles, speeds); 
                });
        }

        public Body() 
        {
            this.WhenAnyValue(m => m.rotate, m => m.height)
                .Subscribe(t => { UpdateRotateAndHeight(t.Item1, t.Item2); }); //must change

            _nod = this.WhenAnyValue(vm => vm.height)
                .Select(t => Math.Asin(t / pressLength) * 180 / Math.PI)
                .ToProperty(this, vm => vm.nod);
        }

        public Body(Body body) : this(body.servos.Values.ToArray())
        {
            height = body.height;
            rotate = body.rotate;
        }

        public override ITime Clone() => new Body(this);
        public override ITime Get() => this;

        public (double height, double rotate) GetOffest(Body body)
        {
            return (body.height - height, body.rotate - rotate);
        }

        public override void Update(object left, object right, int maxTime, int curTime)
        {
            height = ((Body)left).height + (((Body)right).height - ((Body)left).height) / maxTime * curTime;
            rotate = ((Body)left).rotate + (((Body)right).rotate - ((Body)left).rotate) / maxTime * curTime;
        }

        public override void ReadJson(JsonTextReader reader)
        {
            while (reader.TokenType != JsonToken.EndObject)
            {
                if (reader.TokenType == JsonToken.PropertyName)
                {
                    var token = reader.Value;
                    reader.Read();
                    switch (token)
                    {
                        case propHeight:
                            height = (double)reader.Value;
                            break;
                        case propRotate:
                            rotate = (double)reader.Value;
                            break;
                    }
                }
                reader.Read();
            }
        }

        public override void WriteJson(JsonWriter writer)
        {
            writer.WriteStartObject();
            writer.WritePropertyName(propType);
            writer.WriteValue(GetType().FullName);
            writer.WritePropertyName(propHeight);
            writer.WriteValue(height);
            writer.WritePropertyName(propRotate);
            writer.WriteValue(rotate);
            writer.WriteEndObject();
        }

        private void UpdateRotateAndHeight(double newRotate, double newHeight)
        {
            if (newRotate > maxRotate) rotate = maxRotate;
            else if (newRotate < minRotate) rotate = minRotate;
            else rotate = newRotate;

            if (newHeight > maxHeight) height = maxHeight;
            else if (newHeight < minHeight) height = minHeight;
            else height = newHeight;
        }
    }
}
