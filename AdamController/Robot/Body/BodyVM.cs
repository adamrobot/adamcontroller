﻿using Newtonsoft.Json;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using System;
using System.Collections.Generic;
using System.Reactive.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdamController.Robot
{
    public class BodyVM : ReactiveObject
    {
        [Reactive] public Body body { get; set; }

        public BodyVM()
        {
            Servo[] data = {
                Servo.servos[ServoID.BodyBot], Servo.servos[ServoID.BodyTop], Servo.servos[ServoID.BodyRotate]
            };

            body = new Body(data);
        }
    }
}
