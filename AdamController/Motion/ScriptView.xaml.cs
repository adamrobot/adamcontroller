﻿using Newtonsoft.Json;
using ReactiveUI;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reactive;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace AdamController
{
    public partial class ScriptView : UserControl, IViewFor<ScriptVM>
    {
        public static readonly DependencyProperty ViewModelProperty = DependencyProperty
           .Register(nameof(ViewModel), typeof(ScriptVM), typeof(ScriptView));

        public ScriptVM ViewModel
        {
            get => (ScriptVM)GetValue(ViewModelProperty);
            set => SetValue(ViewModelProperty, value);
        }

        object IViewFor.ViewModel
        {
            get => ViewModel;
            set => ViewModel = (ScriptVM)value;
        }

        public ReactiveCommand<Unit, Unit> saveCommand { get; set; }

        public ScriptView(ScriptVM scriptVM)
        {
            InitializeComponent();
            DataContext = this;

            ViewModel = scriptVM;

            InitSaveCommand();
        }

        private void InitSaveCommand()
        {
            var canExecute = this.WhenAnyValue(mv => mv.ViewModel.isSaved, isSaved => !isSaved);

            saveCommand = ReactiveCommand.Create(
                () =>
                {
                    StringBuilder sb = new StringBuilder();
                    StringWriter sw = new StringWriter(sb);
                    JsonWriter writer = new JsonTextWriter(sw);
                    ViewModel.WriteJson(writer);
                    string path = ScriptVM.folderPath + ViewModel.name + ScriptVM.extensions[ViewModel.extension];
                    File.WriteAllText(path, sb.ToString());

                    ViewModel.isSaved = true;
                }
                //canExecute
                );
        }
    }
}
