﻿using ReactiveUI;
using ReactiveUI.Legacy;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Reactive;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace AdamController
{
    public partial class ScriptTreeV : UserControl, IViewFor<ScriptTreeVM>
    {
        public static readonly DependencyProperty ViewModelProperty = DependencyProperty
           .Register(nameof(ViewModel), typeof(ScriptTreeVM), typeof(ScriptTreeV));

        public ScriptTreeVM ViewModel
        {
            get => (ScriptTreeVM)GetValue(ViewModelProperty);
            set => SetValue(ViewModelProperty, value);
        }

        object IViewFor.ViewModel
        {
            get => ViewModel;
            set => ViewModel = (ScriptTreeVM)value;
        }        

        public ScriptTreeV()
        {
            InitializeComponent();
            ViewModel = new ScriptTreeVM();
            DataContext = ViewModel;

            motionTvi.ItemsSource = ViewModel.moveScripts;
            ledTvi.ItemsSource = ViewModel.ledScripts;
            audioTvi.ItemsSource = ViewModel.audioScripts;
            wheelsTvi.ItemsSource = ViewModel.wheelsScripts;
            videoTvi.ItemsSource = ViewModel.videoScripts;
            recognTvi.ItemsSource = ViewModel.recognScripts;

            LoadScripts(ViewModel.moveScripts, ScriptVM.Extension.MOVEMENT);
            LoadScripts(ViewModel.audioScripts, ScriptVM.Extension.AUDIO);
            LoadScripts(ViewModel.ledScripts, ScriptVM.Extension.LED);
            LoadScripts(ViewModel.videoScripts, ScriptVM.Extension.VIDEO);
            LoadScripts(ViewModel.wheelsScripts, ScriptVM.Extension.WHEELS);
            LoadScripts(ViewModel.recognScripts, ScriptVM.Extension.RECOGN);

            InitCommands();
        }

        private void InitCommands()
        {
            ViewModel.addCommand = ReactiveCommand.Create(
                () =>
                {
                    CreateScriptV csv = new CreateScriptV();
                    csv.Show();
                    //foreach (ScriptVM script in ViewModel.scripts)
                    //    if (nameTB.Text == script.name)
                    //        return;
                    //
                    //ScriptVM scriptVM = new ScriptVM(ViewModel.extension);
                    //scriptVM.name = nameTB.Text;
                    //
                    //ViewModel.scripts.Add(scriptVM);
                }
                );

            ViewModel.removeCommand = ReactiveCommand.Create(
                () =>
                {
                    if (treeView.SelectedItem is ScriptVM)
                    {
                        var vm = (ScriptVM)treeView.SelectedItem;
                        TryRemove(vm, (ReactiveList<ScriptVM>)motionTvi.ItemsSource);
                        TryRemove(vm, (ReactiveList<ScriptVM>)ledTvi.ItemsSource);
                        TryRemove(vm, (ReactiveList<ScriptVM>)audioTvi.ItemsSource);
                        TryRemove(vm, (ReactiveList<ScriptVM>)wheelsTvi.ItemsSource);
                        TryRemove(vm, (ReactiveList<ScriptVM>)videoTvi.ItemsSource);
                        TryRemove(vm, (ReactiveList<ScriptVM>)recognTvi.ItemsSource);
                    }
                    //File.Delete(ScriptVM.folderPath + vm.name + ScriptVM.extensions[ViewModel.extension]);
                }
                );
        }

        private void TryRemove(ScriptVM vm, ReactiveList<ScriptVM> scripts)
        {
            foreach (ScriptVM script in scripts)
                if (vm == script)
                {
                    scripts.Remove(vm);
                    return;
                }
        }

        private void LoadScripts(ReactiveList<ScriptVM> scripts, ScriptVM.Extension extension)
        {
            foreach (string file in Directory.EnumerateFiles(ScriptVM.folderPath, "*" + ScriptVM.extensions[extension]))
            {
                string text = File.ReadAllText(file);
                ScriptVM scriptVM = new ScriptVM(extension);
                scriptVM.ReadJson(text);
                scriptVM.name = Path.GetFileNameWithoutExtension(file);
                scriptVM.isSaved = true;

                scripts.Add(scriptVM);
            }
            //if (scripts.Count() < 1)
            //    addCommand.Execute().Subscribe();
        }

        private void TreeViewSelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            if (((TreeView)e.Source).SelectedItem is ScriptVM)
                ViewModel.selectedScript = (ScriptVM)((TreeView)e.Source).SelectedItem;
            else
                ViewModel.selectedScript = null;
        }
    }
}
