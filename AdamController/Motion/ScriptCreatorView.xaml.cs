﻿using ReactiveUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;
using System.Reactive.Disposables;
using System.Collections.ObjectModel;
using System.Reactive;
using System.Reactive.Linq;
using System.IO;
using Newtonsoft.Json;
using ReactiveUI.Legacy;

namespace AdamController
{
    public partial class ScriptCreatorView : UserControl
    {     
        public ScriptCreatorView()
        {
            InitializeComponent();
            DataContext = this;        

            this.WhenAnyValue(c => c.scriptListView.ViewModel.selectedScript)
                .Subscribe(mvm => { timelinesControllerView.ViewModel = mvm; });
        }

        public void Init(Robot.RobotVM robotVM)
        {
            timelinesControllerView.Init(robotVM, ScriptVM.Extension.MOVEMENT);
            scriptListView.Init(ScriptVM.Extension.MOVEMENT);
        }

        public void Init(FingersVM leftFingersVM, FingersVM rightFingersVM)
        {
            timelinesControllerView.Init(leftFingersVM, rightFingersVM, ScriptVM.Extension.ARMS);
            scriptListView.Init(ScriptVM.Extension.ARMS);
        }

        public void Init(WheelsVM wheelsVM)
        {
            timelinesControllerView.Init(wheelsVM, ScriptVM.Extension.WHEELS);
            scriptListView.Init(ScriptVM.Extension.WHEELS);
        }

        public void Init(AllElementsVM allElementsVM)
        {
            timelinesControllerView.Init(allElementsVM, ScriptVM.Extension.ALLELEMENTS);
            scriptListView.Init(ScriptVM.Extension.ALLELEMENTS);
        }

        public void Init(LedVM leftLedVM, LedVM rightLedVM)
        {
            timelinesControllerView.Init(leftLedVM, rightLedVM, ScriptVM.Extension.LED);
            scriptListView.Init(ScriptVM.Extension.LED);
        }
        //
        //public void Init(AudioVM audioVM)
        //{
        //    timelinesControllerView.Init(audioVM, ScriptVM.Extension.AUDIO);
        //    scriptListView.Init(ScriptVM.Extension.AUDIO);
        //}
    }
}
