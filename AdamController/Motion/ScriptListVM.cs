﻿using Newtonsoft.Json;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using ReactiveUI.Legacy;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Reactive.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdamController
{
    public class ScriptListVM : ReactiveObject
    {           
        [Reactive] public ScriptVM selectedScript { get; set; }
        public ReactiveList<ScriptVM> scripts { get; set; } = new ReactiveList<ScriptVM>();        
        [Reactive] public ScriptVM.Extension extension { get; set; }
        
        public ScriptListVM(ScriptVM.Extension extension)
        {
            this.extension = extension;
            selectedScript = new ScriptVM(extension);
        }
    }
}
