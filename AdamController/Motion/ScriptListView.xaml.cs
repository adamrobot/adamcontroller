﻿using ReactiveUI;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Reactive;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace AdamController
{
    public partial class ScriptListView : UserControl, IViewFor<ScriptListVM>
    {
        public static readonly DependencyProperty ViewModelProperty = DependencyProperty
           .Register(nameof(ViewModel), typeof(ScriptListVM), typeof(ScriptListView));

        public ScriptListVM ViewModel
        {
            get => (ScriptListVM)GetValue(ViewModelProperty);
            set => SetValue(ViewModelProperty, value);
        }

        object IViewFor.ViewModel
        {
            get => ViewModel;
            set => ViewModel = (ScriptListVM)value;
        }
        
        public ReactiveCommand<Unit, Unit> addCommand { get; set; }        
        public ReactiveCommand<Unit, Unit> removeCommand { get; set; }

        public ObservableCollection<ScriptView> scriptViews { get; set; } = new ObservableCollection<ScriptView>();        

        public ScriptListView()
        {
            InitializeComponent();
            DataContext = this;            

            InitCommands();            
            listView.SelectedIndex = 0;
        }
        
        public void Init(ScriptVM.Extension extension)
        {
            ViewModel = new ScriptListVM(extension);
            ViewModel.scripts.ItemsAdded.Subscribe(mvm =>
            {
                scriptViews.Add(new ScriptView(mvm));
                scriptViews.Last().saveCommand.Execute().Subscribe();
            });
            ViewModel.scripts.ItemsRemoved.Subscribe(mvm =>
            {
                foreach (var mv in scriptViews)
                    if (mv.ViewModel == mvm)
                    {
                        scriptViews.Remove(mv);
                        return;
                    }
            });

            LoadScripts();
        }

        private void LoadScripts()
        {
            foreach (string file in Directory.EnumerateFiles(ScriptVM.folderPath, "*" + ScriptVM.extensions[ViewModel.extension]))
            {
                string text = File.ReadAllText(file);
                ScriptVM scriptVM = new ScriptVM(ViewModel.extension);
                scriptVM.ReadJson(text);
                scriptVM.name = Path.GetFileNameWithoutExtension(file);
                scriptVM.isSaved = true;

                ViewModel.scripts.Add(scriptVM);
            }
            if (ViewModel.scripts.Count() < 1)
                addCommand.Execute().Subscribe();
        }

        private void InitCommands()
        {
            addCommand = ReactiveCommand.Create(
                () =>
                {
                    foreach (ScriptVM script in ViewModel.scripts)
                        if (nameTB.Text == script.name)
                            return;

                    ScriptVM scriptVM = new ScriptVM(ViewModel.extension);
                    scriptVM.name = nameTB.Text;

                    ViewModel.scripts.Add(scriptVM);
                }
                );            

            removeCommand = ReactiveCommand.Create(
                () =>
                {
                    if (ViewModel.scripts.Count() <= 1)
                        return;
                    var vm = ((ScriptView)listView.SelectedItem).ViewModel;
                    ViewModel.scripts.Remove(vm);
                    File.Delete(ScriptVM.folderPath + vm.name + ScriptVM.extensions[ViewModel.extension]);
                }
                );
        }

        private bool IsExist(string name)
        {
            foreach (var script in ViewModel.scripts)
                if (script.name == name) return true;
            return false;
        }

        private void ListViewSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ScriptView mv = (ScriptView)((ListView)e.Source).SelectedItem;
            if (mv == null)
            {
                ((ListView)e.Source).SelectedIndex = 0;
                return;
            }

            ViewModel.selectedScript = mv.ViewModel;
        }
    }
}
