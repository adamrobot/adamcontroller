﻿using Newtonsoft.Json;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using ReactiveUI.Legacy;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Reactive.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdamController
{
    public class ScriptVM : ReactiveObject
    {
        public enum Extension
        {
            MOVEMENT    = 0,
            LED         = 1,
            AUDIO       = 2,
            VIDEO       = 3,
            WHEELS      = 4,
            RECOGN      = 5,
            SCRIPT      = 6,
            ARROW       = 7,
            ARMS        = 8,
            ALLELEMENTS = 9,
        }
        
        [Reactive] public string name { get; set; } = "";
        [Reactive] public ReactiveList<ReactiveList<TimeUnitVM>> listTUVMs { get; set; } = new ReactiveList<ReactiveList<TimeUnitVM>>();
        [Reactive] public bool isSaved { get; set; } = false;

        static public Dictionary<Extension, string> extensions { get; set; } = new Dictionary<Extension, string>
        {
            { Extension.MOVEMENT,    ".move"   },
            { Extension.LED,         ".led"    },
            { Extension.AUDIO,       ".audio"  },
            { Extension.VIDEO,       ".video"  },
            { Extension.WHEELS,      ".wheels" },
            { Extension.RECOGN,      ".recogn" },
            { Extension.SCRIPT,      ".script" },
            { Extension.ARMS,        ".arms"   },
            { Extension.ALLELEMENTS, ".all"    }
        };

        static public string folderPath { get; set; } = @"D:\IlyaZuev\AdmProject\VR_16.07.2020\Wpf\AdamController\Scripts\";

        [Reactive] public Extension extension { get; set; }

        private const string propName      = "Name";
        private const string propListTUVMs = "ListTUVMs";

        public ScriptVM(Extension extension, int listTUVMCount = 5)
        {
            this.extension = extension;

            for (int i = 0; i < listTUVMCount; i++)
                listTUVMs.Add(new ReactiveList<TimeUnitVM>());
        }

        public void ReadJson(string text)
        {
            JsonTextReader reader = new JsonTextReader(new StringReader(text));

            while (reader.Read())
            {
                if (reader.TokenType == JsonToken.PropertyName)
                {
                    switch (reader.Value)
                    {
                        case propName:
                            reader.Read();
                            name = (string)reader.Value;
                            break;
                        case propListTUVMs:
                            ReadJsonListTUVMs(reader);
                            break;
                    }
                }
            }
        }

        private void ReadJsonListTUVMs(JsonTextReader reader)
        {
            reader.Read(); // read StartArray token            
            reader.Read(); // read StartArray token

            int index = 0;
            while (reader.TokenType != JsonToken.EndArray)
            {
                reader.Read(); // read StartObject or EndArray token
                while (reader.TokenType != JsonToken.EndArray)
                {
                    TimeUnitVM tuvm = new TimeUnitVM(0, null);
                    tuvm.ReadJson(reader);
                    listTUVMs[index].Add(tuvm);
                    reader.Read();
                }
                index++;
                reader.Read(); // read EndArray or Start Array token
            }
        }

        public void WriteJson(JsonWriter writer)
        {
            writer.WriteStartObject();
            writer.WritePropertyName(propName);
            writer.WriteValue(name);
            WriteJsonListTUVMs(writer);
            writer.WriteEndObject();
        }

        private void WriteJsonListTUVMs(JsonWriter writer)
        {
            writer.WritePropertyName(propListTUVMs);
            writer.WriteStartArray();
            foreach (var listTUVM in listTUVMs)
            {
                writer.WriteStartArray();
                foreach (var tuvm in listTUVM)
                    tuvm.WriteJson(writer);
                writer.WriteEnd();
            }                
            writer.WriteEnd();
        }

        //public MotionVM(List<TimeUnitVM> headTUVMs, List<TimeUnitVM> bodyTUVMs)
        //{
        //    this.headTUVMs = headTUVMs;
        //    this.bodyTUVMs = bodyTUVMs;
        //}
    }
}
