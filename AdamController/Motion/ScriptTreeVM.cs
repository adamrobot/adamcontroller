﻿using Newtonsoft.Json;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using ReactiveUI.Legacy;
using System;
using System.Collections.Generic;
using System.Reactive;
using System.Reactive.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdamController
{
    public class ScriptTreeVM : ReactiveObject
    {
        //[Reactive] public ScriptTreeM scriptTreeM { get; set; }
        [Reactive] public ScriptVM selectedScript { get; set; }
        [Reactive] public ReactiveList<ScriptVM> moveScripts { get; set; } = new ReactiveList<ScriptVM>();
        [Reactive] public ReactiveList<ScriptVM> audioScripts { get; set; } = new ReactiveList<ScriptVM>();
        [Reactive] public ReactiveList<ScriptVM> ledScripts { get; set; } = new ReactiveList<ScriptVM>();
        [Reactive] public ReactiveList<ScriptVM> videoScripts { get; set; } = new ReactiveList<ScriptVM>();
        [Reactive] public ReactiveList<ScriptVM> wheelsScripts { get; set; } = new ReactiveList<ScriptVM>();
        [Reactive] public ReactiveList<ScriptVM> recognScripts { get; set; } = new ReactiveList<ScriptVM>();

        public ReactiveCommand<Unit, Unit> addCommand { get; set; }
        public ReactiveCommand<Unit, Unit> removeCommand { get; set; }

        public ScriptTreeVM()
        {
            //scriptTreeM = new ScriptTreeM();
            //scriptTreeM.moveScripts.Add(new ScriptVM(ScriptVM.Extension.MOVEMENT));
        }
    }
}
