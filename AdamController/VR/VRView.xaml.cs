﻿using ReactiveUI;
using ReactiveUI.Legacy;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Reactive;
using System.Reactive.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Newtonsoft.Json;

namespace AdamController
{
    class Coords
    {
        public float PerX_left { get; set; }
        public float PerY_left { get; set; }
        public float PerZ_left { get; set; }

        public float PerX_right { get; set; }
        public float PerY_right { get; set; }
        public float PerZ_right { get; set; }

        public float Per_Body { get; set; }

        public float y_head { get; set; }
        public float x_head { get; set; }

        public float currentChoose { get; set; }

        public float handLeftTriggerPress { get; set; }
        public float handRightGTriggerPress { get; set; }

        public string DirectionToMove_ { get; set; }
    }

    public partial class VRView : UserControl
    {
        private enum BodyOption
        {
            PRESS = 1,
            LEGS  = 2,
            KNEES = 3
        }

        Coords ReadyData;

        private string data_ext;
        private Coord3 coordsLeft = new Coord3();
        private Coord3 coordsRight = new Coord3();
        private Robot.RobotVM robotVM;
        private BodyOption currentBodyOption;

        private double abs_body_pos_leg;
        private double abs_body_pos_pres;
        private double abs_body_pos_fullLeg;

        private bool FixPos;
        private BodyOption LastBodyOption;

        private bool inc;
        private bool dec;

        private double Value_full;
        private double Value_press;
        private double Value_leg;

        public ReactiveCommand<Unit, Unit> runCommand { get; set; }
        private Func<Task> run;

        public VRView()
        {
            InitializeComponent();
            DataContext = this;

            robotVM = new Robot.RobotVM();
            model3DView.ViewModel = new RobotModel3DVM(robotVM);

            run = async () =>
            {
                //VrMove();
                statusTB.Text = "Connected";

                IPHostEntry ipHost = Dns.GetHostEntry("localhost");
                IPAddress ipAddr = ipHost.AddressList[0];
                IPEndPoint ipEndPoint = new IPEndPoint(ipAddr, 11000);

                // Создаем сокет Tcp/Ip
                Socket sListener = new Socket(ipAddr.AddressFamily, SocketType.Stream, ProtocolType.Tcp);

                try
                {
                    sListener.Bind(ipEndPoint);
                    sListener.Listen(10);

                    statusTB.Text = "Started";
                    while (true)
                    {
                        Socket handler = sListener.Accept();
                        string data = null;

                        byte[] bytes = new byte[1024];

                        int bytesRec = handler.Receive(bytes);

                        data += Encoding.UTF8.GetString(bytes, 0, bytesRec);

                        messageTB.Text = data;

                        data_ext = data;

                        CheckPosition();
                        VrMove();
                        await Task.Delay(5);

                        string reply = "Спасибо за запрос в " + data.Length.ToString()
                                + " символов";

                        byte[] msg = Encoding.UTF8.GetBytes(reply);
                        handler.Send(msg);

                        if (data.IndexOf("<TheEnd>") > -1)
                        {
                            break;
                        }

                        handler.Shutdown(SocketShutdown.Both);
                        handler.Close();
                    }
                }
                catch (Exception e)
                {
                }

                for (int i = 0; i < 10000; i++)
                {
                    robotVM.bodyVM.body.height += 0.5;
                    await Task.Delay(5);
                }
            };

            runCommand = ReactiveCommand.CreateFromObservable(
                () => Observable.StartAsync(() => run()));
        }

        //public void Init(Robot.Robot robot)
        //{
        //    this.robot = robot;
        //}


        //Приравниваем полученные данные к координатам;
        void CheckPosition()
        {
            ReadyData = JsonConvert.DeserializeObject<Coords>(data_ext);
        }

        public void VrMove()         // 14 13
        {
            if (Int32.Parse(ReadyData.DirectionToMove_) != -1)
                WheelsVM.Send((DirectionVM.Direction)Int32.Parse(ReadyData.DirectionToMove_));

            //(Max - Min) * % + Min - Robot_C#
            double maxD = robotVM.leftHandVM.hand.maxDist;
            double minD = robotVM.leftHandVM.hand.minDist;

            //переводим занчения в нужные нам;
            double CoordLeft_x = (maxD * Math.Max(ReadyData.PerX_left, 0.1));
            double CoordLeft_y = (maxD * ReadyData.PerY_left);
            double CoordLeft_z = (maxD * Math.Max(ReadyData.PerZ_left, 0.1));

            double CoordRight_x = (maxD * Math.Max(ReadyData.PerX_right, 0.1));
            double CoordRight_y = (maxD * ReadyData.PerY_right);
            double CoordRight_z = (maxD * Math.Max(ReadyData.PerZ_right, 0.1));

            Coord3 coordsLeft_per = new Coord3(CoordLeft_z, CoordLeft_y, -CoordLeft_x);
            robotVM.leftHandVM.hand.coord = coordsLeft_per;

            Coord3 coordsRight_per = new Coord3(CoordRight_z, CoordRight_y, CoordRight_x);
            robotVM.rightHandVM.hand.coord = coordsRight_per;

            double MaxSpeed = 3;

            //теперь аккуратно считаем тело;
            double deltaKnee = Robot.Leg.maxKneeHeight - Robot.Leg.minKneeHeight;
            double deltaBody = Robot.Body.maxHeight - Robot.Body.minHeight;
            double deltaLeg = Robot.Leg.maxHeight - Robot.Leg.minHeight;

            double coeff = Math.Max(Math.Min(1, Math.Abs(ReadyData.Per_Body)) * Math.Sign(ReadyData.Per_Body), 0);
            //проценты корпуса по новой формуле (которую дали)
            abs_body_pos_leg = (deltaKnee * coeff) + Robot.Leg.minKneeHeight;
            abs_body_pos_pres = (deltaBody * coeff) + Robot.Body.minHeight;
            abs_body_pos_fullLeg = (deltaLeg * coeff) + Robot.Leg.minHeight;

            if (LastBodyOption != (BodyOption)ReadyData.currentChoose && FixPos == false)
            {
                Value_full = abs_body_pos_fullLeg;
                Value_press = abs_body_pos_pres;
                Value_leg = abs_body_pos_leg;

                FixPos = true;
                LastBodyOption = (BodyOption)ReadyData.currentChoose;
            }

            switch ((BodyOption)ReadyData.currentChoose)
            {
                case BodyOption.PRESS:
                    if (FixPos)
                    {
                        if (!inc && !dec)
                        {
                            if (robotVM.bodyVM.body.height > Value_press)
                                dec = true;
                            else
                                inc = true;
                        }

                        if (dec)
                        {
                            if (robotVM.bodyVM.body.height > Value_press)
                                robotVM.bodyVM.body.height -= MaxSpeed;
                            else
                            {
                                FixPos = false;
                                dec = false;
                            }
                        }
                        else
                        {
                            if (robotVM.bodyVM.body.height < Value_press)
                                robotVM.bodyVM.body.height += MaxSpeed;
                            else
                            {
                                FixPos = false;
                                inc = false;
                            }
                        }
                    }
                    else
                    {
                        robotVM.bodyVM.body.height = abs_body_pos_pres;
                    }

                    break;
                case BodyOption.LEGS:
                    if (FixPos)
                    {
                        if (!inc && !dec)
                        {
                            if (robotVM.leftLegVM.leg.height > Value_full)
                                dec = true;
                            else
                                inc = true;
                        }

                        if (dec)
                        {
                            if (robotVM.leftLegVM.leg.height > Value_full)
                            {
                                robotVM.rightLegVM.leg.height -= MaxSpeed;
                                robotVM.leftLegVM.leg.height -= MaxSpeed;
                            }
                            else
                            {
                                FixPos = false;
                                dec = false;
                            }
                        }
                        else
                        {
                            if (robotVM.leftLegVM.leg.height < Value_full)
                            {
                                robotVM.rightLegVM.leg.height += MaxSpeed;
                                robotVM.leftLegVM.leg.height += MaxSpeed;
                            }
                            else
                            {
                                FixPos = false;
                                inc = false;
                            }
                        }
                    }
                    else
                    {
                        robotVM.leftLegVM.leg.height = abs_body_pos_fullLeg;
                        robotVM.rightLegVM.leg.height = abs_body_pos_fullLeg;
                    }
                    break;
                case BodyOption.KNEES:
                    if (FixPos)
                    {
                        if (!inc && !dec)
                        {
                            if (robotVM.leftLegVM.leg.kneeHeight > Value_leg)
                                dec = true;
                            else
                                inc = true;
                        }

                        if (dec)
                        {
                            if (robotVM.leftLegVM.leg.kneeHeight > Value_leg)
                            {
                                robotVM.leftLegVM.leg.kneeHeight -= MaxSpeed;
                                robotVM.rightLegVM.leg.kneeHeight -= MaxSpeed;
                            }
                            else
                            {
                                FixPos = false;
                                dec = false;
                            }
                        }
                        else
                        {
                            if (robotVM.leftLegVM.leg.kneeHeight < Value_leg)
                            {
                                robotVM.leftLegVM.leg.kneeHeight += MaxSpeed;
                                robotVM.rightLegVM.leg.kneeHeight += MaxSpeed;
                            }
                            else
                            {
                                FixPos = false;
                                inc = false;
                            }
                        }
                    }
                    else
                    {
                        robotVM.leftLegVM.leg.kneeHeight = abs_body_pos_leg;
                        robotVM.rightLegVM.leg.kneeHeight = abs_body_pos_leg;
                    }
                    break;
            }

            //А голова, увы, считатся так. т.к. я понятия не имею, что тут творится в плане сервов и тд.
            if (ReadyData.y_head < 330 && ReadyData.y_head > -330)
            {
                float headX = (511 - (ReadyData.y_head / (300f / 1023f)));
                headX = (headX - Servo.servos[ServoID.HeadRotate].homePos) * 100 / 1023;
                robotVM.headVM.head.rotate = headX;

                float rotate = 0;
                float leftBound = -20;
                float rightBound = 20;
                if (-headX > rightBound) rotate = -headX - rightBound;
                else if (-headX < leftBound) rotate = -headX - leftBound;

                robotVM.bodyVM.body.rotate = rotate;
            }

            float headY = (511 - (((ReadyData.x_head * -1) * 180) / (300f / 1023f)));
            robotVM.headVM.head.nod = ReadyData.x_head;

            Thread.Sleep(10);
        }
    }
}
