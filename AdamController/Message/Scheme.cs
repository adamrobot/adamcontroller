﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdamController.Message
{
    public abstract class Scheme
    {
        protected Dictionary<int, Field> fields = new Dictionary<int, Field>();

        /*public void SetFieldValue<T>(int fieldId, T value)
        {
            var field = fields[fieldId];
            void* dataPoint = field.GetDataPoint();
            int size = field.GetSize();
            BinaryFormatter bf = new BinaryFormatter();
            var ms = new MemoryStream();
            bf.Serialize(ms, value);
            byte[] buf = ms.ToArray();
            for (int i = size - 1; i >= 0; i--)
                *((byte*)dataPoint + i) = buf[i];
            //*((char*)field.first + i) = (value >> (8 * i)) & 0xff;
        }*/

        abstract public Message Msg(Command command);
    }

    /*class GoalPosition : Scheme
    {
        public UInt16 gp;

        public override Message Msg(Command command, ref Dictionary<int, Field> fields)
        {
            return new Message(command, ref fields).SetTitle("goal position")
                    .Add(new Number(gp).SetTitle("goal position").SetMin(0).SetMax(2047));
        }
    };*/

    public class ServoScheme : Scheme
    {
        public byte[] ids;
        public byte[][] data;
        public byte size;
        public byte addr;

        public override Message Msg(Command command)
        {
            Message msg = new Message(command, fields);
            for (var i = 0; i < ids.Length; i++)
                msg.Add(new ServoField(ids[i], data[i].OfType<byte>().ToList(), size, addr));
            return msg;
        }

        public static ServoScheme operator +(ServoScheme a, ServoScheme b) 
        {
            ServoScheme scheme = new ServoScheme();

            if (a.addr != b.addr || a.size != b.size)
                throw new Exception();

            int length = a.ids.Length + b.ids.Length;
            scheme.ids = new byte[length];
            scheme.data = new byte[length][];
            scheme.addr = a.addr;
            scheme.size = a.size;
            for (var i = 0; i < length; i++)
                scheme.data[i] = new byte[a.size];

            a.ids.CopyTo(scheme.ids, 0);
            b.ids.CopyTo(scheme.ids, a.ids.Length);

            a.data.CopyTo(scheme.data, 0);
            b.data.CopyTo(scheme.data, a.data.Length);
            return scheme;
        }
    }

    public class LedScheme : Scheme
    {
        public List<int> startDiodes = new List<int>();
        public List<int> endDiodes = new List<int>();
        public List<DiodeVM.Color> colors = new List<DiodeVM.Color>();
        public LedVM.Type type;
    
        public override Message Msg(Command command)
        {
            LedMessage msg = new LedMessage(command, fields, type);
            for (var i = 0; i < startDiodes.Count; i++)
                msg.Add(new LedField(startDiodes[i], endDiodes[i], colors[i]));
            return msg;
        }
    }

    public class FingersScheme : Scheme
    {
        public double angle;
        public FingersVM.FingersID id;

        public override Message Msg(Command command)
        {
            FingersMessage msg = new FingersMessage(command, fields, id);
            msg.Add(new FingersField(angle, id));
            return msg;
        }
    }

    public class ArmsScheme : Scheme
    {
        public List<double> angles;
        public List<FingersVM.FingersID> ids;

        public override Message Msg(Command command)
        {
            FingersMessage msg = new FingersMessage(command, fields, ids[0]);
            for (int i = 0; i < angles.Count; i++)
                msg.Add(new FingersField(angles[i], ids[i]));
            return msg;
        }
    }

    public class WheelScheme : Scheme
    {
        public double speed;
        public WheelVM.WheelID id;

        public override Message Msg(Command command)
        {
            WheelMessage msg = new WheelMessage(command, fields, id);
            msg.Add(new WheelField(speed, id));
            return msg;
        }
    }

    public class WheelsScheme : Scheme
    {
        public List<double> speeds;
        public List<WheelVM.WheelID> ids;

        public override Message Msg(Command command)
        {
            WheelMessage msg = new WheelMessage(command, fields, ids[0]);
            for (int i = 0; i < speeds.Count; i++)
                msg.Add(new WheelField(speeds[i], ids[i]));
            return msg;
        }
    }

    public class ServoScriptScheme : Scheme
    {
        //ReactiveList<ReactiveList<TimeUnitVM>>
        public List<List<(int time, List<ServoField> fields)>> units;

        public override Message Msg(Command command)
        {
            Message msg = new Message(command, fields);
            for (var i = 0; i < units.Count; i++)
                msg.Add(new ServoScriptField(units[i]));
            return msg;
        }
    }

    public class DefaultServoScheme : Scheme
    {
        //ReactiveList<ReactiveList<TimeUnitVM>>
        public List<List<ServoField>> units;

        public override Message Msg(Command command)
        {
            Message msg = new Message(command, fields);
            msg.Add(new DefaultServoField(units));
            return msg;
        }
    }

    public class LedScriptScheme : Scheme
    {
        //ReactiveList<ReactiveList<TimeUnitVM>>
        public List<List<(int time, LedVM.Type type, List<LedField> fields)>> units;
    
        public override Message Msg(Command command)
        {
            Message msg = new Message(command, fields);
            for (var i = 0; i < units.Count; i++)
                msg.Add(new LedScriptField(units[i]));
            return msg;
        }
    }
    
    public class DefaultLedScheme : Scheme
    {
        //ReactiveList<ReactiveList<TimeUnitVM>>
        public List<(LedVM.Type type, List<LedField> fields)> units;
    
        public override Message Msg(Command command)
        {
            Message msg = new Message(command, fields);
            msg.Add(new DefaultLedField(units));
            return msg;
        }
    }

    public class WheelScriptScheme : Scheme
    {
        //ReactiveList<ReactiveList<TimeUnitVM>>
        public List<List<(int time, WheelVM.WheelID id, List<WheelField> fields)>> units;

        public override Message Msg(Command command)
        {
            Message msg = new Message(command, fields);
            for (var i = 0; i < units.Count; i++)
                msg.Add(new WheelScriptField(units[i]));
            return msg;
        }
    }
    public class FingersScriptScheme : Scheme
    {
        public List<List<(int time, FingersVM.FingersID id, List<FingersField> fields)>> units;

        public override Message Msg(Command command)
        {
            Message msg = new Message(command, fields);
            for (var i = 0; i < units.Count; i++)
                msg.Add(new FingersScriptField(units[i]));
            return msg;
        }
    }

    public class RunScriptScheme : Scheme
    {
        public override Message Msg(Command command)
        {
            Message msg = new Message(command, fields);
            return msg;
        }
    }

    //public class AudioScheme : Scheme
    //{
    //    public byte[] data;
    //
    //    public override Message Msg(Command command)
    //    {
    //        return new Message(command, fields).
    //            Add(new DataField(data.OfType<byte>().ToList()));
    //    }
    //}
}
