﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdamController.Message
{
    public enum FieldType
    {
        SERVOSCRIPT   = 1,
        SERVO         = 2,
        LED           = 3,
        LEDSCRIPT     = 4,
        WHEEL         = 5,
        WHEELSCRIPT   = 6,
        FINGERS       = 7,
        FINGERSSCRIPT = 8
    };

    public abstract class Field
    {
        public FieldType type { get; protected set; }
    };

    public class ServoField : Field
    {
        public byte id { get; private set; }
        public List<byte> data { get; private set; }
        public byte size { get; private set; }
        public byte addr { get; private set; }

        public ServoField(byte id, List<byte> data, byte size, byte addr)
        {
            this.id = id;
            this.addr = addr;
            this.data = data;
            this.size = size;
            type = FieldType.SERVO;
        }
    }

    public class LedField : Field
    {
        public int startDiode { get; set; }
        public int endDiode { get; set; }
        public DiodeVM.Color color { get; set; }
    
        public LedField(int startDiode, int endDiode, DiodeVM.Color color)
        {
            this.startDiode = startDiode;
            this.endDiode = endDiode;
            this.color = color;
            type = FieldType.LED;
        }
    }

    public class WheelField : Field
    {
        public double speed { get; set; }
        public WheelVM.WheelID id { get; set; }

        public WheelField(double speed, WheelVM.WheelID id)
        {
            this.speed = speed;
            this.id = id;
            type = FieldType.WHEEL;
        }
    }

    public class FingersField : Field
    {
        public double angle { get; set; }
        public FingersVM.FingersID id { get; set; }

        public FingersField(double angle, FingersVM.FingersID id)
        {
            this.angle = angle;
            this.id = id;
            type = FieldType.FINGERS;
        }
    }

    public class ServoScriptField : Field
    {
        public List<(int time, List<ServoField> fields)> units { get; private set; }

        public ServoScriptField(List<(int time, List<ServoField> fields)> units)
        {
            this.units = units;
            type = FieldType.SERVOSCRIPT;
        }
    }

    public class DefaultServoField : Field
    {
        public List<List<ServoField>> units { get; private set; }

        public DefaultServoField(List<List<ServoField>> units)
        {
            this.units = units;
            type = FieldType.SERVOSCRIPT;
        }
    }

    public class LedScriptField : Field
    {
        public List<(int, LedVM.Type type, List<LedField> fields)> units { get; private set; }
    
        public LedScriptField(List<(int time, LedVM.Type type, List<LedField> fields)> units)
        {
            this.units = units;
            type = FieldType.LEDSCRIPT;
        }
    }
    
    public class DefaultLedField : Field
    {
        public List<(LedVM.Type type, List<LedField> fields)> units { get; private set; }
    
        public DefaultLedField(List<(LedVM.Type type, List<LedField> fields)> units)
        {
            this.units = units;
            type = FieldType.LEDSCRIPT;
        }
    }

    public class WheelScriptField : Field
    {
        public List<(int, WheelVM.WheelID id, List<WheelField> fields)> units { get; private set; }

        public WheelScriptField(List<(int, WheelVM.WheelID id, List<WheelField> fields)> units)
        {
            this.units = units;
            type = FieldType.WHEELSCRIPT;
        }
    }

    public class FingersScriptField : Field
    {
        public List<(int, FingersVM.FingersID id, List<FingersField> fields)> units { get; private set; }

        public FingersScriptField(List<(int, FingersVM.FingersID id, List<FingersField> fields)> units)
        {
            this.units = units;
            type = FieldType.FINGERSSCRIPT;
        }
    }
}
