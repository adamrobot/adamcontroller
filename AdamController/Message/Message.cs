﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdamController.Message
{
    public enum Command
    {
        WRITE              = 0,
        REPEATREAD         = 1,
        READ               = 3,
        STOPREAD           = 4,
        UPLOADSERVOSCRIPT  = 5,
        RUNSERVOSCRIPT     = 6,
        STOPSERVOSCRIPT    = 7,
        SETLED             = 8,
        UPLOADLEDSCRIPT    = 9,
        RUNLEDSCRIPT       = 10,
        STOPLEDSCRIPT      = 11,
        DEFAULTSERVO       = 12,
        DEFAULTLEDS        = 13,
        RUNWHEELSSCRIPT    = 14,
        STOPWHEELSSCRIPT   = 15,
        UPLOADWHEELSSCRIPT = 16,
        RUNARMSSSCRIPT     = 17,
        STOPARMSSCRIPT     = 18,
        UPLOADARMSSCRIPT   = 19
    };

    public class Message
    {
        protected int fieldId;

        public int id { get; set; }
        public Command command { get; protected set; }
        public Dictionary<int, Field> fields { get; protected set; }

        static public JsonSerializerSettings jsonSettings = new JsonSerializerSettings { TypeNameHandling = TypeNameHandling.Auto };

        public Message(Command command, Dictionary<int, Field> fields)
        {
            this.command = command;
            this.fields = fields;
            fieldId = 0;
        }

        public Message Add(Field field)
        {
            fieldId++;

            if (!fields.ContainsKey(fieldId))
                fields.Add(fieldId, field);
            else
                fields[fieldId] = field;

            return this;
        }

        public byte[] GetData()
        {
            string json = JsonConvert.SerializeObject(this, Formatting.Indented, jsonSettings);
            byte[] size = BitConverter.GetBytes((Int32)json.Length);
            Array.Reverse(size);
            byte[] jsonArr = Encoding.UTF8.GetBytes(json);
            byte[] result = new byte[size.Length + jsonArr.Length];

            size.CopyTo(result, 0);
            jsonArr.CopyTo(result, size.Length);

            return result;
        }

        /*public Message SetTitle(string title)
        {
            this.title = title;
            return this;
        }*/

        public Message SetId(int id)
        {
            this.id = id;
            return this;
        }

    };

    public class LedMessage : Message
    {
        public LedVM.Type type { get; protected set; }
    
        public LedMessage(Command command, Dictionary<int, Field> fields, LedVM.Type type) : base(command, fields)
        {
            this.type = type;
        }
    }

    public class WheelMessage : Message
    {
        public WheelVM.WheelID id { get; protected set; }

        public WheelMessage(Command command, Dictionary<int, Field> fields, WheelVM.WheelID id) : base(command, fields)
        {
            this.id = id;
        }
    }

    public class FingersMessage : Message
    {
        public FingersVM.FingersID id { get; protected set; }

        public FingersMessage(Command command, Dictionary<int, Field> fields, FingersVM.FingersID id) : base(command, fields)
        {
            this.id = id;
        }
    }
}
