﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdamController.ControlTable
{
    partial class ControlTable
    {
        public Dictionary<CtFieldName, CtField> fields { get; private set; }
        public byte minAddr { get; private set; }
        public byte maxAddr { get; private set; }

        private ControlTable(List<string> lines)
        {
            fields = new Dictionary<CtFieldName, CtField>();

            minAddr = 0;
            maxAddr = 0;

            foreach (string line in lines)
            {
                CtField field = new CtField(line);
                minAddr = Math.Min(minAddr, field.addr);
                maxAddr = Math.Max(maxAddr, (byte)(field.addr + field.size - 1));
                fields.Add(field.name, field);
            }
        }
    }
}
