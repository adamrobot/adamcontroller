﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace AdamController.ControlTable
{
    enum CtFieldAccess
    {
        R,
        RW
    }

    enum CtFieldName
    {
        MODEL_NUMBER,
        VERSION_OF_FIRMWARE,
        ID,
        BAUD_RATE,
        RETURN_DELAY_TIME,
        CW_ANGLE_LIMIT,
        CCW_ANGLE_LIMIT,
        THE_HIGHEST_LIMIT_TEMPERATURE,
        THE_LOWEST_LIMIT_VOLTAGE,
        THE_HIGHEST_LIMIT_VOLTAGE,
        MAX_TORQUE,
        STATUS_RETURN_LEVEL,
        ALARM_LED,
        ALARM_SHUTDOWN,
        MULTI_TURN_OFFSET,
        RESOLUTION_DIVIDER,
        TORQUE_ENABLE,
        LED,
        D_GAIN,
        I_GAIN,
        P_GAIN,
        GOAL_POSITION,
        MOVING_SPEED,
        TORQUE_LIMIT,
        PRESENT_POSITION,
        PRESENT_SPEED,
        PRESENT_LOAD,
        PRESENT_VOLTAGE,
        PRESENT_TEMPERATURE,
        REGISTERED,
        MOVING,
        LOCK,
        PUNCH,
        CURRENT,
        TORQUE_CONTROL_MODE_ENABLE,
        GOAL_TORQUE,
        GOAL_ACCELERATION,

        CW_COMPLIANCE_MARGIN,
        CCW_COMPLIANCE_MARGIN,
        CW_COMPLIANCE_SLOPE,
        CCW_COMPLIANCE_SLOPE
    }

    class CtField
    {
        public CtFieldName name { get; private set; }
        public byte addr { get; private set; }
        public byte size { get; private set; }
        public string text { get; private set; }
        public CtFieldAccess access { get; private set; }
        public string initValue { get; private set; }
        public string value { get; set; }

        public CtField(string str)
        {
            Regex regex = new Regex("\\s*(.+)\\s+(.+)\\s+(.+)\\s+\"(.*)\"\\s+(.+)\\s+(.+)\\s*");
            MatchCollection matches = regex.Matches(str);
            if (matches.Count != 1)
                throw new Exception();

            try
            {
                name = (CtFieldName)Enum.Parse(typeof(CtFieldName), matches[0].Groups[1].Value);
                addr = Convert.ToByte(matches[0].Groups[2].Value);
                size = Convert.ToByte(matches[0].Groups[3].Value);
                text = matches[0].Groups[4].Value;
                access = (CtFieldAccess)Enum.Parse(typeof(CtFieldAccess), matches[0].Groups[5].Value);
                initValue = matches[0].Groups[6].Value;
                value = initValue;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}
