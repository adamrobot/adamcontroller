﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace AdamController.ControlTable
{
    public enum CtType
    {
        RX,
        MX,
        MX28,
        AX12
    }

    partial class ControlTable
    {
        public static Dictionary<CtType, HashSet<int>> types = new Dictionary<CtType, HashSet<int>>
        {
            { CtType.RX, new  HashSet<int> {
                10, 24, 28, 64
            } },
            { CtType.MX, new  HashSet<int> {
                360, 310, 320
            } },
            { CtType.MX28, new  HashSet<int> {
                29
            } },
            { CtType.AX12, new  HashSet<int> {
                12
            } }
        };

        public sealed class CtController
        {
            private static readonly Lazy<CtController> lazy =
                new Lazy<CtController>(() => new CtController());

            public static CtController Instance { get { return lazy.Value; } }

            public Dictionary<CtType, ControlTable> controlTables { get; private set; }

            private CtController()
            {
                controlTables = new Dictionary<CtType, ControlTable>();

                string project = "AdamController";
                string folder = "ControlTable.ControlTables";

                var assembly = Assembly.GetExecutingAssembly();
                var tmp = Assembly.GetExecutingAssembly().GetManifestResourceNames();
                var resourceNames = Assembly.GetExecutingAssembly().GetManifestResourceNames().Where(
                    name => name.StartsWith(string.Format("{0}.{1}", project, folder)));

                foreach (string controltable in resourceNames)
                {
                    Stream stream = Assembly.GetExecutingAssembly().GetManifestResourceStream(controltable);
                    StreamReader reader = new StreamReader(stream);

                    string line;
                    line = reader.ReadLine();
                    if (line == null)
                        throw new Exception();

                    CtType type = (CtType)Enum.Parse(typeof(CtType), line);

                    List<string> lines = new List<string>();
                    while ((line = reader.ReadLine()) != null)
                    {
                        lines.Add(line);
                    }

                    controlTables.Add(type, new ControlTable(lines));
                }
            }
        }
    }
}
