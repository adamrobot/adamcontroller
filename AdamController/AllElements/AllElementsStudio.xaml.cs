﻿using Newtonsoft.Json;
using ReactiveUI;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reactive;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Text.RegularExpressions;

namespace AdamController
{
    public partial class AllElementsStudio : UserControl
    {
        public AllElementsVM allElementsVM;

        public AllElementsStudio()
        {
            InitializeComponent();
        }

        public void Init(WheelsVM wheelsVM, Robot.RobotVM robotVM, LedVM leftLedVM, LedVM rightLedVM, FingersVM leftFingersVM, FingersVM rightFingersVM)
        {
            allElementsVM = new AllElementsVM();

            allElementsVM.wheelsVM = wheelsVM;
            WheelsV.ViewModel = wheelsVM;

            allElementsVM.robotVM = robotVM;
            model3DView.ViewModel = new RobotModel3DVM(robotVM);

            ledsView.leftLedView.ViewModel = leftLedVM;
            allElementsVM.leftLedVM = leftLedVM;

            ledsView.rightLedView.ViewModel = rightLedVM;
            allElementsVM.rightLedVM = rightLedVM;

            LeftFingersV.ViewModel = leftFingersVM;
            allElementsVM.leftFingersVM = leftFingersVM;

            RightFingersV.ViewModel = rightFingersVM;
            allElementsVM.rightFingersVM = rightFingersVM;

            ScriptCreatorView.Init(allElementsVM);
        }
    }
}
