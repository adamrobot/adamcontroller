﻿using Newtonsoft.Json;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using ReactiveUI.Legacy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdamController
{
    public class AllElementsVM : ReactiveObject, ITime
    {
        [Reactive] public WheelsVM wheelsVM { get; set; }
        [Reactive] public Robot.RobotVM robotVM { get; set; }
        [Reactive] public LedVM leftLedVM { get; set; }
        [Reactive] public LedVM rightLedVM { get; set; }
        [Reactive] public FingersVM leftFingersVM { get; set; }
        [Reactive] public FingersVM rightFingersVM { get; set; }

        private const string propType = "Type";

        private const string propHead      = "Head";
        private const string propBody      = "Body";
        private const string propLeftHand  = "leftHand";
        private const string propRightHand = "rightHand";
        private const string propLeftLeg   = "LeftLeg";
        private const string propRightLeg  = "RightLeg";

        private const string propLeftForwardWheel  = "LeftForwardWheel";
        private const string propRightForwardWheel = "RightForwardWheel";
        private const string propLeftBackWheel     = "LeftBackWheel";
        private const string propRightBackWheel    = "RightBackWheel";

        private const string propLeftLed  = "LeftLed";
        private const string propRightLed = "RightLed";

        private const string propLeftFingers  = "LeftFingers";
        private const string propRightFingers = "RightFingers";

        public AllElementsVM()
        {
            wheelsVM = new WheelsVM();
            robotVM = new Robot.RobotVM();
            leftLedVM = new LedVM();
            rightLedVM = new LedVM();
            leftFingersVM = new FingersVM();
            rightFingersVM = new FingersVM();
        }

        public AllElementsVM(AllElementsVM allElementsVM)
        {
            wheelsVM = new WheelsVM(allElementsVM.wheelsVM);

            robotVM = new Robot.RobotVM();
            robotVM.bodyVM.body = new Robot.Body(allElementsVM.robotVM.bodyVM.body);
            robotVM.headVM.head = new Robot.Head(allElementsVM.robotVM.headVM.head);
            robotVM.leftHandVM.hand = new Robot.Hand(allElementsVM.robotVM.leftHandVM.hand);
            robotVM.rightHandVM.hand = new Robot.Hand(allElementsVM.robotVM.rightHandVM.hand);
            robotVM.leftLegVM.leg = new Robot.Leg(allElementsVM.robotVM.leftLegVM.leg);
            robotVM.rightLegVM.leg = new Robot.Leg(allElementsVM.robotVM.rightLegVM.leg);

            leftLedVM = new LedVM(allElementsVM.leftLedVM);
            rightLedVM = new LedVM(allElementsVM.rightLedVM);

            leftFingersVM = new FingersVM(allElementsVM.leftFingersVM);
            rightFingersVM = new FingersVM(allElementsVM.rightFingersVM);
        }

        public ITime Clone() => new AllElementsVM(this);
        public ITime Get() => this;

        public void Update(object left, object right, int maxTime, int curTime)
        {
            WheelsVM inLeftWheelsVM = ((AllElementsVM)(left)).wheelsVM;
            WheelsVM inRightWheelsVM = ((AllElementsVM)(right)).wheelsVM;
            wheelsVM.LeftForwardWheelVM.Update(inLeftWheelsVM.LeftForwardWheelVM, inRightWheelsVM.LeftForwardWheelVM, maxTime, curTime);
            wheelsVM.RightForwardWheelVM.Update(inLeftWheelsVM.RightForwardWheelVM, inRightWheelsVM.RightForwardWheelVM, maxTime, curTime);
            wheelsVM.LeftBackWheelVM.Update(inLeftWheelsVM.LeftForwardWheelVM, inRightWheelsVM.LeftBackWheelVM, maxTime, curTime);
            wheelsVM.RightBackWheelVM.Update(inLeftWheelsVM.RightBackWheelVM, inRightWheelsVM.RightBackWheelVM, maxTime, curTime);

            Robot.RobotVM inLeftRobotVM = ((AllElementsVM)(left)).robotVM;
            Robot.RobotVM inRightRobotVM = ((AllElementsVM)(right)).robotVM;
            robotVM.bodyVM.body.Update(inLeftRobotVM.bodyVM.body, inRightRobotVM.bodyVM.body, maxTime, curTime);
            robotVM.headVM.head.Update(inLeftRobotVM.headVM.head, inRightRobotVM.headVM.head, maxTime, curTime);
            robotVM.leftHandVM.hand.Update(inLeftRobotVM.leftHandVM.hand, inRightRobotVM.leftHandVM.hand, maxTime, curTime);
            robotVM.rightHandVM.hand.Update(inLeftRobotVM.rightHandVM.hand, inRightRobotVM.rightHandVM.hand, maxTime, curTime);
            robotVM.leftLegVM.leg.Update(inLeftRobotVM.leftLegVM.leg, inRightRobotVM.leftLegVM.leg, maxTime, curTime);
            robotVM.rightLegVM.leg.Update(inLeftRobotVM.rightLegVM.leg, inRightRobotVM.rightLegVM.leg, maxTime, curTime);

            leftLedVM.Update(((AllElementsVM)(left)).leftLedVM, ((AllElementsVM)(right)).leftLedVM, maxTime, curTime);
            rightLedVM.Update(((AllElementsVM)(left)).rightLedVM, ((AllElementsVM)(right)).rightLedVM, maxTime, curTime);

            leftFingersVM.Update(((AllElementsVM)(left)).leftFingersVM, ((AllElementsVM)(right)).leftFingersVM, maxTime, curTime);
            rightFingersVM.Update(((AllElementsVM)(left)).rightFingersVM, ((AllElementsVM)(right)).rightFingersVM, maxTime, curTime);
        }

        //public void Update(ITime unit, ITime left, ITime right, int maxTime, int curTime)
        //{
        //    unit.Update(left, right, maxTime, curTime);
        //}

        public void ReadJson(JsonTextReader reader)
        {
            while (reader.TokenType != JsonToken.EndObject)
            {
                if (reader.TokenType == JsonToken.PropertyName)
                {
                    var token = reader.Value;
                    reader.Read();
                    switch (token)
                    {
                        case propHead:
                            robotVM.headVM.head.ReadJson(reader);
                            break;
                        case propBody:
                            robotVM.bodyVM.body.ReadJson(reader);
                            break;
                        case propLeftHand:
                            robotVM.leftHandVM.hand.ReadJson(reader);
                            break;
                        case propRightHand:
                            robotVM.rightHandVM.hand.ReadJson(reader);
                            break;
                        case propLeftLeg:
                            robotVM.leftLegVM.leg.ReadJson(reader);
                            break;
                        case propRightLeg:
                            robotVM.rightLegVM.leg.ReadJson(reader);
                            break;

                        case propLeftForwardWheel:
                            wheelsVM.LeftForwardWheelVM.ReadJson(reader);
                            break;
                        case propRightForwardWheel:
                            wheelsVM.RightForwardWheelVM.ReadJson(reader);
                            break;
                        case propLeftBackWheel:
                            wheelsVM.LeftBackWheelVM.ReadJson(reader);
                            break;
                        case propRightBackWheel:
                            wheelsVM.RightBackWheelVM.ReadJson(reader);
                            break;

                        case propLeftLed:
                            leftLedVM.ReadJson(reader);
                            break;
                        case propRightLed:
                            rightLedVM.ReadJson(reader);
                            break;

                        case propLeftFingers:
                            leftFingersVM.ReadJson(reader);
                            break;
                        case propRightFingers:
                            rightFingersVM.ReadJson(reader);
                            break;
                    }
                }
                reader.Read();
            }
        }

        public void WriteJson(JsonWriter writer)
        {
            writer.WriteStartObject();
            writer.WritePropertyName(propType);
            writer.WriteValue(GetType().FullName);
            writer.WritePropertyName(propHead);
            robotVM.headVM.head.WriteJson(writer);
            writer.WritePropertyName(propBody);
            robotVM.bodyVM.body.WriteJson(writer);
            writer.WritePropertyName(propLeftHand);
            robotVM.leftHandVM.hand.WriteJson(writer);
            writer.WritePropertyName(propRightHand);
            robotVM.rightHandVM.hand.WriteJson(writer);
            writer.WritePropertyName(propLeftLeg);
            robotVM.leftLegVM.leg.WriteJson(writer);
            writer.WritePropertyName(propRightLeg);
            robotVM.rightLegVM.leg.WriteJson(writer);
            writer.WritePropertyName(propLeftForwardWheel);
            wheelsVM.LeftForwardWheelVM.WriteJson(writer);
            writer.WritePropertyName(propRightForwardWheel);
            wheelsVM.RightForwardWheelVM.WriteJson(writer);
            writer.WritePropertyName(propLeftBackWheel);
            wheelsVM.LeftBackWheelVM.WriteJson(writer);
            writer.WritePropertyName(propRightBackWheel);
            wheelsVM.RightBackWheelVM.WriteJson(writer);
            writer.WritePropertyName(propLeftLed);
            leftLedVM.WriteJson(writer);
            writer.WritePropertyName(propRightLed);
            rightLedVM.WriteJson(writer);
            writer.WritePropertyName(propLeftFingers);
            leftFingersVM.WriteJson(writer);
            writer.WritePropertyName(propRightFingers);
            rightFingersVM.WriteJson(writer);
            writer.WriteEndObject();
        }
    }
}
