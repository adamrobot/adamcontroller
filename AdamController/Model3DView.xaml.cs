﻿using HelixToolkit.Wpf.SharpDX;
using ReactiveUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Disposables;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace AdamController
{
    using Point3D = System.Windows.Media.Media3D.Point3D;
    using Vector3D = System.Windows.Media.Media3D.Vector3D;

    public partial class Model3DView : UserControl, IViewFor<RobotModel3DVM>
    {
        public static readonly DependencyProperty ViewModelProperty = DependencyProperty
            .Register(nameof(ViewModel), typeof(RobotModel3DVM), typeof(Model3DView));

        public RobotModel3DVM ViewModel
        {
            get => (RobotModel3DVM)GetValue(ViewModelProperty);
            set => SetValue(ViewModelProperty, value);
        }

        object IViewFor.ViewModel
        {
            get => ViewModel;
            set => ViewModel = (RobotModel3DVM)value;
        }

        public Model3DView()
        {
            InitializeComponent();
            InitViewport();
            DataContext = ViewModel;
            
            //InitRobotModel();
            //groupModel.ItemsSource = ViewModel.groupModel;
        }

        //public void Update(Robot.RobotVM viewModel)
        //{
        //    ViewModel = viewModel;
        //}

        private void InitViewport()
        {
            viewport.DataContext = this;
            viewport.EffectsManager = new DefaultEffectsManager();
            var camera = new OrthographicCamera();
            //robotModel.groupModel.
            //camera.Position = new Point3D(312.5424, 385.4094, -262.7674);
            camera.LookDirection = new Vector3D(0, 0, -1);
            camera.UpDirection = new Vector3D(0, 1, 0);
            camera.FarPlaneDistance = 1000;
            camera.NearPlaneDistance = -1000;            
            camera.Width = 3000;
            camera.LookAt(BaseAdamModel3D.bases[ModelName.BodyPelvis].rotationPoint, 0);
            viewport.Camera = camera;
        }
        
        //private void InitRobotModel()
        //{          
        //    this.WhenActivated(disposable =>
        //    {
        //        this.WhenAnyValue(x => x.ViewModel)
        //            .BindTo(this, view => view.robotModel.viewModel)
        //            .DisposeWith(disposable);
        //        this.WhenAnyValue(x => x.robotModel.viewModel)
        //            .BindTo(this, view => view.ViewModel)
        //            .DisposeWith(disposable);
        //    });
        //
        //    groupModel.ItemsSource = robotModel.groupModel;
        //}
    }
}
