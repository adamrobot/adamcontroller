﻿using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using ReactiveUI.Legacy;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Reactive.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdamController
{
    public class ChatbotVM : ReactiveObject
    {
        [Reactive] public DialogVM selectedDialog { get; set; }
        [Reactive] public ReactiveList<DialogVM> dialogVMs { get; set; } = new ReactiveList<DialogVM>();
        [Reactive] public ReactiveList<ScriptVM> scriptVMs { get; set; } = new ReactiveList<ScriptVM>();

        public ChatbotVM()
        {
            foreach (var i in Enum.GetValues(typeof(ScriptVM.Extension)))
                scriptVMs.Add(new ScriptVM((ScriptVM.Extension)i));
        }
    }
}
