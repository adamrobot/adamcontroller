﻿using ReactiveUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;
using System.Reactive.Disposables;
using System.Collections.ObjectModel;
using System.Reactive;
using System.Threading;
using System.Reactive.Linq;
using ReactiveUI.Fody.Helpers;
using System.Diagnostics;

namespace AdamController
{
    public partial class ChatbotV : UserControl, IViewFor<ChatbotVM>
    {
        public static readonly DependencyProperty ViewModelProperty = DependencyProperty
           .Register(nameof(ViewModel), typeof(ChatbotVM), typeof(ChatbotV));

        public ChatbotVM ViewModel
        {
            get => (ChatbotVM)GetValue(ViewModelProperty);
            set => SetValue(ViewModelProperty, value);
        }

        object IViewFor.ViewModel
        {
            get => ViewModel;
            set => ViewModel = (ChatbotVM)value;
        }

        public ReactiveCommand<Unit, Unit> addCommand { get; set; }
        public ReactiveCommand<Unit, Unit> removeCommand { get; set; }

        public ObservableCollection<DialogV> dialogs { get; set; } = new ObservableCollection<DialogV>();

        public ChatbotV()
        {
            InitializeComponent();
            DataContext = this;

            Init();
            InitCommands();
        }

        private void Init()
        {
            ViewModel = new ChatbotVM();
            ViewModel.dialogVMs.ItemsAdded.Subscribe(dvm =>
            {
                dialogs.Add(new DialogV(dvm));
                Gst.Application.Init();
                //scriptViews.Last().saveCommand.Execute().Subscribe();
            });
            ViewModel.dialogVMs.ItemsRemoved.Subscribe(dvm =>
            {
                foreach (var dv in dialogs)
                    if (dv.ViewModel == dvm)
                    {
                        dialogs.Remove(dv);
                        return;
                    }
            });
        
            //LoadScripts();
        }
        
        private void InitCommands()
        {
            addCommand = ReactiveCommand.Create(
                () =>
                {
                    //foreach (DialogVM dialogVM in ViewModel.dialogVMs)
                    //    if (nameTB.Text == script.name)
                    //        return;

                    DialogVM dialogVM = new DialogVM();
                    //scriptVM.name = nameTB.Text;
        
                    ViewModel.dialogVMs.Add(dialogVM);
                }
                );
        
            removeCommand = ReactiveCommand.Create(
                () =>
                {
                    if (ViewModel.dialogVMs.Count() <= 1)
                        return;
                    var dv = ((DialogV)listView.SelectedItem).ViewModel;
                    ViewModel.dialogVMs.Remove(dv);
                    //File.Delete(ScriptVM.folderPath + vm.name + ScriptVM.extensions[ViewModel.extension]);
                }
                );
        }

        private void ListViewSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            DialogV dv = (DialogV)((ListView)e.Source).SelectedItem;
            if (dv == null)
            {
                ((ListView)e.Source).SelectedIndex = 0;
                return;
            }

            ViewModel.selectedDialog = dv.ViewModel;
        }
    }
}
