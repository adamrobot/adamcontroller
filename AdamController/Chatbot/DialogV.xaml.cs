﻿using ReactiveUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;
using System.Reactive.Disposables;
using System.Collections.ObjectModel;
using System.Reactive;
using System.Threading;
using System.Reactive.Linq;
using ReactiveUI.Fody.Helpers;
using System.Diagnostics;

namespace AdamController
{
    public partial class DialogV : UserControl, IViewFor<DialogVM>
    {
        public static readonly DependencyProperty ViewModelProperty = DependencyProperty
           .Register(nameof(ViewModel), typeof(DialogVM), typeof(DialogV));

        public DialogVM ViewModel
        {
            get => (DialogVM)GetValue(ViewModelProperty);
            set => SetValue(ViewModelProperty, value);
        }

        object IViewFor.ViewModel
        {
            get => ViewModel;
            set => ViewModel = (DialogVM)value;
        }

        //public DialogV()
        //{
        //    InitializeComponent();
        //    DataContext = this;
        //}

        public DialogV(DialogVM dialogVM)
        {
            InitializeComponent();
            DataContext = this;

            ViewModel = dialogVM;
        }
    }
}
