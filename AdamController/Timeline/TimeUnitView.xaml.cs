﻿using ReactiveUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace AdamController
{
    public partial class TimeUnitView : UserControl, IViewFor<TimeUnitVM>
    {
        public static readonly DependencyProperty ViewModelProperty = DependencyProperty
           .Register(nameof(ViewModel), typeof(TimeUnitVM), typeof(TimeUnitView));
        
        public TimeUnitView(TimeUnitVM tuvm)
        {
            InitializeComponent();
            DataContext = this;

            ViewModel = tuvm;
        }

        public TimeUnitVM ViewModel
        {
            get => (TimeUnitVM)GetValue(ViewModelProperty);
            set => SetValue(ViewModelProperty, value);            
        }

        object IViewFor.ViewModel
        {
            get => ViewModel;
            set => ViewModel = (TimeUnitVM)value;
        }

        private void TUVMouseEnter(object sender, MouseEventArgs e)
        {
            ViewModel.isHighlighted = true;
        }

        private void TUVMouseLeave(object sender, MouseEventArgs e)
        {
            ViewModel.isHighlighted = false;
        }
    }
}
