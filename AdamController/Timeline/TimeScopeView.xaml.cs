﻿using ReactiveUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;
using System.Reactive.Disposables;
using System.Collections.ObjectModel;
using System.Reactive;
using System.Reactive.Linq;
using ReactiveUI.Legacy;

namespace AdamController
{
    public partial class TimeScopeView : UserControl, IViewFor<TimelineVM>
    {
        public static readonly DependencyProperty ViewModelProperty = DependencyProperty
           .Register(nameof(ViewModel), typeof(TimelineVM), typeof(TimeScopeView));

        public ReactiveList<Rectangle> littleMarks { get; set; } = new ReactiveList<Rectangle>();
        public ReactiveList<Rectangle> bigMarks    { get; set; } = new ReactiveList<Rectangle>();
        public ReactiveList<Label>     markValues  { get; set; } = new ReactiveList<Label>();

        private double littleStep     = 0.2;
        private double bigStep        = 1;
        public  double additionalTime = 10;

        public TimeScopeView()
        {
            InitializeComponent();
            DataContext = this;
            
            ViewModel = new TimelineVM();

            this.WhenActivated(disposable =>
            {
                this.WhenAnyValue(vm => vm.ViewModel.endFrame)
                    .Subscribe(frame => { UpdateMarks(); })
                    .DisposeWith(disposable);
            });            
        }

        public TimelineVM ViewModel
        {
            get => (TimelineVM)GetValue(ViewModelProperty);
            set => SetValue(ViewModelProperty, value);
        }

        object IViewFor.ViewModel
        {
            get => ViewModel;
            set => ViewModel = (TimelineVM)value;
        }

        private void AddLittleMark(double pos)
        {
            Rectangle rectagle = new Rectangle();
            rectagle.Width = 2;
            Canvas.SetLeft(rectagle, (ViewModel.framesInSec * pos + 1) * ViewModel.frameSize - rectagle.Width / 2);
            littleMarks.Add(rectagle);
        }

        private void AddBigMark(double pos)
        {
            Rectangle rectagle = new Rectangle();
            rectagle.Width = 2;
            Canvas.SetLeft(rectagle, (ViewModel.framesInSec * pos + 1) * ViewModel.frameSize - rectagle.Width / 2);
            bigMarks.Add(rectagle);
        }

        private void AddMarkValue(double pos)
        {
            Label label = new Label();
            label.Width = 100;
            Canvas.SetLeft(label, (ViewModel.framesInSec * pos + 1) * ViewModel.frameSize - label.Width / 2);
            label.Content = pos;
            markValues.Add(label);
        }

        private void UpdateMarks()
        {
            var lastFrame = Math.Max(ViewModel.endFrame, (int)(ActualWidth / ViewModel.frameSize));
            var seconds = lastFrame / (double)ViewModel.framesInSec + additionalTime;
            var bigMarksCount = (int)Math.Floor(seconds / bigStep);

            UpdateBigMarks(bigMarksCount + 1);
            UpdateLittleMarks((int)(bigMarksCount * bigStep / littleStep));           
        }

        private void UpdateLittleMarks(int count)
        {
            if (count < littleMarks.Count())
            {
                littleMarks.RemoveRange(count, littleMarks.Count() - count);
                return;
            }

            for (int i = littleMarks.Count(); i < count; i++)
                AddLittleMark(i * littleStep);
        }

        private void UpdateBigMarks(int count)
        {
            if (count < bigMarks.Count())
            {
                bigMarks.RemoveRange(count, bigMarks.Count() - count);
                markValues.RemoveRange(count, markValues.Count() - count);
                return;
            }

            for (int i = bigMarks.Count(); i < count; i++)
            {
                AddBigMark(i * bigStep);
                AddMarkValue(i * bigStep);
            }
        }
    }
}