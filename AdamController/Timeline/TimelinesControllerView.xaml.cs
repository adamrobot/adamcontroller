﻿using ReactiveUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;
using System.Reactive.Disposables;
using System.Collections.ObjectModel;
using System.Reactive;
using System.Threading;
using System.Reactive.Linq;
using ReactiveUI.Fody.Helpers;
using System.Diagnostics;

namespace AdamController
{
    public partial class TimelinesControllerView : UserControl, IViewFor<ScriptVM>
    {
        public static readonly DependencyProperty ViewModelProperty = DependencyProperty
           .Register(nameof(ViewModel), typeof(ScriptVM), typeof(TimelinesControllerView));

        public ScriptVM ViewModel
        {
            get => (ScriptVM)GetValue(ViewModelProperty);
            set => SetValue(ViewModelProperty, value);
        }

        object IViewFor.ViewModel
        {
            get => ViewModel;
            set => ViewModel = (ScriptVM)value;
        }

        public ReactiveCommand<Unit, Unit> stopCommand { get; set; }
        public ReactiveCommand<Unit, Unit> runCommand { get; set; }
        private Func<CancellationToken, Task> run;

        public ObservableCollection<TimelineView> timelines { get; set; } = new ObservableCollection<TimelineView>();

        public TimelinesControllerView()
        {
            InitializeComponent();
            DataContext = this;
            InitCommands();
        }

        private void InitCommands()
        {
            run = async (ct) =>
            {
                Stopwatch stopWatch = Stopwatch.StartNew();

                int endFrame = 1;
                int startFrame = 1;
                foreach (var timeline in timelines)
                {
                    endFrame   = Math.Max(endFrame,   timeline.ViewModel.endFrame);
                    startFrame = Math.Min(startFrame, timeline.ViewModel.startFrame);
                }
                
                int step = 1000 / timelines.First().ViewModel.framesInSec;
                int velocity = 20;

                //int
                //if ()

                TimeSpan ts;
                for (int i = 1; i <= endFrame; i += timelines.First().ViewModel.framesInSec / velocity)
                {
                    if (ct.IsCancellationRequested)
                        return;

                    if (IsOutdated(stopWatch, step * i))
                        continue;

                    UpdateTimelines(i);
                    
                    if (IsOutdated(stopWatch, step * i))
                        continue;

                    //await Task.Delay(step);

                    stopWatch.Stop();
                    ts = stopWatch.Elapsed;
                    stopWatch.Start();
                    //await Task.Delay(Math.Min(step, step * i - (int)ts.TotalMilliseconds));
                    await Task.Delay(Math.Min(1000 / velocity, step * i - (int)ts.TotalMilliseconds));
                }
                UpdateTimelines(endFrame);
                stopWatch.Stop();
                ts = stopWatch.Elapsed;
                Console.WriteLine(ts.TotalMilliseconds);
            };

            runCommand = ReactiveCommand.CreateFromObservable(
                () => Observable.StartAsync(ct => run(ct))
                    .TakeUntil(stopCommand));
            

            stopCommand = ReactiveCommand.Create(
                () => { },
                runCommand.IsExecuting);
        }

        private void UpdateTimelines(int time)
        {
            foreach (var timeline in timelines)
            {
                timeline.SetStep(time);
                timeline.UpdateSV();
            }
        }

        private bool IsOutdated(Stopwatch stopwatch, int time)
        {
            stopwatch.Stop();
            var ts = stopwatch.Elapsed;
            stopwatch.Start();
            return ts.TotalMilliseconds > time;
        }

        public void InitViewModel(ScriptVM.Extension extension)
        {
            ViewModel = new ScriptVM(extension);

            this.WhenAnyValue(v => v.ViewModel)
                .Subscribe(vm => {
                    for (int i = 0; i < Math.Min(vm.listTUVMs.Count(), timelines.Count); i++)
                    {
                        timelines[i].ViewModel.endFrame = 1;
                        timelines[i].ViewModel.tuvms = vm.listTUVMs[i];
                    }
                });
        }

        //public void Init(ScriptVM.Extension extension)
        //{
        //    switch (extension)
        //    {
        //        case ScriptVM.Extension.MOVEMENT:
        //            {
        //                Init(new Robot.RobotVM(), extension);
        //                break;
        //            }
        //        case ScriptVM.Extension.AUDIO:
        //            {
        //                Init(new AudioVM(), extension);
        //                break;
        //            }
        //        case ScriptVM.Extension.LED:
        //            {
        //                Init(new LedVM(), new LedVM(), extension);
        //                break;
        //            }
        //    }
        //}

        public void Init(Robot.RobotVM robotVM, ScriptVM.Extension extension)
        {
            InitViewModel(extension);

            CreateTimelineView("Голова",      robotVM.headVM.head,      0);
            CreateTimelineView("Левая рука",  robotVM.leftHandVM.hand,  1);
            CreateTimelineView("Правая рука", robotVM.rightHandVM.hand, 2);
            CreateTimelineView("Тело",        robotVM.bodyVM.body,      3);
            var legs = new Robot.Legs(robotVM.leftLegVM.leg, robotVM.rightLegVM.leg);
            CreateTimelineView("Ноги", legs, 4);
        }

        public void Init(AudioVM audioVM, ScriptVM.Extension extension)
        {
            InitViewModel(extension);
        
            CreateTimelineView("Audio", audioVM, 0);
        }

        public void Init(FingersVM leftFingersVM, FingersVM rightFingersVM, ScriptVM.Extension extension)
        {
            InitViewModel(extension);

            CreateTimelineView("левые пальцы", leftFingersVM, 0);
            CreateTimelineView("правые пальцы", rightFingersVM, 1);
        }

        public void Init(WheelsVM wheelsVM, ScriptVM.Extension extension)
        {
            InitViewModel(extension);

            CreateTimelineView("Левое переднее",  wheelsVM.LeftForwardWheelVM, 0);
            CreateTimelineView("Правое переднее", wheelsVM.RightForwardWheelVM, 1);
            CreateTimelineView("Левое заднее",    wheelsVM.LeftBackWheelVM, 2);
            CreateTimelineView("Правое заднее",   wheelsVM.RightBackWheelVM, 3);
        }
        public void Init(AllElementsVM allElementsVM, ScriptVM.Extension extension)
        {
            InitViewModel(extension);

            CreateTimelineView("", allElementsVM, 0);
        }

        public void Init(LedVM leftLedVM, LedVM rightLedVM, ScriptVM.Extension extension)
        {
            InitViewModel(extension);
        
            CreateTimelineView("Левый глаз", leftLedVM, 0);
            CreateTimelineView("Правый глаз", rightLedVM, 1);
        }

        private void CreateTimelineView(string name, ITime timeM, int row)
        {
            var tlv = new TimelineView(name);
            tlv.Init(timeM);
            Grid.SetRow(tlv, row);
            timelines.Add(tlv);
        }
    }
}
