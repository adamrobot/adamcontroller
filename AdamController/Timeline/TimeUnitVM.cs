﻿using Newtonsoft.Json;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using System;
using System.Collections.Generic;
using System.Reactive.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace AdamController
{
    public class TimeUnitVM : ReactiveObject
    {
        [Reactive] public bool isHighlighted { get; set; } = false;
        [Reactive] public bool isSelected { get; set; } = false;
        [Reactive] public int time { get; set; }
        [Reactive] public ITime timeM { get; set; }
        
        private const string propTime   = "Time";
        private const string propUnitVM = "UnitVM";

        public TimeUnitVM(int time, ITime timeM)
        {
            this.time = time;
            this.timeM = timeM;
        }

        public void ReadJson(JsonTextReader reader)
        {
            while (reader.TokenType != JsonToken.EndObject)
            {
                if (reader.TokenType == JsonToken.PropertyName)
                {
                    switch (reader.Value)
                    {
                        case propTime:
                            reader.Read(); // read time value
                            time = (int)(long)reader.Value;
                            break;
                        case propUnitVM:
                            reader.Read(); // read StartObject token
                            reader.Read(); // read Type token
                            reader.Read(); // read Type value
                            string typeName = (string)reader.Value;
                            Type type = Type.GetType(typeName);
                            ITime uvm = (ITime)Activator.CreateInstance(type);
                            uvm.ReadJson(reader);
                            timeM = uvm;
                            break;
                    }
                }
                reader.Read();
            }
        }

        public void WriteJson(JsonWriter writer)
        {
            writer.WriteStartObject();
            writer.WritePropertyName(propTime);
            writer.WriteValue(time);
            writer.WritePropertyName(propUnitVM);
            timeM.WriteJson(writer);
            writer.WriteEndObject();
        }
    }
}
