﻿using Newtonsoft.Json;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using System;
using System.Collections.Generic;
using System.Reactive.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdamController
{
    public interface IJson
    {
        void WriteJson(JsonWriter writer);
        void ReadJson(JsonTextReader reader);
    }
}
