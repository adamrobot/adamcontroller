﻿using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using ReactiveUI.Legacy;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Reactive.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdamController
{
    public class TimelineVM : ReactiveObject
    {
        //[Reactive] public bool isHighlighted { get; set; } = false;

        //public double startTime;
        //public double endTime;

        public int framesInSec = 100;
        [Reactive] public double frameSize { get; set; } = 1;
        [Reactive] public int currentFrame { get; set; } = 1;
        public int startFrame = 1;
        [Reactive] public int endFrame { get; set; }
        [Reactive] public ReactiveList<TimeUnitVM> tuvms { get; set; } = new ReactiveList<TimeUnitVM>();
        [Reactive] public ITime timeM { get; set; }

        public TimelineVM()
        {
        }
    }
}
