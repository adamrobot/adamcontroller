﻿using ReactiveUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;
using System.Reactive.Disposables;
using System.Collections.ObjectModel;
using System.Reactive;
using System.Reactive.Linq;
using ReactiveUI.Legacy;

namespace AdamController
{
    public partial class TimelineView : UserControl, IViewFor<TimelineVM>
    {
        public static readonly DependencyProperty ViewModelProperty = DependencyProperty
           .Register(nameof(ViewModel), typeof(TimelineVM), typeof(TimelineView));

        private int timeOffset = 0;
        private TimeUnitVM selectedTUVM;
        private TimeUnitVM defaultTUVM = new TimeUnitVM(1, null);
        public string name { get; set; }
        public ReactiveCommand<Unit, Unit> addCommand { get; set; }
        public ReactiveCommand<Unit, Unit> removeCommand { get; set; }
        public ObservableCollection<TimeUnitView> timeUnits { get; set; } = new ObservableCollection<TimeUnitView>();

        public TimelineView(string name)
        {
            InitializeComponent();
            DataContext = this;

            this.name = name;

            ViewModel = new TimelineVM();
            selectedTUVM = defaultTUVM;

            //this.WhenActivated(disposable =>
            //{
            //    this.Bind(ViewModel, x => x.currentFrame, x => x.textBlock.Text)
            //        .DisposeWith(disposable);
            //});
            this.WhenActivated(disposable =>
            {
                this.WhenAnyValue(v => v.ViewModel)
                    .Subscribe(vm => { timeScopeView.ViewModel = ViewModel; })
                    .DisposeWith(disposable);

                this.WhenAnyValue(v => v.ViewModel.tuvms)
                   .Subscribe(list =>
                   {
                       foreach (var timeUnit in timeUnits)
                           UnbindTUV(timeUnit);
                       timeUnits.Clear();
                       list.ItemsAdded.Subscribe(tuvm => { CreateTUV(tuvm); });
                       list.ItemsRemoved.Subscribe(tuvm => { RemoveTUV(tuvm); });
                       list.ResetChangeThreshold = -1;
                       foreach (var tuvm in list)
                           CreateTUV(tuvm);
                       SelectTUVM(defaultTUVM);
                       ViewModel.currentFrame = 1;
                   })
                   .DisposeWith(disposable);
                //this.Bind(ViewModel, x => x, x => x.timeScopeView.ViewModel)
                //    .DisposeWith(disposable);

                this.WhenAnyValue(v => v.ViewModel.currentFrame)
                    .Subscribe(frame => { Canvas.SetLeft(currentFrame, frame * ViewModel.frameSize - currentFrame.ActualWidth / 2); })
                    .DisposeWith(disposable);

                this.WhenAnyValue(v => v.ViewModel.endFrame)
                    .Subscribe(frame => { UpdateSize(); })
                    .DisposeWith(disposable);
            });

            //this.WhenActivated(disposable =>
            //{
            //    this.WhenAnyValue(vm => vm.ViewModel.currentFrame)
            //        .Subscribe(t => { Canvas.SetLeft(currentFrame, (t - 1) * ViewModel.frameSize - currentFrame.ActualWidth / 2); })
            //        .DisposeWith(disposable);
            //});

            //_inCoord = this.WhenAnyValue(vm => vm.coord, vm => vm.width, vm => vm.height)
            //    .Select(t => new Coord2(t.Item1.x, t.Item1.y + t.Item3 / 2))
            //    .ToProperty(this, vm => vm.inCoord);
            //
            InitAddCommand();
            InitRemoveCommand();
        }

        private void RemoveTUV(TimeUnitVM tuvm)
        {
            foreach (var timeUnit in timeUnits)
                if (timeUnit.ViewModel == tuvm)
                {
                    timeUnits.Remove(timeUnit);
                    if (ViewModel.endFrame == tuvm.time)
                        UpdateEndFrame();
                    UnbindTUV(timeUnit);
                    SelectTUVM(defaultTUVM);

                    UpdateCurrentFrame();

                    return;
                }
        }

        private void InitRemoveCommand()
        {
            var canExecute = this.WhenAnyValue(x => x.ViewModel.currentFrame)
                .Select(time => { return selectedTUVM != defaultTUVM; });

            removeCommand = ReactiveCommand.Create(
                () =>
                {
                    ViewModel.tuvms.Remove(selectedTUVM);
                },
                canExecute
                );
        }

        private void InitAddCommand()
        {
            var canExecute = this.WhenAnyValue(x => x.ViewModel.currentFrame)
                .Select(time => !IsCreated(time));

            addCommand = ReactiveCommand.Create(
                () =>
                {
                    ViewModel.tuvms.Add(new TimeUnitVM(ViewModel.currentFrame, ViewModel.timeM.Clone()));
                },
                canExecute
                );
        }

        private void UpdateEndFrame()
        {
            ViewModel.endFrame = 1;
            foreach (var tuvm in ViewModel.tuvms)
                ViewModel.endFrame = Math.Max(ViewModel.endFrame, tuvm.time);
        }

        private bool IsCreated(int time)
        {
            foreach (var tuvm in ViewModel.tuvms)
                if (tuvm.time == time) return true;
            return false;
        }

        public void UpdateSV()
        {
            scrollViewer.ScrollToHorizontalOffset(ViewModel.currentFrame * ViewModel.frameSize - currentFrame.ActualWidth / 2 - scrollViewer.ActualWidth / 2);
        }

        public void SetStep(int time)
        {
            var sortedTimeUnits = new List<TimeUnitView>(timeUnits.OrderBy(i => i.ViewModel.time));
            ViewModel.currentFrame = time;
            if (sortedTimeUnits.Count() > 0 && sortedTimeUnits.Last().ViewModel.time <= time)
            {
                ViewModel.timeM.Get().Update(sortedTimeUnits.Last().ViewModel.timeM, sortedTimeUnits.Last().ViewModel.timeM, ViewModel.startFrame, ViewModel.startFrame);
                return;
            }

            sortedTimeUnits.Insert(0, new TimeUnitView(new TimeUnitVM(ViewModel.startFrame, ViewModel.timeM.Clone())));
            
            for (int i = 0; i < sortedTimeUnits.Count() - 1; i++)
            {
                TimeUnitVM leftThvm = sortedTimeUnits[i].ViewModel;
                TimeUnitVM rightThvm = sortedTimeUnits[i + 1].ViewModel;
                if (time >= sortedTimeUnits[i].ViewModel.time && time < sortedTimeUnits[i + 1].ViewModel.time)
                {
                    ViewModel.timeM.Get().Update(leftThvm.timeM, rightThvm.timeM, rightThvm.time - leftThvm.time, time - leftThvm.time);
                    break;
                }
            }
        }

        public void Init(ITime unitVM)
        {
            ViewModel.timeM = unitVM;
        }

        public TimelineVM ViewModel
        {
            get => (TimelineVM)GetValue(ViewModelProperty);
            set => SetValue(ViewModelProperty, value);
        }

        object IViewFor.ViewModel
        {
            get => ViewModel;
            set => ViewModel = (TimelineVM)value;
        }

        private void BindTUV(TimeUnitView tuv)
        {
            tuv.MouseDown += TUVMouseDown;
            tuv.MouseMove += TUVMouseMove;
            tuv.MouseUp   += TUVMouseUp;

            tuv.WhenActivated(disposable =>
            {
                tuv.WhenAnyValue(v => v.ViewModel.time)
                    .Subscribe(time => {
                        Canvas.SetLeft(tuv, time * ViewModel.frameSize - tuv.ActualWidth / 2);
                        UpdateEndFrame();
                    })
                    .DisposeWith(disposable);
            });
        }

        private void UnbindTUV(TimeUnitView tuv)
        {
            tuv.MouseDown -= TUVMouseDown;
            tuv.MouseMove -= TUVMouseMove;
            tuv.MouseUp   -= TUVMouseUp;
        }

        private void CanvasMouseMove(object sender, MouseEventArgs e)
        {
            if (Mouse.Captured == null) return;
            if (Mouse.LeftButton != MouseButtonState.Pressed) return;
            if (selectedTUVM != defaultTUVM) return;

            SetStep(CalcTime(e.GetPosition(canvas).X));
        }

        private void CreateTUV(TimeUnitVM tuvm)
        {
            //TimeUnitView tuv = new TimeUnitView(new TimeUnitVM(time, ViewModel.unitVM.Clone()));
            TimeUnitView tuv = new TimeUnitView(tuvm);
            tuv.Height = canvas.ActualHeight;
            tuv.Width = 6 * ViewModel.frameSize; // need to change            
            BindTUV(tuv);
            //tuv.ViewModel.time = time;
            SelectTUVM(tuvm);
            ViewModel.currentFrame = tuv.ViewModel.time;
            timeUnits.Add(tuv);

            UpdateCurrentFrame();
        }

        //public void AddTUV(TimeUnitVM tuvm)
        //{
        //    TimeUnitView tuv = new TimeUnitView(tuvm);
        //    tuv.Height = canvas.ActualHeight;
        //    tuv.Width = 6 * ViewModel.frameSize; // need to change            
        //    BindTUV(tuv);
        //    ViewModel.timeUnits.Add(tuv);
        //}

        private void CanvasMouseDown(object sender, MouseButtonEventArgs e)
        {
            if (Mouse.Captured != null) return;
            if (Mouse.LeftButton != MouseButtonState.Pressed) return;
            //if (e.ClickCount == 2)
            //{
            //    int time = Math.Max(CalcTime(e.GetPosition(canvas).X), 1);
            //    if (!IsCreated(time)) AddTUV(time); 
            //}
            SelectTUVM(defaultTUVM);
            SetStep(CalcTime(e.GetPosition(canvas).X));

            Mouse.Capture(canvas);
        }

        private void UpdateCurrentFrame()
        {
            ViewModel.currentFrame--; // need to change 
            ViewModel.currentFrame++; //
        }

        private void CanvasMouseUp(object sender, MouseEventArgs e)
        {
            foreach (var tuvm in ViewModel.tuvms)
                if (ViewModel.currentFrame == tuvm.time)
                {
                    SelectTUVM(tuvm);
                    UpdateCurrentFrame();
                }
            Mouse.Capture(null);
        }

        private void SelectTUVM(TimeUnitVM tuvm)
        {
            selectedTUVM.isSelected = false;
            selectedTUVM = tuvm;
            selectedTUVM.isSelected = true;
        }

        private void TUVMouseMove(object sender, MouseEventArgs e)
        {
            if (Mouse.Captured == null) return;
            if (Mouse.LeftButton != MouseButtonState.Pressed) return;

            var tuv = e.Source as TimeUnitView;
            
            int time = CalcTime(e.GetPosition(canvas).X);

            tuv.ViewModel.time = time + timeOffset;

            SetStep(tuv.ViewModel.time);
        }

        private void TUVMouseDown(object sender, MouseEventArgs e)
        {
            if (Mouse.Captured != null) return;
            if (Mouse.LeftButton != MouseButtonState.Pressed) return;

            var tuv = e.Source as TimeUnitView;

            int time = CalcTime(e.GetPosition(canvas).X);

            timeOffset = tuv.ViewModel.time - time;

            SelectTUVM(tuv.ViewModel);

            SetStep(tuv.ViewModel.time);

            Mouse.Capture(tuv);
        }

        private void TUVMouseUp(object sender, MouseEventArgs e)
        {
            Mouse.Capture(null);
        }

        private int CalcTime(double position)
        {
            return Math.Max((int)(position / ViewModel.frameSize), 1);
        }

        private void UpdateSize()
        {
            canvas.Width = Math.Max(ViewModel.endFrame * ViewModel.frameSize + ViewModel.frameSize * ViewModel.framesInSec * timeScopeView.additionalTime, scrollViewer.ActualWidth);
        }

        private void ScrollViewerSizeChanged(object sender, SizeChangedEventArgs e)
        {
            UpdateSize();
        }
    }
}
