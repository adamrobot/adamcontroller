﻿using Newtonsoft.Json;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using System;
using System.Collections.Generic;
using System.Reactive.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdamController
{
    public interface ITime : IJson
    {
        void Update(object left, object right, int maxTime, int curTime);
        ITime Clone();
        ITime Get();
    }
}
