﻿using Newtonsoft.Json;
using ReactiveUI;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reactive;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Text.RegularExpressions;

namespace AdamController
{
    public partial class FingersStudio : UserControl
    {
        public FingersStudio()
        {
            InitializeComponent();

            LeftFingersV.ViewModel = new FingersVM(FingersVM.FingersID.Left);
            RightFingersV.ViewModel = new FingersVM(FingersVM.FingersID.Right);
            FingersScriptCreatorView.Init(LeftFingersV.ViewModel, RightFingersV.ViewModel);
        }
    }
}
