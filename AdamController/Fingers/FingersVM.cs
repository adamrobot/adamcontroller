﻿using Newtonsoft.Json;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using System;
using System.Collections.Generic;
using System.Reactive.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdamController
{
    public class FingersVM : ReactiveObject, ITime
    {
        public enum FingersID
        {
            Left  = 0,
            Right = 1
        }

        [Reactive] public double angle { get; set; } = 0;
        [Reactive] public FingersID id { get; private set; }

        private const string propType      = "Type";
        private const string propAngle     = "Angle";
        private const string propFingersID = "FingersID";

        public ITime Clone() => new FingersVM(this);
        public ITime Get() => this;

        public FingersVM() { }

        public FingersVM(FingersID id)
        {
            this.id = id;
        }

        public FingersVM(FingersVM vm)
        {
            angle = vm.angle;
            id = vm.id;
        }

        public Message.FingersScheme GetScheme()
        {
            Message.FingersScheme outMsg = new Message.FingersScheme();
            outMsg.id = id;
            outMsg.angle = angle;

            return outMsg;
        }

        public void Update(object left, object right, int maxTime, int curTime)
        {
            angle = ((FingersVM)left).angle + (((FingersVM)right).angle - ((FingersVM)left).angle) / maxTime * curTime;
        }

        public void ReadJson(JsonTextReader reader)
        {
            while (reader.TokenType != JsonToken.EndObject)
            {
                if (reader.TokenType == JsonToken.PropertyName)
                {
                    var token = reader.Value;
                    reader.Read();
                    switch (token)
                    {
                        case propAngle:
                            angle = (double)reader.Value;
                            break;
                        case propFingersID:
                            id = (FingersID)((Int64)(reader.Value));
                            break;
                    }
                }
                reader.Read();
            }
        }

        public void WriteJson(JsonWriter writer)
        {
            writer.WriteStartObject();
            writer.WritePropertyName(propType);
            writer.WriteValue(GetType().FullName);
            writer.WritePropertyName(propAngle);
            writer.WriteValue(angle);
            writer.WritePropertyName(propFingersID);
            writer.WriteValue((int)id);
            writer.WriteEndObject();
        }
    }
}
