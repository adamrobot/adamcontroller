﻿using Newtonsoft.Json;
using ReactiveUI;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reactive;
using System.Text;
using System.Threading.Tasks;
using System.Reactive.Disposables;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Text.RegularExpressions;

namespace AdamController
{
    public partial class FingersV : UserControl, IViewFor<FingersVM>
    {
        public static readonly DependencyProperty ViewModelProperty = DependencyProperty
           .Register(nameof(ViewModel), typeof(FingersVM), typeof(FingersV));

        public FingersVM ViewModel
        {
            get => (FingersVM)GetValue(ViewModelProperty);
            set => SetValue(ViewModelProperty, value);
        }

        object IViewFor.ViewModel
        {
            get => ViewModel;
            set => ViewModel = (FingersVM)value;
        }

        public FingersV()
        {
            InitializeComponent();
            ViewModel = new FingersVM();
            DataContext = ViewModel;

            this.WhenActivated(disposable =>
            {
                this.Bind(ViewModel, x => x.angle, x => x.angleSlider.Value)
                    .DisposeWith(disposable);
            });
        }
    }
}
