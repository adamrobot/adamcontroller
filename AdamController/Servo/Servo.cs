using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdamController
{
    public enum ServoID
    {
        BodyRotate      = 242,
        BodyTop         = 241,
        BodyBot         = 240,
        LeftLeg1        = 236,
        LeftLeg2        = 235,
        LeftLeg3        = 234,
        RightLeg1       = 239,
        RightLeg2       = 238,
        RightLeg3       = 237,
        LeftHandRotate  = 245,
        LeftHand1       = 246,
        LeftHand2       = 244,
        LeftHand3       = 243,
        RightHandRotate = 249,
        RightHand1      = 250,
        RightHand2      = 248,
        RightHand3      = 247,
        HeadRotate      = 2,
        HeadNod         = 1,
        //LeftEyeRotate   = 3,
        //RightEyeRotate  = 5,
        //EyesNod         = 4,
    }

    public class Servo
    {
        private const double rxScale = 1024 / 300.0;
        private const double mxScale = 4098 / 360.0;

        static public Dictionary<ServoID, Servo> servos { get; private set; } = new Dictionary<ServoID, Servo>
        {
            { ServoID.BodyRotate,      new Servo(ServoID.BodyRotate,      0,   3.54, mxScale) },
            { ServoID.BodyTop,         new Servo(ServoID.BodyTop,         0,  -3.54, mxScale) },
            { ServoID.BodyBot,         new Servo(ServoID.BodyBot,         0,  -3.54, mxScale) },
            { ServoID.LeftLeg1,        new Servo(ServoID.LeftLeg1,        0,   3.54, mxScale) },
            { ServoID.LeftLeg2,        new Servo(ServoID.LeftLeg2,        0,    3.54, mxScale) },
            { ServoID.LeftLeg3,        new Servo(ServoID.LeftLeg3,        0,   3.54, mxScale) },
            { ServoID.RightLeg1,       new Servo(ServoID.RightLeg1,       0,  -3.54, mxScale) },
            { ServoID.RightLeg2,       new Servo(ServoID.RightLeg2,       0,  -3.54, mxScale) },
            { ServoID.RightLeg3,       new Servo(ServoID.RightLeg3,       0,  -3.54, mxScale) },
            { ServoID.LeftHandRotate,  new Servo(ServoID.LeftHandRotate,  0,   1,    mxScale) },
            { ServoID.LeftHand1,       new Servo(ServoID.LeftHand1,       0,   3.54, mxScale) },
            { ServoID.LeftHand2,       new Servo(ServoID.LeftHand2,       0,   1,    mxScale) },
            { ServoID.LeftHand3,       new Servo(ServoID.LeftHand3,       0,   -1,    mxScale) },
            { ServoID.RightHandRotate, new Servo(ServoID.RightHandRotate, 0,   1,    mxScale) },
            { ServoID.RightHand1,      new Servo(ServoID.RightHand1,      0,   -3.54, mxScale) },
            { ServoID.RightHand2,      new Servo(ServoID.RightHand2,      0,   -1,    mxScale) },
            { ServoID.RightHand3,      new Servo(ServoID.RightHand3,      0,   1,    mxScale) },
            { ServoID.HeadRotate,      new Servo(ServoID.HeadRotate,      0,    1,    rxScale) },
            { ServoID.HeadNod,         new Servo(ServoID.HeadNod,         0,    1,    rxScale) },
        };

        public int homePos { get; private set; }
        public ServoID id { get; private set; }
        public int? modelNumber { get; private set; }
        public double ratio { get; private set; }
        public double? scale { get; private set; }
        public int estimatedPos;

        public delegate void FieldUpdated();
        public event FieldUpdated fieldUpdated = delegate { };

        private Message.ServoField _field;
        public Message.ServoField field
        {
            get { return _field; }
            set { _field = value; fieldUpdated(); }
        }

        private Servo(ServoID id, int homePos, double ratio, double scale)
        {
            this.id = id;
            this.homePos = homePos;
            this.ratio = ratio;
            this.scale = scale;
            modelNumber = null;
            estimatedPos = homePos;
        }

        public void SendStartRepeatMsg(AdamTcpClient client)
        {
            ControlTable.ControlTable ct;
            try
            {
                ct = ControlTable.ControlTable.CtController.Instance.controlTables[GetCtType()];
            }
            catch (Exception e)
            {
                throw e;
                //cathedException(e);
                //return;
            }

            Message.ServoScheme inServoMsg = CreateServoScheme(ct.minAddr, (byte)(ct.maxAddr + 1));

            client.SendData(inServoMsg.Msg(Message.Command.REPEATREAD).SetId((int)id).GetData());
        }

        public void SendStopRepeatMsg(ref AdamTcpClient client)
        {
            Message.ServoScheme inServoMsg = CreateServoScheme(0, 0);

            client.SendData(inServoMsg.Msg(Message.Command.STOPREAD).SetId((int)id).GetData());
        }

        public void SendWriteMsg(ref AdamTcpClient client, byte addr, byte dataSize, byte[] data)
        {
            Message.ServoScheme outServoMsg = CreateServoScheme(addr, dataSize, data);

            client.SendData(outServoMsg.Msg(Message.Command.WRITE).GetData());
        }

        public void InitModelNumber()
        {
            modelNumber = BitConverter.ToInt16(field.data.ToArray(), 0);
        }

        public ControlTable.CtType GetCtType()
        {
            if (modelNumber == null)
                throw new Exception("������ ������������ ������������������");

            foreach (var type in ControlTable.ControlTable.types)
            {
                if (type.Value.Contains(modelNumber.Value))
                {
                    return type.Key;
                }
            }

            throw new Exception("����������� ������ ������������");
        }

        public void SendInitModelNumberMsg(AdamTcpClient client)
        {
            Message.ServoScheme inServoMsg = CreateServoScheme(0, 2);

            client.SendData(inServoMsg.Msg(Message.Command.READ).SetId((int)id).GetData());
        }

        private Message.ServoScheme CreateServoScheme(byte addr, byte dataSize, byte[] data = null)
        {
            Message.ServoScheme servoMsg = new Message.ServoScheme();

            int size = 1;

            servoMsg.ids  = new byte[size];
            servoMsg.data = new byte[size][];
            servoMsg.addr = addr;
            servoMsg.size = dataSize;

            for (var i = 0; i < size; i++)
            {
                servoMsg.ids[i] = (byte)id;
                servoMsg.data[i] = new byte[dataSize];
                if (data != null)
                {
                    for (var j = 0; j < dataSize; j++)
                    {
                        servoMsg.data[i][j] = data[j];
                    }
                }
            }

            return servoMsg;
        }
    }
}
