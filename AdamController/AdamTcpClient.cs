﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace AdamController
{
    public class AdamTcpClient
    {
        public TcpClient client;
        private NetworkStream stream;
        public List<byte> currentBlock { get; private set; }
        private byte[] buffer;
        private int currentSize;
        public string address;
        public int port;

        public delegate void BlockReaded();
        public event BlockReaded blockReaded = delegate { };

        public delegate void Connected();
        public event Connected connected = delegate { };

        public AdamTcpClient(int port)
        {
            this.port = port;
            client = new TcpClient();
            client.ReceiveBufferSize = 100000;
            buffer = new byte[client.ReceiveBufferSize];
            currentBlock = new List<byte>();
            currentSize = 0;
        }

        public void ConnectToServer()
        {
            try
            {
                client.BeginConnect(address, port, ConnectCallback, null);
            }
            catch (Exception e)
            {
                //cathedException(e);
                //throw e;
            }
        }

        public void SendData(byte[] data)
        {
            try
            {
                if (client.Client.Connected)
                    stream.BeginWrite(data, 0, data.Length, WriteCallback, null);
            }
            catch (Exception e)
            {
                //cathedException(e);
                //throw e;
            }
        }

        private void ReadCallback(IAsyncResult AR)
        {
            try
            {
                int currentRead = stream.EndRead(AR);
                int bufferIndex = 0;

                while (true)
                {
                    if (currentSize == 0)
                    {
                        for (var i = currentBlock.Count; i < 4; i++)
                        {
                            if (bufferIndex >= currentRead)
                                break;
                            currentBlock.Add(buffer[bufferIndex]);
                            bufferIndex++;
                        }

                        if (currentBlock.Count >= 4)
                        {
                            byte[] sizeBytes = currentBlock.ToArray();
                            Array.Reverse(sizeBytes);
                            currentSize = BitConverter.ToInt32(sizeBytes, 0);
                            currentBlock.Clear();
                            continue;
                        }
                    }

                    if (bufferIndex >= currentRead)
                        break;

                    while (bufferIndex < currentRead)
                    {
                        currentBlock.Add(buffer[bufferIndex]);
                        bufferIndex++;
                        if (currentBlock.Count >= currentSize)
                        {
                            blockReaded();
                            currentBlock.Clear();
                            currentSize = 0;
                            break;
                        }
                    }
                }

                stream.BeginRead(buffer, 0, buffer.Length, ReadCallback, null);
            }
            catch (Exception e)
            {
                //cathedException(e);
                //throw e;
            }
        }

        private void ConnectCallback(IAsyncResult AR)
        {
            try
            {
                client.EndConnect(AR);
                stream = client.GetStream();
                stream.WriteTimeout = 1000;
                //stream.BeginRead(buffer, 0, buffer.Length, ReadCallback, null);

                connected();
            }
            catch (Exception e)
            {
                //cathedException(e);
                //throw e;
            }
        }

        private void WriteCallback(IAsyncResult AR)
        {
            try
            {
                stream.EndWrite(AR);
            }
            catch (Exception e)
            {
                //cathedException(e);
                //throw e;
            }
        }
    }
}
